package com.kluke.project.datastructures

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class FixedSizeBufferTest {

    @Test
    fun `Fill test`() {
        val buffer = FixedSizeBuffer(maxCapacity = 5, fill = 3)

        assertThat(buffer.size).isEqualTo(5)

        while (buffer.isNotEmpty()) assertThat(buffer.removeFirst()).isEqualTo(3)
    }

    @Test
    fun `Deque test`() {
        val buffer = FixedSizeBuffer<Int>(maxCapacity = 3).apply {
            add(0)
            add(1)
            add(2)
            add(3)
            add(4)
        }

        val list = listOf(buffer.removeFirst(), buffer.removeFirst(), buffer.removeFirst())
        assertThat(list).contains(4, 3, 2)
    }

    @Test
    fun `Push and Pop test`() {
        val buffer = FixedSizeBuffer<Int>(4)

        buffer.add(5)

        assertThat(buffer.removeFirst()).isEqualTo(5)
    }
}