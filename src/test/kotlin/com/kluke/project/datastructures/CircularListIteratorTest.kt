package com.kluke.project.datastructures

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class CircularListIteratorTest {
    @Test
    fun `Returns correct item for next and previous calls`() {
        val i = CircularListIterator(listOf("A", "B", "C"))

        assertThat(i.next()).isEqualTo("A")
        assertThat(i.previous()).isEqualTo("C")
        assertThat(i.next()).isEqualTo("A")
        assertThat(i.next()).isEqualTo("B")
        assertThat(i.next()).isEqualTo("C")
        assertThat(i.previous()).isEqualTo("B")
        assertThat(i.previous()).isEqualTo("A")
        assertThat(i.previous()).isEqualTo("C")
    }

    @Test
    fun `hasItems returns false when list is empty`() {
        val i = CircularListIterator<String>(emptyList())

        assertThat(i.hasNext()).isFalse()
        assertThat(i.hasPrevious()).isFalse()
    }

    @Test
    fun `hasItems returns true when list is populated`() {
        val i = CircularListIterator(listOf(1))

        assertThat(i.hasNext()).isTrue()
        assertThat(i.hasPrevious()).isTrue()
    }
}