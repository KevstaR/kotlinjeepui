package com.kluke.project.datastructures

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

import java.util.concurrent.ArrayBlockingQueue

internal class NonBlockingFixedSizeBufferTest {

    @Test
    fun `add elements keeps a fixed queue size and shifts old elements out`() {
        val queue: ArrayBlockingQueue<Int> = NonBlockingFixedSizeBuffer(10)
        for (i in 1..15) queue += i
        assertThat(queue.size).isEqualTo(10)
        assertThat(queue).containsExactly(6, 7, 8, 9, 10, 11, 12, 13, 14, 15)
    }
}