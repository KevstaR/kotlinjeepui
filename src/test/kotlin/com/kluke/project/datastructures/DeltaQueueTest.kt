package com.kluke.project.datastructures

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.*

class DeltaQueueTest {
    @Test
    fun `Query returns expected value`() {
        val input: Deque<Long> = ArrayDeque<Long>().apply {
            add(0L)
            add(10L) // 10
            add(20L) // 10
            add(30L) // 10
            add(35L) // 5
            add(45L) // 10
            add(55L) // 10
            add(75L) // 20
        }

        val query = input.query(50)

        assertThat(query).isEqualTo(5)
        assertThat(input).containsExactly(55L, 75L)
    }

    @Test
    fun `Empty condition`() {
        val input: Deque<Long> = ArrayDeque()

        val query = input.query(50)

        assertThat(query).isEqualTo(0)
    }

    @Test
    fun `Single element condition - Not enough information to take a delta - return 0`() {
        val input: Deque<Long> = ArrayDeque<Long>().apply {
            add(100L)
        }

        val query = input.query(50)

        assertThat(query).isEqualTo(0)
    }

    @Test
    fun `Take more than available`() {
        val input: Deque<Long> = ArrayDeque<Long>(512).apply {
            add(100L)
            add(110L) // 10
            add(120L) // 10
            add(130L) // 10
        }

        val query = input.query(50)

        assertThat(query).isEqualTo(3)
    }
}