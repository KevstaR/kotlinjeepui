package com.kluke.project

import com.kluke.project.sensors.RoadSpeedSensorEntry
import com.kluke.project.sensors.RoadSpeedSensorTable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.Instant.now
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import java.sql.Connection

internal class DatabaseTest {
    @Test
    @Disabled
    fun `Manual database test`() {
        // "connect" to database file called data.sqlite in the current working directory
        // (creates the file if it does not exist)
        Database.connect(url = "jdbc:sqlite:file:data.sqlite", driver = "org.sqlite.JDBC")
        // this isolation level is required for sqlite, may not be applicable to other DBMS
        TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE

        transaction {
            addLogger(StdOutSqlLogger)
            // create the table
            SchemaUtils.create(RoadSpeedSensorTable)

            // add some entries
            RoadSpeedSensorEntry.new {
                time = now().millis
                value = 420
            }

            RoadSpeedSensorEntry.new {
                time = now().millis
                value = 69
            }
        }

        // new transaction to check the results
        transaction {
            RoadSpeedSensorEntry.all().forEach { println(it) }
        }
    }
}