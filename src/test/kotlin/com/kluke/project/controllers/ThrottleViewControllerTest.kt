package com.kluke.project.controllers

import com.kluke.project.sensors.algorithms.serial.toTPSData
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.data.Offset.offset
import org.junit.jupiter.api.Test

internal class ThrottleViewControllerTest {

    @Test
    fun `calibratedMapping returns values within a specified range`() {
        val low: UByte = 13u // Tested low value in live environment
        val middle: UByte = 127u
        val high: UByte = 202u // Tested high value in live environment

        fun UByte.calibratedMapping() = toTPSData().calibratedMapping()

        assertThat(low.calibratedMapping()).isCloseTo(0.1324, offset(0.01))
        assertThat(middle.calibratedMapping()).isCloseTo(60.5458, offset(0.01))
        assertThat(high.calibratedMapping()).isCloseTo(100.2915, offset(0.01))
    }
}