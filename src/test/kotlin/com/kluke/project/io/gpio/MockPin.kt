package com.kluke.project.io.gpio

import com.pi4j.io.gpio.PinMode
import java.util.EnumSet
import com.pi4j.io.gpio.impl.PinImpl
import com.pi4j.io.gpio.Pin
import com.pi4j.io.gpio.PinPullResistance

object MockPin {
    val DIGITAL_BIDIRECTIONAL_PIN: Pin = PinImpl(MockGpioProvider.name, 0, "GPIO-0",
            EnumSet.of(PinMode.DIGITAL_INPUT, PinMode.DIGITAL_OUTPUT),
            PinPullResistance.all())

    val DIGITAL_INPUT_PIN: Pin = PinImpl(MockGpioProvider.name, 1, "GPIO-1",
            EnumSet.of(PinMode.DIGITAL_INPUT),
            PinPullResistance.all())

    val DIGITAL_OUTPUT_PIN: Pin = PinImpl(MockGpioProvider.name, 2, "GPIO-2",
            EnumSet.of(PinMode.DIGITAL_OUTPUT))

    val PWM_OUTPUT_PIN: Pin = PinImpl(MockGpioProvider.name, 3, "GPIO-3",
            EnumSet.of(PinMode.PWM_OUTPUT))

    val ANALOG_BIDIRECTIONAL_PIN: Pin = PinImpl(MockGpioProvider.name, 4, "GPIO-4",
            EnumSet.of(PinMode.ANALOG_INPUT,
                    PinMode.ANALOG_OUTPUT))
    val ANALOG_INPUT_PIN: Pin = PinImpl(MockGpioProvider.name, 5, "GPIO-5",
            EnumSet.of(PinMode.ANALOG_INPUT))

    val ANALOG_OUTPUT_PIN: Pin = PinImpl(MockGpioProvider.name, 6, "GPIO-6",
            EnumSet.of(PinMode.ANALOG_OUTPUT))

    fun digitalInputPin(address: Int, name: String): Pin = PinImpl(
            MockGpioProvider.name,
            address,
            name,
            EnumSet.of(PinMode.DIGITAL_INPUT),
            PinPullResistance.all())

    fun analogInputPin(address: Int, name: String): Pin = PinImpl(
            MockGpioProvider.name,
            address,
            name,
            EnumSet.of(PinMode.ANALOG_INPUT),
            PinPullResistance.all())
}