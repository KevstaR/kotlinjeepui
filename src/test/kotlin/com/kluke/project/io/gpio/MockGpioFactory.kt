package com.kluke.project.io.gpio

import com.pi4j.io.gpio.impl.GpioControllerImpl

object MockGpioFactory {
    fun getInstance() = GpioControllerImpl(getMockProvider())
    fun getMockProvider() = MockGpioProvider
}