package com.kluke.project.io.gpio

import com.pi4j.io.gpio.GpioPinDigitalInput
import com.pi4j.io.gpio.Pin
import com.pi4j.io.gpio.PinPullResistance.PULL_UP

val gpioFactory = MockGpioFactory.getInstance()

// Speedometer
val speedometerPin: Pin = MockPin.digitalInputPin(address = 0, name = "GPIO-0")

val prvSpeedometerPin: GpioPinDigitalInput = gpioFactory.provisionDigitalInputPin(speedometerPin, PULL_UP)

// Transmission
val solenoidPin1: Pin = MockPin.digitalInputPin(address = 1, name = "GPIO-1")
val solenoidPin2: Pin = MockPin.digitalInputPin(address = 2, name = "GPIO-2")
val solenoidPinTc: Pin = MockPin.digitalInputPin(address = 3, name = "GPIO-3")

val prvSolenoidPin1: GpioPinDigitalInput = gpioFactory.provisionDigitalInputPin(solenoidPin1, PULL_UP)
val prvSolenoidPin2: GpioPinDigitalInput = gpioFactory.provisionDigitalInputPin(solenoidPin2, PULL_UP)
val prvSolenoidPinTc: GpioPinDigitalInput = gpioFactory.provisionDigitalInputPin(solenoidPinTc, PULL_UP)

// Shifter
val nssReversePin: Pin = MockPin.digitalInputPin(address = 4, name = "GPIO-4")
val nssThirdPin: Pin = MockPin.digitalInputPin(address = 5, name = "GPIO-5")
val nssSecondFirstPin: Pin = MockPin.digitalInputPin(address = 6, name = "GPIO-6")
val nssHallEffectPin: Pin = MockPin.digitalInputPin(address = 7, name = "GPIO-7")

val prvReversePin: GpioPinDigitalInput = gpioFactory.provisionDigitalInputPin(nssReversePin, PULL_UP)
val prvThirdPin: GpioPinDigitalInput = gpioFactory.provisionDigitalInputPin(nssThirdPin, PULL_UP)
val prvSecondFirstPin: GpioPinDigitalInput = gpioFactory.provisionDigitalInputPin(nssSecondFirstPin, PULL_UP)
val prvHallEffectPin: GpioPinDigitalInput = gpioFactory.provisionDigitalInputPin(nssHallEffectPin, PULL_UP)
