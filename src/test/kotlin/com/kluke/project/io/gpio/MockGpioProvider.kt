package com.kluke.project.io.gpio

import com.pi4j.io.gpio.GpioProvider
import com.pi4j.io.gpio.GpioProviderBase
import com.pi4j.io.gpio.Pin
import com.pi4j.io.gpio.PinState



object MockGpioProvider : GpioProviderBase(), GpioProvider {
    override fun getName() = "MockGpioProvider"

    fun setMockState(pin: Pin, state: PinState) {
        // cache pin state
        getPinCache(pin).state = state

        // dispatch event
        dispatchPinDigitalStateChangeEvent(pin, state)
    }

    fun setMockAnalogValue(pin: Pin, value: Double) {
        // cache pin state
        getPinCache(pin).analogValue = value

        // dispatch event
        dispatchPinAnalogValueChangeEvent(pin, value)
    }
}