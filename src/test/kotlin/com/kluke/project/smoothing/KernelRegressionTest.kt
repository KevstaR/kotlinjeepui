package com.kluke.project.smoothing

import com.kluke.project.lcarsFont
import com.kluke.project.views.TestKernelRegressionView
import com.kluke.project.views.TestStaticKernelRegressionView
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.paint.Color
import javafx.scene.paint.Color.BLACK
import javafx.stage.Stage
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import tornadofx.*
import kotlin.math.cos
import kotlin.math.sin
import kotlin.random.Random

private const val TEST_DIMENSION = 1000.0

/**
 * Set of manual tests for visually inspecting and adjusting kernel regression parameters.
 */
internal class KernelRegressionTest {
    @Test
    @Disabled
    fun `Kernel Regression applied - Display random Fourier transform with rotation`() {
        class MockView : App(TestKernelRegressionView::class) {
            override fun start(stage: Stage) {
                super.start(stage.apply {
                    isResizable = false
                    width = TEST_DIMENSION
                    height = TEST_DIMENSION
                })
            }

            override fun createPrimaryScene(view: UIComponent) = Scene(view.root, TEST_DIMENSION, TEST_DIMENSION, BLACK)
        }

        launch<MockView>()
    }

    @Test
    @Disabled
    fun `Kernel Regression applied - Display static Fourier transform with static rotation`() {
        class MockView : App(TestStaticKernelRegressionView::class) {
            override fun start(stage: Stage) {
                super.start(stage.apply {
                    isResizable = false
                    width = TEST_DIMENSION
                    height = TEST_DIMENSION
                })
            }

            override fun createPrimaryScene(view: UIComponent) = Scene(view.root, TEST_DIMENSION, TEST_DIMENSION, BLACK)
        }

        launch<MockView>()
    }
}

interface TestPointGenerator {
    fun generate(): List<Pair<Double, Double>>
}

abstract class AbstractTestPointGenerator : TestPointGenerator {
    /**
     * Generates a corresponding y value for a given x and y coordinate pair along with a function to rotate.
     */
    protected fun dyRotateFunction(x: Double, y: Double, t: Double,
                                   function: (x: Double, y: Double, t: Double) -> Double) =
            (function(x, y, t) + x * sin(t)) / cos(t) // Same as "* secant(t)"
}

class StaticTestPointGenerator(private val xRange: IntRange = 0..1023,
                               private val yStart: Double = 0.0,
                               private val theta: Double = -0.3,
                               private val amplitude: Double = 200.0,
                               private val frequency: Double = 40.0
) : AbstractTestPointGenerator(), TestPointGenerator {
    override fun generate(): List<Pair<Double, Double>> {
        // Generate Y point for each X
        val resultPoints = mutableListOf<Pair<Double, Double>>()
        for (x in xRange) {
            val dy = dyRotateFunction(x = x.toDouble(), y = yStart, t = theta) { xx, y, t ->
                staticFourierNoise(x = xx * cos(t) + y * sin(t))
            }

            // Pair each x with it's corresponding transformed dy point
            resultPoints += x.toDouble() to dy
        }

        // Return immutable collection
        return resultPoints.toList()
    }

    private fun staticFourierNoise(x: Double): Double {
        val xPos = x / frequency

        val fourier = 0.04381 * cos(1 * xPos) + 0.01290 * sin(1 * xPos) +
                0.02225 * cos(2 * xPos) + 0.04752 * sin(2 * xPos) -
                0.00115 * cos(3 * xPos) - 0.01666 * sin(3 * xPos) -
                0.01701 * cos(4 * xPos) - 0.03435 * sin(4 * xPos) -
                0.00844 * cos(5 * xPos) + 0.08326 * sin(5 * xPos) +
                0.00350 * cos(6 * xPos) + 0.05216 * sin(6 * xPos) -
                0.00456 * cos(7 * xPos) - 0.02620 * sin(7 * xPos) +
                0.05718 * cos(8 * xPos) + 0.08333 * sin(8 * xPos)

        return amplitude * fourier
    }
}

class RandomTestPointGenerator(private val xRange: IntRange = 0..1023,
                               private val yStart: Double = 0.0,
                               private val amplitude: Double = Random.nextDouble(from = 100.0, until = 300.0),
                               private val frequency: Double = Random.nextDouble(from = 20.0, until = 60.0),
                               private val theta: Double = -Random.nextDouble(from = 0.0, until = 1.0)
) : AbstractTestPointGenerator(), TestPointGenerator {
    override fun generate(): List<Pair<Double, Double>> {
        // Generate Y point for each X
        val points = mutableListOf<Pair<Double, Double>>()
        for (x in xRange) {
            val dy = dyRotateFunction(x = x.toDouble(), y = yStart, t = theta) { xx, yy, t ->
                randomFourier(x = xx * cos(t) + yy * sin(t))
            }

            // Pair each x with it's corresponding transformed dy point
            points += x.toDouble() to dy
        }
        return points.toList()
    }

    private fun randomFourier(x: Double): Double {
        fun randomDecimal() = Random.nextDouble(from = 0.00115, until = 0.08)

        val xPos = x / frequency

        val fourier = randomDecimal() * cos(1 * xPos) + randomDecimal() * sin(1 * xPos) +
                randomDecimal() * cos(2 * xPos) + randomDecimal() * sin(2 * xPos) -
                randomDecimal() * cos(3 * xPos) - randomDecimal() * sin(3 * xPos) -
                randomDecimal() * cos(4 * xPos) - randomDecimal() * sin(4 * xPos) -
                randomDecimal() * cos(5 * xPos) + randomDecimal() * sin(5 * xPos) +
                randomDecimal() * cos(6 * xPos) + randomDecimal() * sin(6 * xPos) -
                randomDecimal() * cos(7 * xPos) - randomDecimal() * sin(7 * xPos) +
                randomDecimal() * cos(8 * xPos) + randomDecimal() * sin(8 * xPos)

        return amplitude * fourier
    }
}

/**
 * View function for displaying data points.
 */
fun Parent.dot(x: Double, y: Double, color: Color) = circle {
    centerX = x
    centerY = y
    radius = 1.0
    fill = color
}

fun Parent.drawYLines() {
    for (i in -100..379) if (i % 10 == 0) {
        text {
            x = 0.0
            y = i.toDouble()

            text = (-i).toString()
            font = lcarsFont(12.0)
            fill = Color.WHITE
        }
        rectangle(x = 0, y = i, width = TEST_DIMENSION, height = 1.0) {
            fill = Color.WHITE
            opacity = 0.5
        }
    }
}