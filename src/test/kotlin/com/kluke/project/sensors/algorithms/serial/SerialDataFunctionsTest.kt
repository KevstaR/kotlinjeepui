package com.kluke.project.sensors.algorithms.serial

import com.kluke.project.map
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.data.Offset
import org.junit.jupiter.api.Test
import java.lang.Character.getNumericValue
import java.lang.Integer.toBinaryString
import kotlin.random.Random

internal class SerialDataFunctionsTest {

    @Test
    fun `toRPM returns expected RPM`() {
        assertThat(toRPM(highByte = 255u, lowByte = 127u)).isEqualTo(306)
        assertThat(toRPM(highByte = 130u, lowByte = 61u)).isEqualTo(602)
        assertThat(toRPM(highByte = 65u, lowByte = 31u)).isEqualTo(1204)
    }

    @Test
    fun `extractErrorCodes returns expected error list`() {
        val byte: UByte = 158u

        val expectedErrorCodes = listOf(InjectorErrorCodes.INJECTOR_2_OPEN, InjectorErrorCodes.INJECTOR_6_OPEN)

        val result = byte.extractErrorCodes()

        assertThat(result).containsAll(expectedErrorCodes)
    }

    @Test
    fun `extractErrorCodes returns empty list when no error codes exist`() {
        val byte: UByte = 254u

        val result = byte.extractErrorCodes()

        assertThat(result).isEmpty()
    }

    @Test
    fun `toTPSData returns valid data object`() {
        val byte: UByte = 255u

        val result: TPSData = byte.toTPSData()

        val voltage = result.voltage
        val percentage = result.percentage

        assertThat(voltage).isCloseTo(4.98046875, Offset.offset(0.01))
        assertThat(percentage).isCloseTo(100.0, Offset.offset(0.01))
    }

    @Test
    fun `toAngularUnit returns valid data object`() {
        val byte: UByte = 127u

        val result = byte.toAngularUnit()

        val degrees = result.degree
        val radians = result.radian

        assertThat(degrees).isCloseTo(127.0, Offset.offset(0.01))
        assertThat(radians).isCloseTo(2.2165681, Offset.offset(0.001))
    }

    @Test
    fun `getBit returns all correct positions of a byte`() {
        val uByte: UByte = 146u // 10010010

        toBinaryString(uByte.toInt())
                .reversed()
                .map { getNumericValue(it) }
                .forEachIndexed { index, i -> assertThat(uByte.getBit(position = index)).isEqualTo(i) }
    }

    @Test
    fun `getLoopStatus returns correct enum based on deceleration byte`() {
        val decelUByte: UByte = 2u // 00000010

        val result = decelUByte.getLoopStatus()

        assertThat(result).isEqualTo(LoopStatus.DECELERATION_LOOP)
    }

    @Test
    fun `getLoopStatus returns correct enum based on open byte`() {
        val decelUByte: UByte = 0u // 00000000

        val result = decelUByte.getLoopStatus()

        assertThat(result).isEqualTo(LoopStatus.OPEN_LOOP)
    }

    @Test
    fun `getLoopStatus returns correct enum based on closed byte`() {
        val decelUByte: UByte = 64u // 01000000

        val result = decelUByte.getLoopStatus()

        assertThat(result).isEqualTo(LoopStatus.CLOSED_LOOP)
    }

    @Test
    fun `getLoopStatus returns correct enum when other bits are set`() {
        val decelUByte: UByte = 18u // 00010010

        val result = decelUByte.getLoopStatus()

        assertThat(result).isEqualTo(LoopStatus.DECELERATION_LOOP)
    }
}