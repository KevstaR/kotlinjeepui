package com.kluke.project.sensors.algorithms

import com.kluke.project.sensors.enums.ShifterPosition
import com.kluke.project.sensors.enums.ShifterPosition.*
import com.kluke.project.sensors.states.ShifterState
import com.pi4j.io.gpio.PinState.HIGH
import com.pi4j.io.gpio.PinState.LOW
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class ShifterAlgorithmTest {

    private val sot: SensorAlgorithm<ShifterState, ShifterPosition> = ShifterAlgorithm()

    @Test
    fun `Park gear state from null`() {
        // Given: UNDETERMINED gear state
        val sot: SensorAlgorithm<ShifterState, ShifterPosition> = ShifterAlgorithm()

        val state = ShifterState(reverseGear = LOW, thirdGear = LOW, secondFirstGear = LOW, hallEffect = HIGH)

        // When:
        val result = sot.read(state)

        // Then:
        assertThat(result).isEqualTo(PARK)
    }

    @Test
    fun `Park gear state from reverse`() {
        // Given: Reverse gear state and UNDETERMINED gear state
        val sot: SensorAlgorithm<ShifterState, ShifterPosition> = ShifterAlgorithm()

        val reverseState = ShifterState(reverseGear = HIGH, thirdGear = LOW, secondFirstGear = LOW, hallEffect = LOW)
        val state = ShifterState(reverseGear = LOW, thirdGear = LOW, secondFirstGear = LOW, hallEffect = HIGH)

        // When:
        sot.read(reverseState)
        val result = sot.read(state)

        // Then:
        assertThat(result).isEqualTo(PARK)
    }

    @Test
    fun `Drive gear state from neutral`() {
        // Given: Neutral gear state and UNDETERMINED gear state
        val sot: SensorAlgorithm<ShifterState, ShifterPosition> = ShifterAlgorithm()

        val neutralState = ShifterState(reverseGear = LOW, thirdGear = LOW, secondFirstGear = LOW, hallEffect = LOW)
        val state = ShifterState(reverseGear = LOW, thirdGear = LOW, secondFirstGear = LOW, hallEffect = HIGH)

        // When:
        sot.read(neutralState)
        val result = sot.read(state)

        // Then:
        assertThat(result).isEqualTo(DRIVE)
    }

    @Test
    fun `Drive gear state from third`() {
        // Given: Third gear state and UNDETERMINED gear state
        val sot: SensorAlgorithm<ShifterState, ShifterPosition> = ShifterAlgorithm()

        val undetermined = ShifterState(reverseGear = LOW, thirdGear  = LOW, secondFirstGear  = LOW, hallEffect  = HIGH)
        val reverse = ShifterState(reverseGear = HIGH, thirdGear = LOW, secondFirstGear  = LOW, hallEffect  = LOW)
        val neutral = ShifterState(reverseGear = LOW, thirdGear  = LOW, secondFirstGear  = LOW, hallEffect  = LOW)
        val third = ShifterState(reverseGear = LOW, thirdGear = HIGH, secondFirstGear = LOW, hallEffect = LOW)

        // When
        listOf(undetermined, reverse, neutral, undetermined, third).forEach { sot.read(it) }
        val result = sot.read(undetermined)

        // Then:
        assertThat(result).isEqualTo(DRIVE)
    }

    @Test
    fun `Reverse gear state`() {
        // Given: Reverse gear state
        val state = ShifterState(reverseGear = HIGH, thirdGear = LOW, secondFirstGear = LOW, hallEffect = LOW)

        // When:
        val result = sot.read(state)

        // Then:
        assertThat(result).isEqualTo(REVERSE)
    }

    @Test
    fun `Third gear state`() {
        // Given: Third gear state
        val state = ShifterState(reverseGear = LOW, thirdGear = HIGH, secondFirstGear = LOW, hallEffect = LOW)

        // When:
        val result = sot.read(state)

        // Then:
        assertThat(result).isEqualTo(THIRD)
    }

    @Test
    fun `Second-First gear state`() {
        // Given: Second-First gear state
        val state = ShifterState(reverseGear = LOW, thirdGear = LOW, secondFirstGear = HIGH, hallEffect = LOW)

        // When:
        val result = sot.read(state)

        // Then:
        assertThat(result).isEqualTo(SECOND_FIRST)
    }

    @Test
    fun `Neutral gear state`() {
        // Given: Neutral gear state
        val state = ShifterState(reverseGear = LOW, thirdGear = LOW, secondFirstGear = LOW, hallEffect = LOW)

        // When:
        val result = sot.read(state)

        // Then:
        assertThat(result).isEqualTo(NEUTRAL)
    }
}