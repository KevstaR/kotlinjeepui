package com.kluke.project.sensors.algorithms

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.data.Offset
import org.junit.jupiter.api.Test

internal class SpeedAlgorithmImplTest {
    private val speedAlgorithm: SpeedAlgorithm = SpeedAlgorithmImpl()

    @Test
    fun `read returns the converted speed in KPH`() {
        assertThat(speedAlgorithm.read(62.0)).isCloseTo(112.24, Offset.offset(0.01))
    }
}