package com.kluke.project.sensors.algorithms

import com.kluke.project.units.Celsius
import com.kluke.project.units.Fahrenheit
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.offset
import org.junit.jupiter.api.Test

internal class TemperatureAlgorithmTest {

    private val algorithm = TemperatureAlgorithm()

    @Test
    fun read() {
        data class SensorTestValues(val rawSensorValue: Int, val expectedSensorValueKelvin: Double, val expectedSensorValueFahrenheit: Double, val expectedSensorValueCelsius: Double)
        listOf(
                SensorTestValues(rawSensorValue = 1, expectedSensorValueKelvin = 444.23871647845374, expectedSensorValueFahrenheit = 339.95968966121677, expectedSensorValueCelsius = 171.08871647845376),
                SensorTestValues(rawSensorValue = 18, expectedSensorValueKelvin = 411.27624270241733, expectedSensorValueFahrenheit = 280.6272368643513, expectedSensorValueCelsius = 138.12624270241736),
                SensorTestValues(rawSensorValue = 220, expectedSensorValueKelvin = 370.5586180647727, expectedSensorValueFahrenheit = 207.3355125165909, expectedSensorValueCelsius = 97.40861806477272),
                SensorTestValues(rawSensorValue = 225, expectedSensorValueKelvin = 370.02720528311016, expectedSensorValueFahrenheit = 206.37896950959833, expectedSensorValueCelsius = 96.87720528311019),
                SensorTestValues(rawSensorValue = 230, expectedSensorValueKelvin = 369.5019776429104, expectedSensorValueFahrenheit = 205.43355975723875, expectedSensorValueCelsius = 96.35197764291041),
                SensorTestValues(rawSensorValue = 235, expectedSensorValueKelvin = 368.982637307746, expectedSensorValueFahrenheit = 204.49874715394287, expectedSensorValueCelsius = 95.83263730774604),
                SensorTestValues(rawSensorValue = 240, expectedSensorValueKelvin = 368.4689032027678, expectedSensorValueFahrenheit = 203.5740257649821, expectedSensorValueCelsius = 95.31890320276784),
                SensorTestValues(rawSensorValue = 245, expectedSensorValueKelvin = 367.96050966231024, expectedSensorValueFahrenheit = 202.6589173921585, expectedSensorValueCelsius = 94.81050966231027),
                SensorTestValues(rawSensorValue = 250, expectedSensorValueKelvin = 367.45720520898755, expectedSensorValueFahrenheit = 201.75296937617762, expectedSensorValueCelsius = 94.30720520898757),
                SensorTestValues(rawSensorValue = 255, expectedSensorValueKelvin = 366.9587514490157, expectedSensorValueFahrenheit = 200.85575260822836, expectedSensorValueCelsius = 93.80875144901574),
                SensorTestValues(rawSensorValue = 260, expectedSensorValueKelvin = 366.4649220705291, expectedSensorValueFahrenheit = 199.96685972695244, expectedSensorValueCelsius = 93.31492207052912),
                SensorTestValues(rawSensorValue = 265, expectedSensorValueKelvin = 365.97550193338174, expectedSensorValueFahrenheit = 199.08590348008718, expectedSensorValueCelsius = 92.82550193338176),
                SensorTestValues(rawSensorValue = 270, expectedSensorValueKelvin = 365.49028624040415, expectedSensorValueFahrenheit = 198.2125152327275, expectedSensorValueCelsius = 92.34028624040417),
                SensorTestValues(rawSensorValue = 275, expectedSensorValueKelvin = 365.00907978133773, expectedSensorValueFahrenheit = 197.34634360640797, expectedSensorValueCelsius = 91.85907978133775),
                SensorTestValues(rawSensorValue = 512, expectedSensorValueKelvin = 344.36132703601913, expectedSensorValueFahrenheit = 160.18038866483448, expectedSensorValueCelsius = 71.21132703601916),
                SensorTestValues(rawSensorValue = 768, expectedSensorValueKelvin = 319.02013629075896, expectedSensorValueFahrenheit = 114.56624532336616, expectedSensorValueCelsius = 45.87013629075898),
                SensorTestValues(rawSensorValue = 800, expectedSensorValueKelvin = 314.78762720556085, expectedSensorValueFahrenheit = 106.94772897000958, expectedSensorValueCelsius = 41.637627205560875),
                SensorTestValues(rawSensorValue = 900, expectedSensorValueKelvin = 297.12239997780955, expectedSensorValueFahrenheit = 75.15031996005723, expectedSensorValueCelsius = 23.972399977809573),
                SensorTestValues(rawSensorValue = 950, expectedSensorValueKelvin = 282.5158363550507, expectedSensorValueFahrenheit = 48.85850543909127, expectedSensorValueCelsius = 9.365836355050703),
                SensorTestValues(rawSensorValue = 975, expectedSensorValueKelvin = 271.12824236900207, expectedSensorValueFahrenheit = 28.360836264203762, expectedSensorValueCelsius = -2.021757630997911),
                SensorTestValues(rawSensorValue = 995, expectedSensorValueKelvin = 256.8319619345472, expectedSensorValueFahrenheit = 2.6275314821850415, expectedSensorValueCelsius = -16.318038065452754),
                SensorTestValues(rawSensorValue = 996, expectedSensorValueKelvin = 255.88013256693063, expectedSensorValueFahrenheit = 0.9142386204751674, expectedSensorValueCelsius = -17.26986743306935),
        ).forEach {
            val offset = 0.01
            val kelvin = algorithm.read(it.rawSensorValue)
            assertThat(kelvin).isCloseTo(it.expectedSensorValueKelvin, offset(offset))
            assertThat(Fahrenheit.convert(kelvin)).isCloseTo(it.expectedSensorValueFahrenheit, offset(offset))
            assertThat(Celsius.convert(kelvin)).isCloseTo(it.expectedSensorValueCelsius, offset(offset))
        }
    }
}