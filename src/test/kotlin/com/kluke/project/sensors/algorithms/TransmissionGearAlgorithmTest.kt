package com.kluke.project.sensors.algorithms

import com.kluke.project.sensors.enums.TransmissionGear
import com.kluke.project.sensors.enums.TransmissionGear.*
import com.kluke.project.sensors.states.TransmissionState
import com.pi4j.io.gpio.PinState.HIGH
import com.pi4j.io.gpio.PinState.LOW
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class TransmissionGearAlgorithmTest {

    private val sot: SensorAlgorithm<TransmissionState, TransmissionGear> = TransmissionGearSensorAlgorithm()

    @Test
    fun `First gear`() {
        // Given: FIRST gear
        val state = TransmissionState(solenoid1 = HIGH, solenoid2 = LOW)

        // When:
        val result = sot.read(state)

        // Then:
        assertThat(result).isEqualTo(FIRST)
    }

    @Test
    fun `Second gear`() {
        // Given: SECOND gear
        val state = TransmissionState(solenoid1 = HIGH, solenoid2 = HIGH)

        // When:
        val result = sot.read(state)

        // Then:
        assertThat(result).isEqualTo(SECOND)
    }

    @Test
    fun `Third gear`() {
        // Given: THIRD gear
        val state = TransmissionState(solenoid1 = LOW, solenoid2 = HIGH)

        // When:
        val result = sot.read(state)

        // Then:
        assertThat(result).isEqualTo(THIRD)
    }

    @Test
    fun `Overdrive gear`() {
        // Given: OVERDRIVE gear
        val state = TransmissionState(solenoid1 = LOW, solenoid2 = LOW)

        // When:
        val result = sot.read(state)

        // Then:
        assertThat(result).isEqualTo(OVERDRIVE)
    }
}