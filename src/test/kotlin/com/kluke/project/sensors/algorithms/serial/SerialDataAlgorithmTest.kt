package com.kluke.project.sensors.algorithms.serial

import org.assertj.core.api.Assertions.assertThat
import org.joda.time.Instant
import org.junit.jupiter.api.Test
import java.util.*
import java.util.concurrent.ConcurrentNavigableMap
import java.util.concurrent.ConcurrentSkipListMap
import kotlin.random.Random
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.assertEquals

internal class SerialDataAlgorithmTest {
    private val buffer: MutableList<UByte> = mutableListOf()
    private val expectedBuffer: MutableList<UByte> = mutableListOf()

    private val eventContext: ConcurrentNavigableMap<Instant, Any> = ConcurrentSkipListMap(Comparator.comparingLong { it.millis })
    private val expectedFrameQueue: Deque<UByteArray> = ArrayDeque()

    @BeforeTest
    fun before() {
        clear()
    }

    @AfterTest
    fun after() {
        clear()
    }

    private fun clear() {
        buffer.clear()
        expectedBuffer.clear()

        eventContext.clear()
        expectedFrameQueue.clear()
    }

    @Test
    fun `extractFrames creates frame from buffer`() {

        val result: List<UByteArray> = extractFrame(
                mutableListOf<UByte>(
                        0u, 1u, 2u, 3u, 255u, 0u,
                        0u, 1u, 2u, 3u, 4u, 255u, 0u
                )
        )

        // Cannot compare UByteArray directly
        assertThat(result.map { uByteArray -> uByteArray.map { it.toInt() } })
                .isEqualTo(listOf(listOf(0, 1, 2, 3), listOf(0, 1, 2, 3, 4)))
    }

    @Test
    fun `extractFrames with even odd even odd list expecting 3 elements left in the buffer`() {
        buffer.addAll(
                mutableListOf<UByte>(
                        0u, 1u, 2u, 3u, 255u, 0u,
                        0u, 1u, 2u, 3u, 4u, 255u, 0u,
                        0u, 1u, 2u, 3u, 4u, 5u, 255u, 0u,
                        0u, 1u, 2u, 3u, 4u, 5u, 6u, 255u, 0u,
                        0u, 1u, 2u
                )
        )
        expectedFrameQueue.addAll(
                listOf(
                        ubyteArrayOf(0u, 1u, 2u, 3u),
                        ubyteArrayOf(0u, 1u, 2u, 3u, 4u),
                        ubyteArrayOf(0u, 1u, 2u, 3u, 4u, 5u),
                        ubyteArrayOf(0u, 1u, 2u, 3u, 4u, 5u, 6u)
                )
        )
        expectedBuffer.addAll(listOf<UByte>(0u, 1u, 2u))

        `test and assert expected frameQueue and buffer states`()
    }

    @Test
    fun `extractFrames with even odd even list expecting 4 elements left in the buffer`() {
        buffer.addAll(
                listOf<UByte>(
                        0u, 1u, 2u, 3u, 255u, 0u,
                        0u, 1u, 2u, 3u, 4u, 255u, 0u,
                        0u, 1u, 2u, 3u, 4u, 5u, 255u, 0u,
                        0u, 1u, 2u, 3u
                )
        )
        expectedFrameQueue.addAll(
                listOf(
                        ubyteArrayOf(0u, 1u, 2u, 3u),
                        ubyteArrayOf(0u, 1u, 2u, 3u, 4u),
                        ubyteArrayOf(0u, 1u, 2u, 3u, 4u, 5u)
                )
        )
        expectedBuffer.addAll(listOf<UByte>(0u, 1u, 2u, 3u))

        `test and assert expected frameQueue and buffer states`()
    }

    @Test
    fun `extractFrames with even odd even list and 255s expecting 1 element 255 left in the buffer`() {
        buffer.addAll(
                mutableListOf<UByte>(
                        255u, 255u, 2u, 3u, 255u, 0u,
                        0u, 1u, 255u, 255u, 4u, 255u, 0u,
                        0u, 1u, 2u, 255u, 4u, 5u, 255u, 0u,
                        255u
                )
        )
        expectedFrameQueue.addAll(
                listOf(
                        ubyteArrayOf(255u, 2u, 3u),
                        ubyteArrayOf(0u, 1u, 255u, 4u),
                        ubyteArrayOf(0u, 1u, 2u, 255u, 4u, 5u)
                )
        )
        expectedBuffer.addAll(listOf<UByte>(255u))

        `test and assert expected frameQueue and buffer states`()
    }

    @Test
    fun `extractFrames buffer is empty`() {
        buffer.addAll(emptyList<UByte>())
        expectedFrameQueue.clear()
        expectedBuffer.clear()

        `test and assert expected frameQueue and buffer states`()
    }

    @Test
    fun `extractFrames buffer has a single 255 element`() {
        buffer.add(255u)
        expectedFrameQueue.clear()
        expectedBuffer.add(255u)

        `test and assert expected frameQueue and buffer states`()
    }

    @Test
    fun `extractFrames buffer has a single 0 element`() {
        buffer.add(0u)
        expectedFrameQueue.clear()
        expectedBuffer.add(0u)

        `test and assert expected frameQueue and buffer states`()
    }

    @Test
    fun `extractFrames buffer has a single non-control element`() {
        buffer.add(30u)
        expectedFrameQueue.clear()
        expectedBuffer.add(30u)

        `test and assert expected frameQueue and buffer states`()
    }

    @Test
    fun `extractFrames buffer has both control bytes only`() {
        buffer.addAll(listOf<UByte>(255u, 0u))
        expectedFrameQueue.clear()
        expectedBuffer.clear()

        `test and assert expected frameQueue and buffer states`()
    }

    @Test
    fun `extractFrames buffer has no control bytes`() {
        buffer.addAll(listOf<UByte>(255u, 255u, 2u, 3u))
        expectedFrameQueue.clear()
        expectedBuffer.addAll(listOf<UByte>(255u, 255u, 2u, 3u))

        `test and assert expected frameQueue and buffer states`()
    }

    @Test
    fun `extractFrames called multiple times`() {
        buffer.addAll(
                listOf<UByte>(
                        255u, 255u, 2u, 3u, 255u, 0u,
                        0u, 1u, 255u, 255u, 4u, 255u, 0u,
                        0u, 1u, 2u, 255u, 4u, 5u, 255u, 0u,
                        255u
                )
        )
        expectedFrameQueue.addAll(
                listOf<UByteArray>(
                        ubyteArrayOf(255u, 2u, 3u),
                        ubyteArrayOf(0u, 1u, 255u, 4u),
                        ubyteArrayOf(0u, 1u, 2u, 255u, 4u, 5u),
                        ubyteArrayOf(255u, 1u, 2u, 3u)
                )
        )
        expectedBuffer.add(255u)
        eventContext.clear()

        // 1
        val one = extractFrame(mutableBuffer = buffer)
        assertEquals(expected = expectedBuffer, actual = buffer)

        buffer.addAll(listOf(1u, 2u, 3u, 255u, 0u))

        // 2
        val two = extractFrame(mutableBuffer = buffer)

        val result = (one.map { it.toList() } + two.map { it.toList() }).toList()

        assertEquals(expected = listOf<UByte>(), actual = buffer)
        assertEquals(expected = expectedFrameQueue.map { it.toList() }.toList(), actual = result)
    }

    private fun `test and assert expected frameQueue and buffer states`() {
        // Test
        val frame = extractFrame(mutableBuffer = buffer).map { it.toList() }.toList()
        val expected = expectedFrameQueue.map {it.toList() }.toList()

        // Assert
        assertThat(frame).isEqualTo(expected)
        assertThat(buffer).isEqualTo(expectedBuffer)
    }

        @Test
    fun `removeDuplicateMaxBits filters a simple list correctly`() {
        val input = listOf<UByte>(0u, 1u, 2u, 255u, 255u, 4u, 5u)
        val expected = listOf<UByte>(0u, 1u, 2u, 255u, 4u, 5u)

        assertEqualRemoveDuplicateMaxBits(input = input, expected = expected)
    }

        @Test
    fun `removeDuplicateMaxBits filters an even list correctly`() {
        val mutableList = mutableListOf<UByte>(255u, 255u, 0u, 0u, 255u, 255u, 255u, 255u)
        val expectedList = listOf<UByte>(255u, 0u, 0u, 255u, 255u)

        assertEqualRemoveDuplicateMaxBits(input = mutableList, expected = expectedList)
    }

        @Test
    fun `removeDuplicateMaxBits filters an odd list correctly`() {
        val mutableList = mutableListOf<UByte>(255u, 255u, 0u, 0u, 255u, 255u, 255u, 255u, 255u)
        val expectedList = listOf<UByte>(255u, 0u, 0u, 255u, 255u, 255u)

        assertEqualRemoveDuplicateMaxBits(input = mutableList, expected = expectedList)
    }

        @Test
    fun `removeDuplicateMaxBits filters an odd list of all 255s correctly`() {
        val mutableList = mutableListOf<UByte>(255u, 255u, 255u, 255u, 255u, 255u, 255u)
        val expectedList = listOf<UByte>(255u, 255u, 255u, 255u)

        assertEqualRemoveDuplicateMaxBits(input = mutableList, expected = expectedList)
    }

        @Test
    fun `removeDuplicateMaxBits filters an even list of all 255s correctly`() {
        val mutableList = mutableListOf<UByte>(
                255u, 255u,
                255u, 255u,
                255u, 255u,
                255u, 255u,
                255u, 255u
        )

        val expectedList = listOf<UByte>(255u, 255u, 255u, 255u, 255u)

        assertEqualRemoveDuplicateMaxBits(input = mutableList, expected = expectedList)
    }

        @Test
    fun `removeDuplicateMaxBits filters an odd list of all random correctly`() {
        val filterByte: UByte = 255u
        val mutableList = mutableListOf<UByte>()
        repeat(times = 11) { mutableList.add(randomUByteTo254()) }

        (4..5).forEach { mutableList.add(it, filterByte) }

        val size = mutableList.size
        (size - 2 until size).forEach { mutableList.add(it, filterByte) }

        val result = mutableList.removeDuplicateMaxBits()

        assert(result[4] == filterByte)
        assert(result[10] == filterByte)
        assert(result.size == mutableList.size - 2)
    }

        private fun randomUByteTo254() = Random.nextInt(0, 254).toUByte()

        private fun assertEqualRemoveDuplicateMaxBits(input: List<UByte>, expected: List<UByte>) {
        val inputsAsStrings = input.removeDuplicateMaxBits().map { it.toString() }
        val expectedAsStrings = expected.map { it.toString() }

        assertThat(inputsAsStrings).isEqualTo(expectedAsStrings)
    }
}