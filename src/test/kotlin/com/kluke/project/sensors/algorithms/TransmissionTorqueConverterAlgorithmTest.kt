package com.kluke.project.sensors.algorithms

import com.kluke.project.sensors.enums.TorqueConverter
import com.kluke.project.sensors.enums.TorqueConverter.LOCKED
import com.kluke.project.sensors.enums.TorqueConverter.UNLOCKED
import com.kluke.project.sensors.states.TorqueConverterState
import com.pi4j.io.gpio.PinState.HIGH
import com.pi4j.io.gpio.PinState.LOW
import org.junit.jupiter.api.Test

import org.assertj.core.api.Assertions.assertThat


internal class TransmissionTorqueConverterAlgorithmTest {

    private val sot: SensorAlgorithm<TorqueConverterState, TorqueConverter> = TransmissionTorqueConverterAlgorithm()

    @Test
    fun `Unlocked state`() {
        // Given: UNLOCKED state
        val state = TorqueConverterState(solenoid = LOW)

        // When:
        val result = sot.read(state)

        // Then:
        assertThat(result).isEqualTo(UNLOCKED)
    }

    @Test
    fun `Locked state`() {
        // Given: LOCKED state
        val state = TorqueConverterState(solenoid = HIGH)

        // When:
        val result = sot.read(state)

        // Then:
        assertThat(result).isEqualTo(LOCKED)
    }
}