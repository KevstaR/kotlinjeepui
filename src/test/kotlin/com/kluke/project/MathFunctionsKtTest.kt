package com.kluke.project

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.data.Offset.offset
import org.junit.jupiter.api.Test

internal class MathFunctionsKtTest {

    @Test
    fun map() {
        // Given:
        val double = 100.0

        // When:
        val result = double.map(0.0, 100.0, 0.0, 1000.0)

        // Then:
        assertThat(result).isEqualTo(1000.0)
    }

    @Test
    fun mapInt() {
        // When:
        val result = 100.map(0, 100, 0, 47)

        // Then:
        assertThat(result).isEqualTo(47.0)
    }

    @Test
    fun mapRange() {
        // Given:
        val double = 51.0

        // When:
        val r = double.map(12..96, 21..46)

        // Then:
        assertThat(r).isCloseTo(32.6071, offset(0.01))
    }
}