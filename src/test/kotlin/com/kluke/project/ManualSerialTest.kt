package com.kluke.project

import com.fazecast.jSerialComm.SerialPort
import com.kluke.project.sensors.SerialDataSourceImpl
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import java.lang.Thread.sleep
import java.util.*
import java.util.concurrent.ConcurrentSkipListMap

class ManualSerialTest {

    @Test
    @Disabled("Manual test for local serial testing. Requires a live serial stream on a port.")
    fun `Live serial test with external serial connection`() {
        val frameLength = 31
        val eventContext: EventContext = ConcurrentSkipListMap(Comparator.comparingLong { it.millis })
        val serialPort = SerialPort.getCommPort("/dev/ttyACM0")//"COM4")

        GlobalScope.launch {
            SerialDataSourceImpl(serialPort = serialPort, eventContext = eventContext, frameSize = frameLength).run()
        }

        while (true) {
            sleep(1000)

            eventContext.pollFirstEntry().also {
                it?.value ?: println(it)
            }
        }
    }
}