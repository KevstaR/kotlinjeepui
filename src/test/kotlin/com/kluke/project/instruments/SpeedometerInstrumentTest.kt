package com.kluke.project.instruments

import com.kluke.project.sensors.RoadSpeedFrame
import com.kluke.project.sensors.algorithms.SpeedAlgorithmImpl
import com.kluke.project.units.KilometersPerHour
import com.kluke.project.units.MilesPerHour
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.data.Offset.offset
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class SpeedometerInstrumentTest {
    private val mockRoadSpeedSensor = MockRoadSpeedSensor()
    private val speedometerInstrument = SpeedometerInstrument(
        roadSpeedSensor = mockRoadSpeedSensor,
        speedAlgorithm = SpeedAlgorithmImpl(),
        measuredUnit = KilometersPerHour
    )

    @BeforeEach
    fun `before each`() {
        mockRoadSpeedSensor.frameProperty().set(RoadSpeedFrame(62, 0, 200))
    }

    @Test
    fun `Characterization test - Speed returns default unit`() {
        assertThat(speedometerInstrument.property().get()).isCloseTo(112.24, offset(0.01))
    }

    @Test
    fun `Characterization test - Speed returns specified unit`() {
        speedometerInstrument.measuredUnitProperty().set(MilesPerHour)

        assertThat(speedometerInstrument.property().get()).isCloseTo(69.74, offset(0.01))
    }
}