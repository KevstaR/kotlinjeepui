package com.kluke.project.instruments

import com.kluke.project.sensors.RoadSpeedFrame
import com.kluke.project.sensors.algorithms.SpeedAlgorithmImpl
import com.kluke.project.units.GravitationalAcceleration
import com.kluke.project.units.KilometersPerHourPerSecond
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.data.Offset
import org.junit.jupiter.api.Test

internal class AccelerometerInstrumentTest {
    private val mockRoadSpeedSensor = MockRoadSpeedSensor()
    private val accelerometerInstrument = AccelerometerInstrument(
        roadSpeedSensor = mockRoadSpeedSensor,
        speedAlgorithm = SpeedAlgorithmImpl(),
        measuredUnit = KilometersPerHourPerSecond
    )

    @Test
    fun `Acceleration returns default value`() {
        // Given:
        mockRoadSpeedSensor.frameProperty().set(RoadSpeedFrame(62, 0, 200))

        // When:
        val acceleration = accelerometerInstrument.property()

        // Then:
        assertThat(acceleration.get()).isCloseTo(-18.1027, Offset.offset(.01))
    }

    @Test
    fun `Acceleration returns specified unit`() {
        // Given:
        val accelerometerInstrument = AccelerometerInstrument(
            roadSpeedSensor = MockRoadSpeedSensor(),
            speedAlgorithm = SpeedAlgorithmImpl()
        )

        accelerometerInstrument.measuredUnitProperty().set(GravitationalAcceleration)

        // When:
        val acceleration = accelerometerInstrument.property()

        // Then:
        assertThat(acceleration.get()).isCloseTo(-0.5124, Offset.offset(0.01))
    }
}