package com.kluke.project.instruments

import com.kluke.project.sensors.RoadSpeedFrame
import com.kluke.project.sensors.RoadSpeedSensor
import com.kluke.project.sensors.Sensor
import com.kluke.project.sensors.algorithms.SpeedAlgorithm
import com.kluke.project.services.TimeService
import javafx.beans.property.LongProperty
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleLongProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.ObservableList
import tornadofx.observableListOf

internal class MockRoadSpeedSensor : RoadSpeedSensor {
    private val totalCountProperty: LongProperty = SimpleLongProperty()
    override fun totalCountProperty() = totalCountProperty

    private val frameProperty: ObjectProperty<RoadSpeedFrame> = SimpleObjectProperty()
    override fun frameProperty() = frameProperty

    private var deltaBufferProperty: ObservableList<Int> = observableListOf(20, 20, 20, 20, 20, 10, 10, 10, 10, 10)
    override fun deltaBufferProperty() = deltaBufferProperty
}

internal class MockGallonsPerHourSensor : Sensor<Double> {
    private val property: ObjectProperty<Double> = SimpleObjectProperty()
    override fun property() = property
}

internal class MockSpeedAlgorithm : SpeedAlgorithm {
    override fun read(state: Number) = state.toDouble()
}

internal class MockTimeService : TimeService {
    private val property: LongProperty = SimpleLongProperty()
    override fun elapsedDurationProperty() = property
}