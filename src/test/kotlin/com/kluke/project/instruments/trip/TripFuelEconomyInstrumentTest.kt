package com.kluke.project.instruments.trip

import com.kluke.project.database.AveragingDatabaseManager
import com.kluke.project.instruments.*
import com.kluke.project.instruments.trip.database.TripDatabase
import com.kluke.project.sensors.RoadSpeedFrame
import com.kluke.project.sensors.RoadSpeedSensor
import com.kluke.project.sensors.Sensor
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class TripFuelEconomyInstrumentTest : AbstractTripTest<Double>() {
    private val mockGphDatabase = MockDoubleDatabase()
    private val mockGphDatabaseCount = MockLongDatabase()

    private val mockRawDatabase = MockDoubleDatabase()
    private val mockRawDatabaseCount = MockLongDatabase()

    private val mockGallonsPerHourSensor: Sensor<Double> = MockGallonsPerHourSensor()
    private val mockRoadSpeedSensor: RoadSpeedSensor = MockRoadSpeedSensor()

    override fun testedInstrument() = TripFuelEconomyInstrument(
        gallonsPerHourSensor = mockGallonsPerHourSensor,
        roadSpeedSensor = mockRoadSpeedSensor,
        speedAlgorithm = MockSpeedAlgorithm(),
        gallonsPerHourAverager = AveragingDatabaseManager(mockGphDatabase, mockGphDatabaseCount),
        rawRoadSpeedAverager = AveragingDatabaseManager(mockRawDatabase, mockRawDatabaseCount),
        measuredUnit = MockFuelEconomyUnit()
    )

    private fun setGphProperty(value: Double) = mockGallonsPerHourSensor.property().set(value)
    private fun setProperty(roadSpeedFrame: RoadSpeedFrame) = mockRoadSpeedSensor.frameProperty().set(roadSpeedFrame)
    private fun assertDatabaseEqualTo(tripDatabase: TripDatabase, value: Number) = assertThat(tripDatabase.read()).isEqualTo(value)
    private fun assertDatabasesAreEmpty() {
        assertDatabaseEqualTo(mockGphDatabase,0.0)
        assertDatabaseEqualTo(mockGphDatabaseCount,0L)
        assertDatabaseEqualTo(mockRawDatabase,0.0)
        assertDatabaseEqualTo(mockRawDatabaseCount,0L)
    }

    @Test
    fun `property returns expected value`() {
        /*
         Since we mock out all of the conversion factors,
         treating the delta like MPH makes it a little easier to reason about.
         Example: 50 Miles Per Hour / 5 Gallons Per Hour = 10 Miles Per Gallon
         */
        setGphProperty(5.0)
        setProperty(RoadSpeedFrame(50, 0, 200))
        assertPropertyEqualTo(10.0)
    }

    @Test
    fun `save with a single set for each property`() {
        setGphProperty(1.0)
        setProperty(RoadSpeedFrame(10, 0, 200))
        assertDatabasesAreEmpty()

        instrument.save()

        assertDatabaseEqualTo(mockGphDatabase,1.0)
        assertDatabaseEqualTo(mockGphDatabaseCount,1L)
        assertDatabaseEqualTo(mockRawDatabase,10.0)
        assertDatabaseEqualTo(mockRawDatabaseCount,1L)
    }

    @Test
    fun `save with multiple sets of each property`() {
        for (i in 1..5) {
            setGphProperty(i.toDouble())
            setProperty(RoadSpeedFrame(i * 10, 0, 200))
        }

        assertDatabasesAreEmpty()

        instrument.save()

        assertDatabaseEqualTo(mockGphDatabase,3.0)
        assertDatabaseEqualTo(mockGphDatabaseCount,5L)
        assertDatabaseEqualTo(mockRawDatabase,30.0)
        assertDatabaseEqualTo(mockRawDatabaseCount,5L)
    }

    @Test
    fun `reset called on empty databases`() {
        instrument.reset()
        assertPropertyEqualTo(0.0)
        assertDatabasesAreEmpty()
    }

    @Test
    fun `reset with normally`() {
        setGphProperty(5.0)
        setProperty(RoadSpeedFrame(50, 0, 200))
        instrument.save()

        assertPropertyEqualTo(10.0)
        assertDatabaseEqualTo(mockGphDatabase,5.0)
        assertDatabaseEqualTo(mockGphDatabaseCount,1L)
        assertDatabaseEqualTo(mockRawDatabase,50.0)
        assertDatabaseEqualTo(mockRawDatabaseCount,1L)

        instrument.reset()
        assertPropertyEqualTo(0.0)
        assertDatabasesAreEmpty()
    }

    @Test
    fun `close saves to database`() {
        setGphProperty(5.0)
        setProperty(RoadSpeedFrame(50, 0, 200))
        assertDatabasesAreEmpty()

        instrument.close()

        assertDatabaseEqualTo(mockGphDatabase,5.0)
        assertDatabaseEqualTo(mockGphDatabaseCount,1L)
        assertDatabaseEqualTo(mockRawDatabase,50.0)
        assertDatabaseEqualTo(mockRawDatabaseCount,1L)
    }
}