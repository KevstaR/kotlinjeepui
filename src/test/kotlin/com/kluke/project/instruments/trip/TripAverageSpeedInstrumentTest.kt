package com.kluke.project.instruments.trip

import com.kluke.project.database.AveragingDatabaseManager
import com.kluke.project.instruments.*
import com.kluke.project.instruments.trip.database.TripDatabase
import com.kluke.project.sensors.RoadSpeedFrame
import com.kluke.project.sensors.RoadSpeedSensor
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import kotlin.math.PI
import kotlin.math.roundToInt
import kotlin.math.sin

internal class TripAverageSpeedInstrumentTest : AbstractTripTest<Double>() {
    private val mockRoadSpeedSensor: RoadSpeedSensor = MockRoadSpeedSensor()
    private val mockDistanceDatabase: TripDatabase = MockDoubleDatabase()
    private val mockDistanceDatabaseCount: TripDatabase = MockLongDatabase()
    private val averagingDatabaseManager = AveragingDatabaseManager(mockDistanceDatabase, mockDistanceDatabaseCount)

    override fun testedInstrument() = TripAverageSpeedInstrument(
        roadSpeedSensor = mockRoadSpeedSensor,
        averageSpeedManager = averagingDatabaseManager,
        speedAlgorithm = MockSpeedAlgorithm(),
        measuredUnit = MockMeasuredUnit()
    )

    private fun setProperty(roadSpeedFrame: RoadSpeedFrame) = mockRoadSpeedSensor.frameProperty().set(roadSpeedFrame)
    private fun assertDatabaseEqualTo(value: Double) = assertThat(mockDistanceDatabase.read()).isEqualTo(value)
    private fun assertDatabaseCountEqualTo(value: Long) = assertThat(mockDistanceDatabaseCount.read()).isEqualTo(value)
    private fun assertDatabaseIsEmpty() = assertDatabaseEqualTo(0.0)
    private fun assertDatabaseCountIsEmpty() = assertDatabaseCountEqualTo(0)
    private fun assertDatabasesAreEmpty() {
        assertDatabaseIsEmpty()
        assertDatabaseCountIsEmpty()
    }

    @Test
    fun `property returns expected value`() {
        listOf(5, 10, 15, 20, 25)
            .mapIndexed { index, i -> RoadSpeedFrame(i, (index + 1).toLong(), 200) }
            .forEach(::setProperty)
        assertPropertyEqualTo(15.0)
    }

    @Test
    fun `save with a single set property`() {
        setProperty(RoadSpeedFrame(15, 0, 200))
        assertPropertyEqualTo(15.0)
        assertDatabasesAreEmpty()

        instrument.save()
        assertPropertyEqualTo(15.0)
        assertDatabaseEqualTo(15.0)
        assertDatabaseCountEqualTo(1)
    }

    @Test
    fun `save with multiple property increments`() {
        setProperty(RoadSpeedFrame(15, 0, 200))
        setProperty(RoadSpeedFrame(45, 1, 200))
        assertPropertyEqualTo(30.0)
        assertDatabasesAreEmpty()

        instrument.save()
        assertPropertyEqualTo(30.0)
        assertDatabaseEqualTo(30.0)
        assertDatabaseCountEqualTo(2)
    }

    @Test
    fun `reset called normally`() {
        setProperty(RoadSpeedFrame(12, 0, 200))
        instrument.save()

        assertPropertyEqualTo(12.0)
        assertDatabaseEqualTo(12.0)
        assertDatabaseCountEqualTo(1)

        instrument.reset()
        assertPropertyEqualTo(0.0)
        assertDatabasesAreEmpty()
    }

    @Test
    fun `reset called on empty databases`() {
        instrument.reset()
        assertPropertyEqualTo(0.0)
        assertDatabasesAreEmpty()
    }

    @Test
    fun `close saves to database`() {
        setProperty(RoadSpeedFrame(20, 0, 200))
        assertDatabaseIsEmpty()

        instrument.close()
        assertDatabaseEqualTo(20.0)
    }

    @Test
    fun `average from speedometer is accurate`() {
        // Create sin wave function for simulated, non-linear, test sensor data
        val scale = 25.0

        val wave = (0..156).map { (sin(it / scale - PI / 2.0) + 1.0) * scale }.map { it.roundToInt() }
        val waveAverage = wave.average()
        println("Average: $waveAverage")

        wave.forEachIndexed { index, d ->
            setProperty(RoadSpeedFrame(d, index.toLong(), 100))
        }
        println(testedProperty())
        println(mockDistanceDatabase.read())
        println(mockDistanceDatabaseCount.read())

        instrument.save()

        wave.forEachIndexed { index, d ->
            setProperty(RoadSpeedFrame(d, index.toLong(), 100))
        }
        println(testedProperty())
        println(mockDistanceDatabase.read())
        println(mockDistanceDatabaseCount.read())

        instrument.save()

        println(testedProperty())
        println(mockDistanceDatabase.read())
        println(mockDistanceDatabaseCount.read())

        instrument.close()
        instrument.start()

        println(testedProperty())
        println(mockDistanceDatabase.read())
        println(mockDistanceDatabaseCount.read())
    }

    @Test
    fun `loaded db saves correct value`() {
        mockDistanceDatabase.write(500.0)

        setProperty(RoadSpeedFrame(100, 0, 200))


        assertPropertyEqualTo(600.0)
        assertDatabaseEqualTo(500.0)

        println("Instrument Property: ${instrument.property().get()}")
        println("Mock DB: ${mockDistanceDatabase.read()}")
        println("Mock DB Count: ${mockDistanceDatabaseCount.read()}")

        instrument.save()

        assertPropertyEqualTo(600.0)

        assertDatabaseEqualTo(600.0)
        println("Instrument Property: ${instrument.property().get()}")
        println("Mock DB: ${mockDistanceDatabase.read()}")
        println("Mock DB Count: ${mockDistanceDatabaseCount.read()}")
    }
}