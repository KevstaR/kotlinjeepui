package com.kluke.project.instruments.trip

import com.kluke.project.database.SimpleDatabaseManager
import com.kluke.project.instruments.MockLongDatabase
import com.kluke.project.instruments.MockMeasuredUnit
import com.kluke.project.instruments.MockRoadSpeedSensor
import com.kluke.project.instruments.trip.database.TripDatabase
import com.kluke.project.sensors.RoadSpeedSensor
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class TripDistanceInstrumentTest : AbstractTripTest<Double>() {
    private val mockRoadSpeedSensor: RoadSpeedSensor = MockRoadSpeedSensor()
    private val mockTripDatabase: TripDatabase = MockLongDatabase()

    override fun testedInstrument() = TripDistanceInstrument(
        roadSpeedSensor = mockRoadSpeedSensor,
        distanceManager = SimpleDatabaseManager(mockTripDatabase),
        measuredUnit = MockMeasuredUnit(),
    )

    private fun setProperty(value: Long) = mockRoadSpeedSensor.totalCountProperty().set(value)
    private fun assertDatabaseEqualTo(value: Long) = assertThat(mockTripDatabase.read()).isEqualTo(value)
    private fun assertDatabaseIsEmpty() = assertDatabaseEqualTo(0)

    @Test
    fun `property returns expected value`() {
        setProperty(15)
        assertPropertyEqualTo(15.0)

        instrument.save()
        assertPropertyEqualTo(15.0)

        instrument.reset()
        assertPropertyEqualTo(0.0)

        setProperty(20)
        assertPropertyEqualTo(5.0)

        instrument.save()
        assertPropertyEqualTo(5.0)
    }

    @Test
    fun `save writes to database and stores value in property`() {
        setProperty(15)
        assertPropertyEqualTo(15.0)
        assertDatabaseIsEmpty()

        instrument.save()
        assertPropertyEqualTo(15.0)
        assertDatabaseEqualTo(15)
    }


    @Test
    fun `reset clears database and property`() {
        setProperty(10)
        instrument.save()

        assertDatabaseEqualTo(10)

        instrument.reset()
        assertDatabaseIsEmpty()
    }

    @Test
    fun `close saves to database`() {
        setProperty(20)
        assertDatabaseIsEmpty()

        instrument.close()
        assertDatabaseEqualTo(20)
    }

    @Test
    fun `reset is called on an empty property and database`() {
        assertPropertyEqualTo(0.0)
        assertDatabaseIsEmpty()

        instrument.reset()

        assertPropertyEqualTo(0.0)
        assertDatabaseIsEmpty()
    }
}