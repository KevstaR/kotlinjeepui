package com.kluke.project.instruments.trip

import com.kluke.project.database.SimpleDatabaseManager
import com.kluke.project.instruments.MockLongDatabase
import com.kluke.project.instruments.MockMeasuredUnit
import com.kluke.project.instruments.MockTimeService
import com.kluke.project.instruments.trip.database.TripDatabase
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class TripFormattedTimeInstrumentTest : AbstractTripTest<String>() {
    private val mockElapsedTimeDatabase: TripDatabase = MockLongDatabase()
    private val mockTimeService = MockTimeService()

    override fun testedInstrument() = TripFormattedTimeInstrument(
        elapsedTimeManager = SimpleDatabaseManager(mockElapsedTimeDatabase),
        timeService = mockTimeService,
        measuredUnit = MockMeasuredUnit()
    )

    private fun setProperty(value: Long) = mockTimeService.elapsedDurationProperty().set(value)
    private fun assertDatabaseEqualTo(value: Long) = assertThat(mockElapsedTimeDatabase.read()).isEqualTo(value)
    private fun assertDatabaseIsEmpty() = assertDatabaseEqualTo(0L)

    @Test
    fun `property returns expected value`() {
        for (seconds in listOf<Long>(25, 26, 27, 28, 30)) {
            setProperty(seconds)
            assertPropertyEqualTo("00:00:$seconds")
        }

        setProperty(8600) // Seconds
        assertPropertyEqualTo("02:23:20")
    }

    @Test
    fun `save writes to database and stores value in property`() {
        setProperty(25) // Seconds
        assertPropertyEqualTo("00:00:25")

        instrument.save()
        assertPropertyEqualTo("00:00:25")
        assertDatabaseEqualTo(25L)
    }

    @Test
    fun `reset clears database and property`() {
        setProperty(10) // Seconds
        assertPropertyEqualTo("00:00:10")

        instrument.save()
        assertPropertyEqualTo("00:00:10")
        assertDatabaseEqualTo(10L)

        instrument.reset()
        assertPropertyEqualTo("00:00:00")
        assertDatabaseIsEmpty()

        setProperty(30)
        assertPropertyEqualTo("00:00:20")

        instrument.save()
        assertPropertyEqualTo("00:00:20")
        assertDatabaseEqualTo(20L)
    }

    @Test
    fun `close saves to database`() {
        setProperty(20)
        assertDatabaseIsEmpty()

        instrument.close()
        assertDatabaseEqualTo(20L)
    }

    @Test
    fun `reset is called on an empty property and database`() {
        assertPropertyEqualTo("00:00:00")
        assertDatabaseIsEmpty()

        instrument.reset()

        assertPropertyEqualTo("00:00:00")
        assertDatabaseIsEmpty()
    }
}