package com.kluke.project.instruments.trip

import org.assertj.core.api.Assertions
import org.assertj.core.api.ObjectAssert
import org.junit.jupiter.api.BeforeEach

abstract class AbstractTripTest<T> {
    protected lateinit var instrument: TripInstrument<T>

    abstract fun testedInstrument(): TripInstrument<T>
    protected fun testedProperty(): T = instrument.property().get()

    protected fun assertPropertyEqualTo(value: T): ObjectAssert<T> = Assertions.assertThat(testedProperty()).isEqualTo(value)

    @BeforeEach
    fun `before each`() {
        instrument = testedInstrument().apply { start() }
    }
}