package com.kluke.project.instruments

import com.kluke.project.instruments.trip.database.AbstractDatabase
import com.kluke.project.instruments.trip.database.TripDatabase

internal class MockLongDatabase(val initial: Long = 0) : AbstractDatabase(), TripDatabase {
    var database = initial
    override fun readFromDatabase() = database
    override fun writeToDatabase(value: Number) { database = value.toLong() }
}

internal class MockDoubleDatabase(val initial: Double = 0.0) : AbstractDatabase(), TripDatabase {
    var database = initial
    override fun readFromDatabase() = database
    override fun writeToDatabase(value: Number) { database = value.toDouble() }
}

