package com.kluke.project.instruments

import com.sothawo.mapjfx.Coordinate
import org.junit.jupiter.api.Test

internal class StreetMapInstrumentTest {

    @Test
    fun `latLonUrlBuilder returns a list of coordinates that match a specific URL pattern`() {
        val regex = Regex("https://[a-c]\\.tile\\.openstreetmap\\.org/16/[0-9]+/[0-9]+\\.png")

        val result = latLonUrlBuilder(
                startCoordinate = Coordinate(38.5916, -79.6494),
                endCoordinate = Coordinate(36.5507, -75.8674)
        )

        result.forEach { assert(regex matches it) }
    }
}