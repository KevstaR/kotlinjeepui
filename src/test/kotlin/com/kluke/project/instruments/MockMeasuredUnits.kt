package com.kluke.project.instruments

import com.kluke.project.units.Describable
import com.kluke.project.units.FuelEconomyUnit
import com.kluke.project.units.MeasuredUnit
import com.kluke.project.units.SimpleDescribable

internal class MockMeasuredUnit : MeasuredUnit, Describable by SimpleDescribable("MOCK", "MOCK", "MOCK") {
    override fun conversionFactor() = 1.0
}

internal class MockFuelEconomyUnit : FuelEconomyUnit, Describable by SimpleDescribable("MOCK", "MOCK", "MOCK") {
    override fun conversionFactor() = 1.0
}