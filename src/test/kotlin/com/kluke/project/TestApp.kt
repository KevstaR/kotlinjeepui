package com.kluke.project

import com.kluke.project.Instruments.*
import com.kluke.project.ServiceConfiguration.EVENT_CONTEXT
import com.kluke.project.ServiceConfiguration.SERIAL_DATA_SOURCE
import com.kluke.project.ServiceConfiguration.TICKER_100MS
import com.kluke.project.database.FrameManager
import com.kluke.project.instruments.camera.CameraInstrument
import com.kluke.project.instruments.VelocityInstrument
import com.kluke.project.instruments.trip.TripInstrument
import com.kluke.project.sensors.*
import com.kluke.project.services.Service
import javafx.stage.Stage
import kotlinx.coroutines.*
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import tornadofx.*
import java.lang.Thread.sleep

class TestApp : AbstractApp(), KoinComponent {
    override fun koinModules(): List<Module> = listOf(main, testPins, inputPins, mockServices, sensors, instruments, testModule)

    override fun start(stage: Stage) {
        startServices()
        startMockServices()
        super.start(stage)
    }

    override fun stop() {
        super.stop()
        get<VelocityInstrument>(named(ODOMETER_INSTRUMENT)).close()
        get<CameraInstrument>(named(FRONT_CAMERA)).close()
        stopTripInstruments()
    }

    private fun startServices() {
        // Start global tick event service
        get<Service>(named(TICKER_100MS)).start()

        get<CameraInstrument>(named(FRONT_CAMERA)).start()

        startTripInstruments()

        // Start Real Mock Serial data source
        GlobalScope.launch { get<SerialDataSource>(named(SERIAL_DATA_SOURCE)).run() }

        FrameManager(eventContext = get(named(EVENT_CONTEXT))).run()
    }

    private fun startMockServices() = get<List<Service>>(named("MockServices")).forEach(Service::start)
    private fun startTripInstruments() = tripInstruments().forEach(TripInstrument<out Any>::start)
    private fun stopTripInstruments() = tripInstruments().forEach(TripInstrument<out Any>::close)
    private fun tripInstruments() = get<List<TripInstrument<out Any>>>(named(TRIP_INSTRUMENTS))
}

