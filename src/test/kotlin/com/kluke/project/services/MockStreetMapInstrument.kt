package com.kluke.project.services

import com.kluke.project.instruments.StreetMapInstrument
import com.sothawo.mapjfx.Coordinate
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleObjectProperty
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.File
import tornadofx.*

class MockStreetMapInstrument : StreetMapInstrument {
    private val coordinateProperty = SimpleObjectProperty(Coordinate(0.0, 0.0))
    private var coordinate by coordinateProperty

    init {
        GlobalScope.launch {
            val lines = File("src/test/resources/TestTripLatLong").readLines().map { it.parseToLatLong() }
            val iterator = lines.iterator()

            while (iterator.hasNext()) {
                runLater {

                val next = iterator.next()

                    synchronized(coordinate) {
                        coordinate = Coordinate(next.latitude, next.longitude)
                    }
                }

                delay(250)
            }
        }
    }

    override fun coordinateProperty() = coordinateProperty

    private fun String.parseToLatLong(): Coordinate {
        // Match brackets on each line
        val match = Regex("\\[(.*?)]").find(this)

        // Extract capture group
        val parsedValue = match!!.groups[1]!!.value

        // Split into lat long
        val split = parsedValue.split(',')

        // Extract values
        val latitude = split[0].substringAfter('=').toDouble()
        val longitude = split[1].substringAfter('=').toDouble()

        return Coordinate(latitude, longitude)
    }
}