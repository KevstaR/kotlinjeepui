package com.kluke.project.services

import com.ginsberg.cirkle.CircularList
import com.kluke.project.io.gpio.MockGpioProvider
import com.kluke.project.map
import com.kluke.project.smoothing.StaticTestPointGenerator
import com.pi4j.io.gpio.GpioPinAnalogInput
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit.*
import java.util.concurrent.atomic.AtomicInteger

class MockFuelState(scheduler: ScheduledExecutorService,
                    private val provider: MockGpioProvider,
                    private val pin: GpioPinAnalogInput) : AbstractService(scheduler) {
    val mockData = CircularList(generatedData())
    val count = AtomicInteger(0)

    override fun start() {
        val invocation = {
            val state = mockData[count.get()]

            provider.setMockAnalogValue(pin.pin, state.second)

            count.incrementAndGet()

            Unit
        }

        handle = scheduler.scheduleAtFixedRate(invocation, 0, 200, MILLISECONDS)
    }

    /**
     * Map all y values to the sensor's range: 0-1023
     */
    private fun generatedData(): List<Pair<Double, Double>> {
        val data = StaticTestPointGenerator().generate()
        val dataUnzip = data.unzip()

        val xValues = dataUnzip.first
        val yValues = dataUnzip.second

        val yMax = yValues.maxOrNull() ?: 1023.0
        val yMin = yValues.minOrNull() ?: 0.0

        // Map Y values to max analog range.
        val sensorRange = yValues.map { y -> y.map(inMin = yMin, inMax = yMax, outMin = 0.0, outMax = 1023.0) }

        return xValues.zip(sensorRange)
    }
}