package com.kluke.project.services

import com.kluke.project.events.TickEvent
import com.kluke.project.io.gpio.MockGpioProvider
import com.pi4j.io.gpio.GpioPinDigitalInput
import com.pi4j.io.gpio.PinState
import tornadofx.*
import kotlin.math.PI
import kotlin.math.sin

class MockSpeedometerState(private val provider: MockGpioProvider,
                           private val speedometerPin: GpioPinDigitalInput,
                           private val service: Service) : Component() {
    init {
        subscribe<TickEvent> {
            // Create sin wave function for simulated, non-linear, test sensor data
            val scale = 25.0
            val formula = (sin((service as TickService).totalTicks.get() / scale - PI / 2.0) + 1.0) * scale

            // Toggle pin based on sin wave value
            for (i in 0..formula.toInt()) {
                provider.setMockState(speedometerPin.pin, PinState.getInverseState(speedometerPin.state))
            }
        }
    }
}