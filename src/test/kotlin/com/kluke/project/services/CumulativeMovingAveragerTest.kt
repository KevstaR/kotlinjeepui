package com.kluke.project.services

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.data.Offset.offset
import org.junit.jupiter.api.Test

internal class CumulativeMovingAveragerTest {
    @Test
    fun averageTest() {
        val given = listOf(0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 11.0, 7.0, 8.0, 9.0)
        val expected = listOf(0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.7, 4.1, 4.5, 5.0)


        val averager = CumulativeMovingAverager()

        given.forEachIndexed { i, d ->
            assertThat(averager.accumulateAndGet(d)).isCloseTo(expected[i], offset(0.1))
        }
    }

    @Test
    fun `total average returns expected result`() {
        with(CumulativeMovingAverager() as CumulativeAverager) {
            listOf(5, 10, 15, 20, 25).forEach(::accumulateAndGet)
            assertThat(get()).isEqualTo(15.0)
        }
    }
}