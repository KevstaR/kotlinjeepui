package com.kluke.project.services

import com.kluke.project.io.gpio.MockGpioProvider
import com.pi4j.io.gpio.GpioPinDigitalInput
import com.pi4j.io.gpio.PinState
import com.pi4j.io.gpio.PinState.HIGH
import com.pi4j.io.gpio.PinState.LOW
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit.SECONDS
import java.util.concurrent.atomic.AtomicInteger

class MockTransmissionState(scheduler: ScheduledExecutorService,
                            private val provider: MockGpioProvider,
                            private val solenoidPin1: GpioPinDigitalInput,
                            private val solenoidPin2: GpioPinDigitalInput,
                            private val solenoidPin3: GpioPinDigitalInput) : AbstractService(scheduler), Service {

    private val stateTable = listOf(
            LOW  to HIGH,
            LOW  to LOW,
            HIGH to LOW,
            HIGH to HIGH
    )

    private val count = AtomicInteger(0)

    override fun start() {
        val invocation = {
            if (count.get() >= stateTable.size) count.set(0)

                setStates(stateTable[count.get()])

            count.incrementAndGet()

            Unit
        }

        handle = scheduler.scheduleAtFixedRate(invocation, 0, 2, SECONDS)
    }

    private fun setStates(states: Pair<PinState, PinState>) {
        // Transmission Gear
        provider.setMockState(solenoidPin1.pin, PinState.getInverseState(states.first))
        provider.setMockState(solenoidPin2.pin, PinState.getInverseState(states.second))

        // Torque Converter
        provider.setMockState(solenoidPin3.pin, PinState.getInverseState(solenoidPin3.state))
    }
}