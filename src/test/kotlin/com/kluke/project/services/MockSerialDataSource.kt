package com.kluke.project.services

import com.ginsberg.cirkle.circular
import com.kluke.project.sensors.SerialDataSource
import com.kluke.project.EventContext
import com.kluke.project.datastructures.circularIterator
import com.kluke.project.sensors.algorithms.serial.ECUFrame
import com.kluke.project.sensors.algorithms.serial.createFrame
import javafx.beans.property.SimpleObjectProperty
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.joda.time.Instant.now
import tornadofx.*
import kotlin.random.Random
import kotlin.random.nextLong

class MockSerialDataSource(private val eventContext: EventContext) : SerialDataSource {

    private val frameProperty = SimpleObjectProperty<ECUFrame>()

    override suspend fun run() {
        GlobalScope.launch {
            val ecuData = listOf(
                            ubyteArrayOf(177u, 20u, 63u, 158u, 110u, 106u, 222u, 100u, 90u, 92u, 253u, 254u, 13u /*TPS min*/, 15u, 0u, 110u, 248u, 0u, 16u, 100u, 0u, 72u, 15u, 39u, 128u, 116u, 160u, 0u, 0u, 10u, 243u),
                            ubyteArrayOf(177u, 20u, 63u, 118u, 110u, 106u, 222u, 126u, 115u, 96u, 253u, 254u, 50u /*TPS*/, 15u, 0u, 110u, 248u, 0u, 16u, 99u, 0u, 72u, 15u, 39u, 128u, 116u, 160u, 0u, 0u, 14u, 243u),
                            ubyteArrayOf(177u, 20u, 63u, 118u, 110u, 106u, 223u, 254u, 182u, 94u, 253u, 254u, 30u /*TPS*/, 15u, 0u, 110u, 248u, 0u, 16u, 100u, 0u, 76u, 15u, 39u, 128u, 116u, 160u, 0u, 0u, 14u, 243u),
                            ubyteArrayOf(177u, 20u, 63u, 118u, 110u, 106u, 223u, 254u, 182u, 94u, 253u, 254u, 127u /*TPS*/, 15u, 0u, 110u, 248u, 0u, 16u, 101u, 0u, 76u, 15u, 39u, 128u, 116u, 160u, 0u, 0u, 14u, 243u),
                            ubyteArrayOf(177u, 20u, 63u, 120u, 110u, 106u, 222u, 254u, 74u, 96u, 253u, 254u, 202u /*TPS  max*/, 15u, 0u, 110u, 248u, 0u, 16u, 103u, 0u, 76u, 15u, 39u, 128u, 116u, 160u, 0u, 0u, 14u, 243u)
                    ).map { it.createFrame(31) }.circular()

            val ecuDataIterator = ecuData.circularIterator()

            while (true) {
                delay(Random.nextLong(200L..600L))

                val frame = ecuDataIterator.next()

                // Set frame property
                runLater {
                    synchronized(frameProperty) {
                        frameProperty.set(frame)
                    }
                }

                // Record to event stream
                eventContext[now()] = frame
            }
        }
    }

    override fun property(): SimpleObjectProperty<ECUFrame> = frameProperty
}