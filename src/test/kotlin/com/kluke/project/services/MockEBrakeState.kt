package com.kluke.project.services

import com.kluke.project.io.gpio.MockGpioProvider
import com.pi4j.io.gpio.GpioPinDigitalInput
import com.pi4j.io.gpio.PinState.getInverseState
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit.MILLISECONDS
import kotlin.random.Random

class MockEBrakeState(scheduler: ScheduledExecutorService,
                      private val provider: MockGpioProvider,
                      private val pin: GpioPinDigitalInput) : AbstractService(scheduler) {
    override fun start() {
        val action = { provider.setMockState(pin.pin, getInverseState(pin.state)) }
        handle = scheduler.scheduleAtFixedRate(action, 7000, Random.nextLong(1000, 3000), MILLISECONDS)
    }
}