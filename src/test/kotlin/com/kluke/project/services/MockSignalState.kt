package com.kluke.project.services

import com.kluke.project.io.gpio.MockGpioProvider
import com.pi4j.io.gpio.GpioPinDigitalInput
import com.pi4j.io.gpio.PinState.getInverseState
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit.MILLISECONDS
import kotlin.random.Random.Default.nextLong

class MockSignalState(scheduler: ScheduledExecutorService,
                      private val provider: MockGpioProvider,
                      private val leftSignalPin: GpioPinDigitalInput,
                      private val rightSignalPin: GpioPinDigitalInput,
                      private val lightsSignalPin: GpioPinDigitalInput,
                      private val brightsSignalPin: GpioPinDigitalInput) : AbstractService(scheduler), Service {
    private val invocations = listOf(
            { provider.setMockState(leftSignalPin.pin, getInverseState(leftSignalPin.state)) },
            { provider.setMockState(rightSignalPin.pin, getInverseState(rightSignalPin.state)) },
            { provider.setMockState(lightsSignalPin.pin, getInverseState(lightsSignalPin.state)) },
            { provider.setMockState(brightsSignalPin.pin, getInverseState(brightsSignalPin.state)) },
    )


    override fun start() {
        for (i in invocations) scheduler.scheduleAtFixedRate(i, 0, nextLong(from = 1000, until = 3000), MILLISECONDS)
    }
}