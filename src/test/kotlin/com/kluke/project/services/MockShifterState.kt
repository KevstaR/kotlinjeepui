package com.kluke.project.services

import com.kluke.project.io.gpio.MockGpioProvider
import com.kluke.project.sensors.states.ShifterState
import com.pi4j.io.gpio.GpioPinDigitalInput
import com.pi4j.io.gpio.PinState.*
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit.MILLISECONDS
import java.util.concurrent.atomic.AtomicInteger

class MockShifterState(scheduler: ScheduledExecutorService,
                       private val provider: MockGpioProvider,
                       private val nssReversePin: GpioPinDigitalInput,
                       private val nssThirdPin: GpioPinDigitalInput,
                       private val nssHallEffectPin: GpioPinDigitalInput,
                       private val nssSecondFirstPin: GpioPinDigitalInput) : AbstractService(scheduler), Service {
    private val count = AtomicInteger(0)
    private val table = states() + states().reversed()

    private fun states() = listOf(
            ShifterState(hallEffect = HIGH, reverseGear = LOW,  thirdGear = LOW,  secondFirstGear = LOW), // PARK
            ShifterState(hallEffect = LOW,  reverseGear = HIGH, thirdGear = LOW,  secondFirstGear = LOW), // REVERSE
            ShifterState(hallEffect = LOW,  reverseGear = LOW,  thirdGear = LOW,  secondFirstGear = LOW), // NEUTRAL
            ShifterState(hallEffect = HIGH, reverseGear = LOW,  thirdGear = LOW,  secondFirstGear = LOW), // DRIVE
            ShifterState(hallEffect = LOW,  reverseGear = LOW,  thirdGear = HIGH, secondFirstGear = LOW), // THIRD
            ShifterState(hallEffect = LOW,  reverseGear = LOW,  thirdGear = LOW,  secondFirstGear = HIGH) // SECOND_FIRST
    )

    override fun start() {
        val invocation = {
            if (count.get() >= table.size) count.set(0)

            val state = table[count.get()]

            // Shift State - Inverted-inverted states for testing.  Real life states inverted.
            provider.setMockState(nssReversePin.pin, getInverseState(state.reverseGear))
            provider.setMockState(nssThirdPin.pin, getInverseState(state.thirdGear))
            provider.setMockState(nssHallEffectPin.pin, getInverseState(state.hallEffect))
            provider.setMockState(nssSecondFirstPin.pin, getInverseState(state.secondFirstGear))

            count.incrementAndGet()

            Unit
        }

        handle = scheduler.scheduleAtFixedRate(invocation, 0, 1500, MILLISECONDS)
    }
}