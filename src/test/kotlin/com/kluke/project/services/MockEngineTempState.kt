package com.kluke.project.services

import com.ginsberg.cirkle.CircularList
import com.kluke.project.io.gpio.MockGpioProvider
import com.pi4j.io.gpio.GpioPinAnalogInput
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit.MILLISECONDS
import java.util.concurrent.atomic.AtomicInteger

class MockEngineTempState(scheduler: ScheduledExecutorService,
                          private val provider: MockGpioProvider,
                          private val pin: GpioPinAnalogInput) : AbstractService(scheduler) {
    private val mockData = CircularList(generatedData())
    private val count = AtomicInteger(0)

    override fun start() {
        provider.setMockAnalogValue(pin.pin, 256.0)

        val invocation = {
            val state = mockData[count.get()]

            provider.setMockAnalogValue(pin.pin, state.toDouble())

            count.incrementAndGet()

            Unit
        }

        handle = scheduler.scheduleAtFixedRate(invocation, 0, 4990, MILLISECONDS)
    }

    /**
     * Map all y values to the sensor's range: 0-1023
     *
     * Information on sensor ranges:
     * 1   = 339.96 F -- Max reading capable by the sensor
     * 18  = 280.62 F -- Real life max on most gauges
     * 220 = 207.34 F
     * 225 = 206.38 F
     * 230 = 205.43 F
     * 235 = 204.50 F
     * 240 = 203.57 F
     * 245 = 202.65 F
     * 250 = 201.75 F
     * 255 = 200.86 F
     * 260 = 199.97 F
     * 265 = 199.09 F
     * 270 = 198.21 F
     * 275 = 197.35 F
     * 512 = 160.18 F
     * 768 = 114.57 F
     * 800 = 106.95 F
     * 900 = 75.15 F
     * 950 = 48.86 F
     * 975 = 28.36 F
     * 995 = 2.63 F
     * 996 = 0.91 F
     */
    private fun generatedData() = listOf(
            996,
            975,
            950,
            900,
            800,
            768,
            512,
            275,
            270,
            260,
            255,
            235,
            240,
            245,
            220,
            225,
            18,
            1,
    )
}