package com.kluke.project.services

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.data.Offset.offset
import org.junit.jupiter.api.Test
import org.joda.time.Period.seconds
import java.util.concurrent.Executors.newScheduledThreadPool
import java.util.concurrent.TimeUnit.SECONDS

internal class TimeTest {
    @Test
    fun `Time format returns correct value`() {
        val timeFormat = timeFormat()
        assertThat(timeFormat.print(seconds(0).normalizedStandard())).isEqualTo("00:00:00")
        assertThat(timeFormat.print(seconds(120).normalizedStandard())).isEqualTo("00:02:00")
        assertThat(timeFormat.print(seconds(69).normalizedStandard())).isEqualTo("00:01:09")
        assertThat(timeFormat.print(seconds(2).normalizedStandard())).isEqualTo("00:00:02")
    }

    @Test
    fun `Time service returns correct property value after set duration`() {
        val exec = newScheduledThreadPool(1)
        val timeService: TimeService = TimeServiceImpl(exec)
        exec.awaitTermination(4, SECONDS)
        assertThat(timeService.elapsedDurationProperty().get()).isEqualTo(5)
    }

    @Test
    fun `Nanos returns correct values`() {
        with(Duration(1)) {
            assertThat(toSeconds()).isEqualTo(1)
            assertThat(toMinutes()).isCloseTo(0.016, offset(0.001))
            assertThat(toHours()).isCloseTo(0.00027, offset(0.00001))
        }
    }
}