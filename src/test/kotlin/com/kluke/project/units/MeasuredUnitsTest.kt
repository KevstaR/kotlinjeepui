package com.kluke.project.units

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.data.Offset
import org.assertj.core.data.Offset.offset
import org.junit.jupiter.api.Test

internal class MeasuredUnitsTest {

    @Test
    fun `kph conversion factor test`() {
        assertThat(KilometersPerHour.conversionFactor()).isCloseTo(1.0, offset(0.0))
    }

    @Test
    fun `mph conversion factor test`() {
        assertThat(MilesPerHour.conversionFactor()).isCloseTo(0.6214, offset(0.0))
    }

    @Test
    fun `kelvin to fahrenheit conversion factor test`() {
        val givenValueInKelvin = 100.0
        val expectedValueInFahrenheit =  1.8 * (givenValueInKelvin - 273.15) + 32.0 // -279.66999999999996

        assertThat(Fahrenheit.convert(100.0)).isCloseTo(expectedValueInFahrenheit, offset(0.01))
    }

    @Test
    fun `kelvin to celsius`() {
        val givenValueInKelvin = 100.0
        val expectedValueInCelsius = givenValueInKelvin - 273.15 // 173.15

        assertThat(Celsius.convert(givenValueInKelvin)).isCloseTo(expectedValueInCelsius, offset(0.01))
    }
}