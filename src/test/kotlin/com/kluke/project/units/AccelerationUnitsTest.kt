package com.kluke.project.units

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.data.Offset.offset
import org.junit.jupiter.api.Test

internal class AccelerationUnitsTest {

    @Test
    fun conversionFactor() {
        assertThat(KilometersPerHourPerSecond.conversionFactor()).isCloseTo(1.0, offset(0.0))
        assertThat(MilesPerHourPerSecond.conversionFactor()).isCloseTo(0.6214, offset(0.0))
        assertThat(GravitationalAcceleration.conversionFactor()).isCloseTo(0.02833, offset(0.0))
    }
}