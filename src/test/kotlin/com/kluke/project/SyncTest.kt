package com.kluke.project

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicLong

class SyncTest {
    @Test
    fun save() {
        runBlocking {
            val incrementer = Incrementer()

            val scope = CoroutineScope(Executors.newFixedThreadPool(4).asCoroutineDispatcher())
            scope.launch {
                val coroutines = (1..1000).map {
                    //create 1000 coroutines (light-weight threads).
                    launch {
                        // and in each of them, increment the sharedCounter 1000 times.
                        repeat(1000) {
                            incrementer.update()
                        }
                    }
                }

                // wait for all coroutines to finish their jobs.
                coroutines.forEach { it.join() }
            }.join()

            println("The number of shared counter is ${incrementer.getSharedCounter()}")
        }
    }
}

class Incrementer {
    private val sharedCounter = AtomicLong(0)

    // the increment (++) operation is done atomically, so all threads wait for its completion
    fun update() = sharedCounter.incrementAndGet()
    fun getSharedCounter() = sharedCounter.get()
}