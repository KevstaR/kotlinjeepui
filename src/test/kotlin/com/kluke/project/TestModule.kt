package com.kluke.project

import com.fazecast.jSerialComm.SerialPort
import com.kluke.project.AnalogPins.ENGINE_TEMP_SENSOR_PIN
import com.kluke.project.AnalogPins.FUEL_SENSOR_PIN
import com.kluke.project.DigitalPins.*
import com.kluke.project.ServiceConfiguration.EVENT_CONTEXT
import com.kluke.project.ServiceConfiguration.SERIAL_DATA_SOURCE
import com.kluke.project.ServiceConfiguration.TICKER_100MS
import com.kluke.project.instruments.StreetMapInstrument
import com.kluke.project.io.gpio.MockGpioFactory
import com.kluke.project.io.gpio.MockPin.analogInputPin
import com.kluke.project.io.gpio.MockPin.digitalInputPin
import com.kluke.project.sensors.Platform.UNIX
import com.kluke.project.sensors.Platform.WINDOWS
import com.kluke.project.sensors.SerialDataSourceImpl
import com.kluke.project.services.*
import com.pi4j.io.gpio.GpioController
import com.sun.javafx.PlatformUtil.isUnix
import com.sun.javafx.PlatformUtil.isWindows
import org.jetbrains.exposed.sql.Database
import org.koin.core.component.get
import org.koin.core.qualifier.named
import org.koin.dsl.module

val testPins = module {
    fun createDigitalPin(enum: DigitalPins) = single(named(enum), createdAtStart = true) { digitalInputPin(address = enum.address, name = enum.name) }
    for (pinName in DigitalPins.values()) createDigitalPin(pinName)

    fun createAnalogPin(enum: AnalogPins) = single(named(enum), createdAtStart = true) { analogInputPin(address = enum.address, name = enum.name) }
    for (pinName in AnalogPins.values()) createAnalogPin(pinName)
}

val mockServices = module {
    single(qualifier = named("MockSpeedometerState"), createdAtStart = true) {
        MockSpeedometerState(
                provider = get(),
                speedometerPin = get(named(SPEEDOMETER_PIN)),
                service = get(named(TICKER_100MS))
        )
    }

    single<Service>(qualifier = named("MockTransmissionState"), createdAtStart = true) {
        MockTransmissionState(
                scheduler = get(),
                provider = get(),
                solenoidPin1 = get(named(TRANSMISSION_SOLENOID1_PIN)),
                solenoidPin2 = get(named(TRANSMISSION_SOLENOID2_PIN)),
                solenoidPin3 = get(named(TRANSMISSION_SOLENOID3_PIN)),
        )
    }

    single<Service>(qualifier = named("MockShifterState"), createdAtStart = true) {
        MockShifterState(
                scheduler = get(),
                provider = get(),
                nssReversePin = get(named(NEUTRAL_SAFETY_SWITCH_REVERSE_PIN)),
                nssThirdPin = get(named(NEUTRAL_SAFETY_SWITCH_THIRD_PIN)),
                nssHallEffectPin = get(named(NEUTRAL_SAFETY_SWITCH_HALL_EFFECT_PIN)),
                nssSecondFirstPin = get(named(NEUTRAL_SAFETY_SWITCH_SECOND_FIRST_PIN)),
        )
    }

    single<Service>(qualifier = named("MockFuelState"), createdAtStart = true) {
        MockFuelState(
                scheduler = get(),
                provider = get(),
                pin = get((named(FUEL_SENSOR_PIN)))
        )
    }

    single<Service>(qualifier = named("MockEngineTempState"), createdAtStart = true) {
        MockEngineTempState(
                scheduler = get(),
                provider = get(),
                pin = get(named(ENGINE_TEMP_SENSOR_PIN))
        )
    }

    single<Service>(qualifier = named("MockEBrakeState"), createdAtStart = true) {
        MockEBrakeState(
                scheduler = get(),
                provider = get(),
                pin = get(named(E_BRAKE_SENSOR_PIN))
        )
    }

    single<Service>(qualifier = named("MockSignalState"), createdAtStart = true) {
        MockSignalState(
                scheduler = get(),
                provider = get(),
                leftSignalPin = get(named(LEFT_SIGNAL_SENSOR_PIN)),
                rightSignalPin = get(named(RIGHT_SIGNAL_SENSOR_PIN)),
                lightsSignalPin = get(named(LIGHTS_SIGNAL_SENSOR_PIN)),
                brightsSignalPin = get(named(BRIGHTS_SIGNAL_SENSOR_PIN))
        )
    }

    factory<List<Service>>(named("MockServices")) {
        listOf(
            get(named("MockTransmissionState")),
            get(named("MockShifterState")),
            get(named("MockFuelState")),
            get(named("MockEngineTempState")),
            get(named("MockEBrakeState")),
            get(named("MockSignalState")),
        )
    }
}

val testModule = module {
    // Database
    // "connect" to database file called data.sqlite in the current working directory
    // (creates the file if it does not exist)
    single(createdAtStart = true) { Database.connect(url = "jdbc:sqlite:file:data.sqlite", driver = "org.sqlite.JDBC") }

    // Pin Configuration
    single { MockGpioFactory.getMockProvider() }
    single<GpioController> { MockGpioFactory.getInstance() }

    // Mock Instrument
    single<StreetMapInstrument> { MockStreetMapInstrument() }


    // Serial data source
    single(qualifier = named(SERIAL_DATA_SOURCE), createdAtStart = true) {
        val platform = when {
            isWindows() -> WINDOWS
            isUnix() -> UNIX
            else -> throw RuntimeException("Unsupported platform.")
        }

        val port = SerialPort.getCommPort(platform.port())

        if (port.isOpen) {
            SerialDataSourceImpl(serialPort = port, eventContext = get(named(EVENT_CONTEXT)))
        } else {
            MockSerialDataSource(eventContext = get(named(EVENT_CONTEXT)))
        }
    }
}