package com.kluke.project

import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.koin.test.KoinTest
import tornadofx.launch

internal class TestGui : KoinTest {
    @Test
    @Disabled
    fun `Launch app with test context for local development`() = launch<TestApp>()
}