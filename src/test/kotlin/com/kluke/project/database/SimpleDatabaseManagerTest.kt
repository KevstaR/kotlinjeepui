package com.kluke.project.database

import com.kluke.project.instruments.MockLongDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicLong

internal class SimpleDatabaseManagerTest : AbstractDatabaseManagerTest<Long, Long>() {
    private val mockTripDatabase = MockLongDatabase(100)

    override fun testedInstrument(): TripDatabaseManager<Long, Long> = SimpleDatabaseManager(mockTripDatabase)

    private fun assertDatabaseEqualTo(value: Long) = assertThat(mockTripDatabase.database).isEqualTo(value)

    @Test
    fun `save and reset properly track changes when the sensor value is incremented`() {
        setProperty(50)
        assertThat(mockTripDatabase.database).isEqualTo(100)

        instrument.save()
        assertDatabaseEqualTo(150)
        assertInputPropertyEqualTo(50L)
        assertOutputPropertyEqualTo(150L)

        setProperty(60)

        assertDatabaseEqualTo(150)

        instrument.save()
        assertDatabaseEqualTo(160)
        assertInputPropertyEqualTo(60L)
        assertOutputPropertyEqualTo(160L)

        instrument.reset()
        assertDatabaseEqualTo(0)
        assertInputPropertyEqualTo(60L)
        assertOutputPropertyEqualTo(0L)

        setProperty(90)
        instrument.save()

        assertDatabaseEqualTo(30)
        assertInputPropertyEqualTo(90L)
        assertOutputPropertyEqualTo(30L)
    }

    @Test
    fun `save updates backing db with running increments`() {
        var runningTotal = 0L
        for (increment in listOf(50L, 25L, 1000L)) {
            runningTotal += increment
            setProperty(runningTotal)
            instrument.save()

            assertDatabaseEqualTo(runningTotal + mockTripDatabase.initial)
        }
    }

    @Test
    fun `reset sets backing db to 0`() {
        instrument.inputProperty().value = 15L
        instrument.save()

        instrument.reset()

        assertDatabaseEqualTo(0)
        assertInputPropertyEqualTo(15L)
        assertOutputPropertyEqualTo(0L)
    }

    @Test
    fun `reset gets called multiple times`() {
        instrument.inputProperty().value = 15L
        instrument.save()

        repeat(times = 5) { instrument.reset() }
        assertDatabaseEqualTo(0)
    }

    @Test
    fun `save is synchronized`() {
        runBlocking {
            val incrementer = Incrementer()

            val scope = CoroutineScope(Executors.newFixedThreadPool(4).asCoroutineDispatcher())
            scope.launch {
                val coroutines = (1..1000).map {
                    //create 1000 coroutines (light-weight threads).

                    launch {
                        for (i in 1..1000) { // and in each of them, increment the sharedCounter 1000 times.
                            synchronized(instrument) {
                                setProperty(incrementer.update())
                            }

                            // Test
                            instrument.save()
                        }
                    }
                }

                // wait for all coroutines to finish their jobs.
                coroutines.forEach { it.join() }
            }.join()

            assertInputPropertyEqualTo(1000000L)
            assertDatabaseEqualTo(1000000 + mockTripDatabase.initial)
        }
    }

    private class Incrementer {
        private val sharedCounter = AtomicLong(0)

        // the increment (++) operation is done atomically, so all threads wait for its completion
        fun update() = sharedCounter.incrementAndGet()
    }
}