package com.kluke.project.database

import com.kluke.project.instruments.MockDoubleDatabase
import com.kluke.project.instruments.MockLongDatabase
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test


internal class AveragingDatabaseManagerTest : AbstractDatabaseManagerTest<Double, Double>(){
    private data class TestData(val input: Double, val expected: Double)

    private val mockAverageDatabase = MockDoubleDatabase()
    private val mockCountDatabase = MockLongDatabase()

    override fun testedInstrument(): TripDatabaseManager<Double, Double> = AveragingDatabaseManager(mockAverageDatabase, mockCountDatabase)

    private fun assertAverageDatabaseEqualTo(value: Double) = assertThat(mockAverageDatabase.database).isEqualTo(value)
    private fun assertCountDatabaseEqualTo(value: Long) = assertThat(mockCountDatabase.database).isEqualTo(value)

    @Test
    fun `save and reset properly accumulate changes when the sensor value is incremented`() {
        listOf(
            TestData(1.0, 1.0),
            TestData(2.0, 1.5),
            TestData(3.0, 2.0),
            TestData(4.0, 2.5),
            TestData(5.0, 3.0),
        ).forEach {
            setProperty(it.input)
            assertOutputPropertyEqualTo(it.expected)
        }
    }

    @Test
    fun `property gives average`() {
        for (i in 1..5) setProperty(i.toDouble())
        assertOutputPropertyEqualTo(3.0)
    }

    @Test
    fun `save is called multiple times in a row and keeps correct average`() {
        setProperty(5.0)
        repeat(2) { instrument.save() }
        assertAverageDatabaseEqualTo(5.0)
        assertCountDatabaseEqualTo(1)
    }

    @Test
    fun `reset sets backing db to 0`() {
        // Given:
        setProperty(15.0)
        instrument.save()

        assertAverageDatabaseEqualTo(15.0)
        assertCountDatabaseEqualTo(1)

        // When:
        instrument.reset()

        // Then:
        assertAverageDatabaseEqualTo(0.0)
        assertCountDatabaseEqualTo(0)

        assertInputPropertyEqualTo(15.0)
        assertOutputPropertyEqualTo(0.0)
    }

    @Test
    fun `reset doesn't keep average`() {
        setProperty(5.0)
        setProperty(10.0)

        assertOutputPropertyEqualTo(7.5)

        instrument.reset()

        assertOutputPropertyEqualTo(0.0)
    }
}