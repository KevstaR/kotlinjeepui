package com.kluke.project.database

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.ObjectAssert
import org.junit.jupiter.api.BeforeEach

abstract class AbstractDatabaseManagerTest<A, B> {
    protected lateinit var instrument: TripDatabaseManager<A, B>

    abstract fun testedInstrument(): TripDatabaseManager<A, B>
    protected fun testedInputProperty(): A = instrument.inputProperty().get()
    protected fun testedOutputProperty(): B = instrument.outputProperty().get()

    protected fun setProperty(value: A) = instrument.inputProperty().set(value)

    protected fun assertInputPropertyEqualTo(value: A): ObjectAssert<A> = assertThat(testedInputProperty()).isEqualTo(value)
    protected fun assertOutputPropertyEqualTo(value: B): ObjectAssert<B> = assertThat(testedOutputProperty()).isEqualTo(value)

    @BeforeEach
    fun `before each`() {
        instrument = testedInstrument()
    }
}