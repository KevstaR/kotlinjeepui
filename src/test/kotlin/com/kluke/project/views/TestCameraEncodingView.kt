package com.kluke.project.views

import com.github.sarxos.webcam.Webcam
import com.kluke.project.instruments.camera.CameraInstrument
import com.kluke.project.instruments.camera.CameraInstrumentImpl
import com.kluke.project.instruments.camera.Encoder
import com.kluke.project.units.SimpleDescribable
import com.kluke.project.views.components.cameraBox
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.input.KeyCode.S
import javafx.scene.paint.Color
import javafx.stage.Stage
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import tornadofx.*
import java.awt.image.BufferedImage
import java.io.File
import java.lang.Thread.sleep
import java.util.*

const val WIDTH = 640.0
const val HEIGHT = 410.0

private lateinit var cameraInstrument: CameraInstrument

class TestCameraEncodingView {
    @Test
    @Disabled("Manual test. Press S to encode")
    fun `In memory images are encoded on save`() {
        cameraInstrument = CameraInstrumentImpl(
                camera = Webcam.getDefault(),
                describable = SimpleDescribable(
                        name = "FRONT CAMERA",
                        abbreviation = "FC",
                        id = "front-camera"
                )
        )
        launch<MockApp>()
    }

    @Test
    @Disabled("Manual test. Press S to start mock encoding and display encoding text.")
    fun `Visual test of encoder UI - Mocked Encoder`() {
        cameraInstrument = CameraInstrumentImpl(
                camera = Webcam.getDefault(),
                encoder = object : Encoder {
                    override fun encode(frameBuffer: Queue<BufferedImage>, file: File) = sleep(2000)
                },
                describable = SimpleDescribable(
                        name = "FRONT CAMERA",
                        abbreviation = "FC",
                        id = "front-camera"
                )
        )
        launch<MockApp>()
    }
}

class MockApp : App(TestEncodingView::class) {
    override fun start(stage: Stage) {
        super.start(stage.apply {
            isResizable = false
            width = WIDTH
            height = HEIGHT
        })
        cameraInstrument.start()
    }

    override fun createPrimaryScene(view: UIComponent) = Scene(view.root, HEIGHT, WIDTH, Color.BLACK).apply {
        onMouseClicked = EventHandler {
            println("x: ${it.sceneX}, y: ${it.sceneY}")
        }
        onKeyPressed = EventHandler {
            when (it.code) {
                S -> cameraInstrument.save()
                else -> return@EventHandler
            }
        }
    }

    override fun stop() = cameraInstrument.stop()
}

class TestEncodingView : View("Test Encoding View") {
    override val root = region {
        cameraBox(
                gaugeWidth = 60.0,
                gaugeHeight = 185.0,
                topHeight = 10.0,
                bottomHeight = 10.0,
                leftWidth = 20.0,
                rightWidth = 20.0
        ) {
            cameraProperty().bind(cameraInstrument.property())
            encodingStatusProperty().bind(cameraInstrument.encodingStatusProperty())
        }
    }
}