package com.kluke.project.views

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ViewFunctionsKtTest {

    @Test
    fun toStringWithPaddedStart() {
        assertThat(1.toStringWithPaddedStart(pad = 2)).isEqualTo("01")
        assertThat(10.5.toStringWithPaddedStart(pad = 2)).isEqualTo("10")
        assertThat(2.toStringWithPaddedStart(pad = 3, paddingChar = '.')).isEqualTo("..2")
    }

    @Test
    fun precisionTest() {
        assertThat(100.0123.toStringPrecision(precision = 2)).isEqualTo("100.01")
        assertThat(100.0.toStringPrecision(precision = 2)).isEqualTo("100.0")
        assertThat(100.24.toStringPrecision(precision = 3)).isEqualTo("100.24")
        assertThat(0.6942.toStringPrecision(precision = 3)).isEqualTo("0.694")
        assertThat(12.345678.toStringPrecision(precision = 4)).isEqualTo("12.3456")
        assertThat(140.toStringPrecision(precision = 4)).isEqualTo("140")
        assertThat("Anything".toStringPrecision(precision = 4)).isEqualTo("Anything")
    }

    @Test
    fun makeTextWithSign() {
        assertThat(14.makeTextWithSign(padding = 2)).isEqualTo(" 14")
        assertThat(1.makeTextWithSign(padding = 2)).isEqualTo(" 01")
        assertThat((-10).makeTextWithSign(padding = 3)).isEqualTo("-010")
        assertThat((-10).makeTextWithSign(padding = 2)).isEqualTo("-10")
        assertThat((-7.9).makeTextWithSign(padding = 2)).isEqualTo("-07")
        assertThat(0.makeTextWithSign(padding = 2)).isEqualTo(" 00")
    }

    @Test
    fun `pieChart returns correct value`() {
        // When:
        val result = listOf(1.0, 2.0, 3.0, 4.0).toDegrees()

        // Then:
        assertThat(result).isEqualTo(listOf(36.0, 72.0, 108.0, 144.0))
    }
}