package com.kluke.project.views

import com.kluke.project.services.MockFuelState
import com.kluke.project.smoothing.StaticTestPointGenerator
import com.kluke.project.smoothing.dot
import com.kluke.project.smoothing.kernelRegression
import javafx.scene.layout.Background.EMPTY
import javafx.scene.layout.Region
import javafx.scene.paint.Color
import tornadofx.*
import java.util.concurrent.Executors

class TestStaticKernelRegressionView : View("Test Static Kernel Regression View") {
    override val root = Region()

    private val dataPoints = StaticTestPointGenerator().generate()

    init {
        val result = dataPoints.kernelRegression()

        with(root) {
            background = EMPTY
            translateY = 100.0

            // Draw original x, y points
            for (item in dataPoints) dot(item.first, -item.second, Color.WHITE)

            // Draw kernel regression line
            for (item in result) dot(item.first.toDouble(), -item.second.toDouble(), Color.RED)
        }
    }
}
