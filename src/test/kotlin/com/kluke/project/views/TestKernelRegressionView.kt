package com.kluke.project.views

import com.kluke.project.smoothing.RandomTestPointGenerator
import com.kluke.project.smoothing.dot
import com.kluke.project.smoothing.kernelRegression
import javafx.scene.Node
import javafx.scene.layout.Background.EMPTY
import javafx.scene.layout.Region
import javafx.scene.paint.Color.RED
import javafx.scene.paint.Color.WHITE
import tornadofx.*
import java.lang.Thread.sleep
import java.util.concurrent.atomic.AtomicReference

class TestKernelRegressionView : View("Test Kernel Regression View") {
    override val root = Region()

    private val dataPointsRef = AtomicReference(RandomTestPointGenerator().generate())

    init {
        task(daemon = true) {
            while (true) {
                val childList: MutableList<Node>? = root.getChildList()

                if (childList != null && childList.isNotEmpty()) runLater { childList.clear() }

                val data = dataPointsRef
                        .getAndUpdate { RandomTestPointGenerator().generate() }

                val result = data.kernelRegression()

                with(root) {

                    runLater {
                        // Draw original x, y points
                        for (item in data) dot(item.first, -item.second, WHITE)

                        // Draw kernel regression line
                        for (item in result) dot(item.first.toDouble(), -item.second.toDouble(), RED)
                    }
                }

                sleep(1000)
            }
        }

        with(root) {
            background = EMPTY
            translateY = 100.0
        }
    }
}