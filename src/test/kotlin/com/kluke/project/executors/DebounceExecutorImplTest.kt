package com.kluke.project.executors

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

import java.lang.Thread.sleep
import kotlin.concurrent.thread

internal class DebounceExecutorImplTest {

    @Test
    fun execute() {
        // Create a counter to track invocations
        var count = 0

        // System under test
        val debounceExecutor: DebounceExecutor = DebounceExecutorImpl()

        // Op to invoke after a debounce
        val op = {
            count++

            Unit
        }

        // Try to invoke multiple times in threads
        repeat(times = 4) {
            thread {
                debounceExecutor.execute("Test", 1000, op)
            }
        }

        // Wait for debounce to finish
        sleep(1500)

        assertThat(count).isEqualTo(1)
    }
}