package com.kluke.project.views

import com.kluke.project.CustomFont
import com.kluke.project.colors.LCARS_BLUE
import com.kluke.project.colors.LCARS_ORANGE
import com.kluke.project.instruments.ShifterInstrument
import com.kluke.project.sensors.enums.ShifterPosition
import javafx.scene.layout.Region
import javafx.scene.paint.Color.TRANSPARENT
import javafx.scene.paint.CycleMethod
import javafx.scene.paint.LinearGradient
import javafx.scene.paint.Stop
import javafx.scene.shape.Rectangle
import javafx.scene.text.TextAlignment.CENTER
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import tornadofx.*

class ShifterView : View("ShifterView"), KoinComponent {
    private val shifterInstrument = get<ShifterInstrument>()

    override val root = Region()

    private val shiftBoxWidth = 120.0
    private val shiftBoxHeight = 100.0

    init {
        with(root) {
            stackpane {
                text {
                    translateXProperty().bind(shifterInstrument.shifterTextPositionProperty())
                    translateY = 12.0
                    text = ShifterPosition.values().map { it.abbreviation }.fold("", { s1, s2 -> "$s1 $s2" })
                    fill = LCARS_BLUE
                    font = CustomFont.LCARS_UC.getFont(72.0)
                    textAlignment = CENTER
                }
                val gradient = LinearGradient(0.0, 0.0, 1.0, 0.0, true, CycleMethod.NO_CYCLE,
                        Stop(0.0, TRANSPARENT),
                        Stop(0.5, LCARS_BLUE),
                        Stop(1.0, TRANSPARENT)
                )

                clip = Rectangle().apply {
                    translateX = 2.0
                    translateY = 2.0
                    width = shiftBoxWidth
                    height = shiftBoxHeight
                    fill = gradient
                }
            }

            hbox {
                val width = shiftBoxWidth / 2.0
                val height = shiftBoxHeight / 2.0
                vbox {
                    lcarsGaugeEdge(width = width, height = height, color = LCARS_ORANGE)
                    lcarsGaugeEdge(width = width, height = height, color = LCARS_ORANGE) { scaleY = -1.0 }
                }
                vbox {
                    lcarsGaugeEdge(width = width, height = height, color = LCARS_ORANGE) { scaleX = -1.0 }
                    lcarsGaugeEdge(width = width, height = height, color = LCARS_ORANGE) {
                        scaleX = -1.0
                        scaleY = -1.0
                    }
                }
            }
        }
    }
}
