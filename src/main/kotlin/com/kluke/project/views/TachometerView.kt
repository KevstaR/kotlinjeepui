package com.kluke.project.views

import com.kluke.project.colors.LCARS_BLUE
import com.kluke.project.colors.LCARS_ORANGE
import com.kluke.project.controllers.TachometerViewController
import com.kluke.project.lcarsFont
import com.kluke.project.lcarsFontSmall
import com.kluke.project.views.components.tripBox
import javafx.event.EventTarget
import javafx.scene.Parent
import javafx.scene.layout.Region
import javafx.scene.shape.Rectangle
import javafx.scene.text.TextAlignment.CENTER
import tornadofx.*

class TachometerView : View() {
    private val controller by inject<TachometerViewController>()

    override val root = region {
        tripBox(
            gaugeHeight = 95.0,
            gaugeWidth = 40.0,
            leftWidth = 20.0,
            rightWidth = 20.0,
            valueFont = lcarsFont(144.0),
            bottomHeight = 50.0
        ) {
            valueProperty().bind(controller.property().stringBinding { it?.rpm?.toStringWithPaddedStart(pad = 4) })
            with("        ") { unitNameProperty().set("${this}ROTATIONS PER SECOND${this}") }
            titleProperty().set("TACHOMETER")
            valueTextOffsetProperty().set(-8.0)
            titleTextOffsetProperty().set(-22.0)
        }
        progressBar()
    }

    private fun EventTarget.progressBar() =
        hbox {
            spacing = 2.0
            translateX = 46.0
            translateY = 170.0

            bindChildren(controller.fillBar()) {
                Rectangle().apply {
                    this.height = 20.0
                    this.width = 13.0
                    withColor(LCARS_BLUE)
                }
            }
        }
}