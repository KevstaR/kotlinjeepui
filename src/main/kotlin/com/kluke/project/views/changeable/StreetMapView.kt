package com.kluke.project.views.changeable

import com.kluke.project.colors.LCARS_ORANGE
import com.kluke.project.instruments.StreetMapInstrument
import com.kluke.project.instruments.latLonUrlBuilder
import com.kluke.project.views.withColor
import com.sothawo.mapjfx.Configuration
import com.sothawo.mapjfx.Coordinate
import com.sothawo.mapjfx.MapView
import com.sothawo.mapjfx.Projection.WEB_MERCATOR
import javafx.scene.Parent
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import tornadofx.*
import java.io.File
import java.nio.file.Files

class StreetMapView : View("Street Map View"), KoinComponent {

    private val streetMapInstrument = get<StreetMapInstrument>()

    override val root = stackpane {
        mapView {
            prefHeight = 370.0
            prefWidth = 666.0

            val cacheDir = File("mapjfx-cache").toPath()
            Files.createDirectories(cacheDir)

            /*
            Offline cache population:
            https://www.sothawo.com/2019/10/mapjfx-2-9-0-and-1-28-0-with-offline-cache-improvements/

            Map tiles explained:
            https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
             */
            offlineCache.apply {
                cacheDirectory = cacheDir
                setActive(true)

                val urls = latLonUrlBuilder(
                        startCoordinate = Coordinate(38.5916, -79.6494),
                        endCoordinate = Coordinate(36.5507, -75.8674)
                )

                preloadURLs(urls)
            }

            zoom = 16.0

            initialize(Configuration.builder()
                    .projection(WEB_MERCATOR)
                    .showZoomControls(false)
                    .build())

            // Workaround for library issue with bound property
            streetMapInstrument.coordinateProperty().onChange {
                centerProperty().set(it)
            }
        }

        circle {
            radius = 10.0
            withColor(LCARS_ORANGE)
        }
    }

    private fun Parent.mapView(op: MapView.() -> Unit = {}) = MapView().attachTo(this, op)
}
