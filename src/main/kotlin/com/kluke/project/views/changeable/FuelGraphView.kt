package com.kluke.project.views.changeable

import com.kluke.project.controllers.changeable.FuelController
import com.kluke.project.viewcomponents.Styles
import com.kluke.project.viewcomponents.lcarsLineGraph
import com.kluke.project.viewcomponents.lcarsSide
import tornadofx.*

class FuelGraphView : View("Fuel Graph") {
    private val controller by inject<FuelController>()

    override val root = hbox {
        val shift = -40.0

        lcarsSide()
        region {
            translateX = shift
            addClass(Styles.chart)

            lcarsLineGraph {
                graphTitle = "FUEL LEVEL"
                graphHeight = 360.0
                graphWidth = 630.0

                data.add(controller.rawFuelInstrumentSeries)
                data.add(controller.fuelInstrumentSeries)
            }
        }
        lcarsSide {
            scaleX = -1.0
            translateX = shift * 2.0
        }
    }


}
