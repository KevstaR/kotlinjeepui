package com.kluke.project.views.changeable

import tornadofx.*

class ChangeableViewContainer : View("ChangeableViewContainer") {
    override val root = region {
        translateY = 17.0
    }
}