package com.kluke.project.views.changeable

import com.ginsberg.cirkle.CircularList
import com.kluke.project.datastructures.CircularListIterator

val secondaryViews = CircularListIterator(
        CircularList(
                listOf(
                        RoadSpeedGraphView::class,
                        FrontCameraView::class,
                        MotionMultiGraphView::class,
                        StreetMapView::class,
                        FuelGraphView::class,
                        DataView::class,
                        TripDetailView::class,
                )
        )
)
