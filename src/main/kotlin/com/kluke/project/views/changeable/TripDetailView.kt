package com.kluke.project.views.changeable

import com.kluke.project.controllers.changeable.TripDetailController
import com.kluke.project.lcarsFont
import com.kluke.project.views.components.tripBox
import com.kluke.project.views.paddedDecimal
import javafx.scene.Parent
import tornadofx.*

class TripDetailView : View("Trip Detailed View") {
    private val controller by inject<TripDetailController>()

    override val root = hbox {
        translateX = 20.0
        translateY = 6.0

        vbox {
            tripDistanceBox()
            tripFormattedTimeBox()
        }
        vbox {
            translateX = -35.0
            tripAverageSpeed()
            tripFuelEconomyBox()
        }
    }

    private fun Parent.tripDistanceBox() =
        tripBox(
            gaugeWidth = 50.0,
            gaugeHeight = 90.0,
            topHeight = 25.0,
            unitFont = lcarsFont(32.0),
            valueFont = lcarsFont(120.0),
            titleFont = lcarsFont(28.0),
        ) {
            unitNameProperty().bind(controller.distanceInstrument.measuredUnitProperty().select { it.nameProperty() })
            valueProperty().bind(controller.distanceInstrument.property().stringBinding { (it ?: 0.0).paddedDecimal() })
            titleProperty().bind(controller.distanceInstrument.nameProperty())
            titleTextOffsetProperty().set(14.0)
        }

    private fun Parent.tripFormattedTimeBox() =
        tripBox(
            gaugeWidth = 50.0,
            gaugeHeight = 90.0,
            bottomHeight = 25.0,
            unitFont = lcarsFont(32.0),
            valueFont = lcarsFont(120.0),
            titleFont = lcarsFont(28.0),
        ) {
            unitNameProperty().bind(controller.formattedTimeInstrument.measuredUnitProperty().select { it.nameProperty() })
            valueProperty().bind(controller.formattedTimeInstrument.property())
            titleProperty().bind(controller.formattedTimeInstrument.nameProperty())
            titleTextOffsetProperty().set(10.0)
        }

    private fun Parent.tripAverageSpeed() =
        tripBox(
            gaugeWidth = 50.0,
            gaugeHeight = 90.0,
            topHeight = 25.0,
            unitFont = lcarsFont(32.0),
            valueFont = lcarsFont(120.0),
            titleFont = lcarsFont(28.0),
        ) {
            unitNameProperty().bind(controller.averageSpeedInstrument.measuredUnitProperty().select { it.nameProperty() })
            valueProperty().bind(controller.averageSpeedInstrument.property().stringBinding {
                it?.paddedDecimal(paddedStart = 5, decimalPrecision = 1) ?: "000.0"
            })
            titleProperty().bind(controller.averageSpeedInstrument.nameProperty())
            titleTextOffsetProperty().set(12.0)
        }

    private fun Parent.tripFuelEconomyBox() =
        tripBox(
            gaugeWidth = 50.0,
            gaugeHeight = 90.0,
            topHeight = 25.0,
            unitFont = lcarsFont(32.0),
            valueFont = lcarsFont(120.0),
            titleFont = lcarsFont(28.0),
        ) {
            unitNameProperty().bind(controller.fuelEconomyInstrument.measuredUnitProperty().select { it.nameProperty() })
            valueProperty().bind(controller.fuelEconomyInstrument.property().stringBinding {
                it?.paddedDecimal(paddedStart = 6) ?: "000.00"
            })
            titleProperty().bind(controller.fuelEconomyInstrument.nameProperty())
            titleTextOffsetProperty().set(12.0)
        }
}