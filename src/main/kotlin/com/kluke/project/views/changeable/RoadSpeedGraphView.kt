package com.kluke.project.views.changeable

import com.kluke.project.controllers.graphs.RoadSpeedGraphController
import com.kluke.project.viewcomponents.Styles
import com.kluke.project.viewcomponents.lcarsLineGraph
import com.kluke.project.viewcomponents.lcarsSide
import tornadofx.*

class RoadSpeedGraphView : View("RoadSpeedGraphView") {
    private val controller by inject<RoadSpeedGraphController>()
    override val root = region {
        val mainHeight = 360.0
        val mainWidth = 630.0
        val shift = -40.0

        hbox {
            lcarsSide()
            region {
                addClass(Styles.chart)
                translateX = shift

                lcarsLineGraph {
                    graphHeight = mainHeight
                    graphWidth = mainWidth

                    graphTitle = "ROADSPEED"

                    data.add(controller.series)

                }
            }
            lcarsSide {
                scaleX = -1.0
                translateX = shift * 2.0
            }
        }
    }
}