package com.kluke.project.views.changeable

import com.kluke.project.Instruments.FRONT_CAMERA
import com.kluke.project.instruments.camera.CameraInstrument
import com.kluke.project.views.components.cameraBox
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.qualifier.named
import tornadofx.*

class FrontCameraView : View("Front Camera View"), KoinComponent {
    private val instrument = get<CameraInstrument>(named(FRONT_CAMERA))

    override val root = region {
        translateX = 20.0
        cameraBox(
                gaugeWidth = 60.0,
                gaugeHeight = 185.0,
                topHeight = 10.0,
                bottomHeight = 10.0,
                leftWidth = 20.0,
                rightWidth = 20.0
        ) {
            cameraProperty().bind(instrument.property())
            encodingStatusProperty().bind(instrument.encodingStatusProperty())
            titleProperty().set("FRONT CAMERA")
        }
    }
}