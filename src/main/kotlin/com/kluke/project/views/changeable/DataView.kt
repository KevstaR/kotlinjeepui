package com.kluke.project.views.changeable

import com.kluke.project.ServiceConfiguration.SERIAL_DATA_SOURCE
import com.kluke.project.colors.LCARS_BLUE
import com.kluke.project.colors.LCARS_LIGHTORANGE
import com.kluke.project.lcarsFont
import com.kluke.project.sensors.SerialDataSource
import com.kluke.project.sensors.algorithms.serial.ECUFrame
import com.kluke.project.views.*
import javafx.scene.Parent
import javafx.scene.layout.HBox
import javafx.scene.paint.Color.WHITE
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.qualifier.named
import tornadofx.*
import kotlin.reflect.KProperty1

class DataView : View("Data View"), KoinComponent {
    private val serialDataSource = get<SerialDataSource>(named(SERIAL_DATA_SOURCE))

    private val font = lcarsFont(26.0)

    override val root = region {
        vbox {
            val verticalWidth = 15.0
            val horizontalHeight = 15.0

            val frameWidth = 50.0
            val frameHeight = 355.0
            val frameColor = WHITE

            fun Parent.lcarsOutline(hHeight: Double = horizontalHeight, op: HBox.() -> Unit = {}) = hbox {
                lcarsCorner(
                        width            = frameWidth,
                        height           = frameHeight * .2,
                        verticalWidth    = verticalWidth,
                        horizontalHeight = hHeight,
                        color            = frameColor
                )
                lcarsCap(color = WHITE, width = verticalWidth, height = hHeight)
                op()
            }

            lcarsOutline(hHeight = horizontalHeight + 20.0) {
                text {
                    font = lcarsFont(34.0)
                    text = " DIAGNOSTIC SERIAL DATA "
                    fill = LCARS_BLUE
                }
            }
            vbox {
                for (p in listOf(0.25, 0.25, 0.50)) {
                    lcarsBox(height = (frameHeight * .6) * p, width = verticalWidth, color = frameColor)
                }
            }
            lcarsOutline {
                scaleY = -1.0
            }
        }

        hbox {
            translateX = 30.0
            translateY = 60.0
            spacing = 110.0

            sidePane(
                    mapOf(
                            "MAP:"                    to ECUFrame::map,
                            "O2:"                     to ECUFrame::o2,
                            "BAROMETRIC PRESSURE:"    to ECUFrame::barometricPressure,
                            "COLD ENRICHMENT OFFSET:" to ECUFrame::coldEnrichmentOffset,
                            "CTS:"                    to ECUFrame::cts,
                            "INJ OPEN ERRORS:"        to ECUFrame::injectorOpenFlags,
                            "SHORT TERM FUEL TRIM:"   to ECUFrame::shortTermFuelTrim,
                            "LONG TERM FUEL TRIM:"    to ECUFrame::longTermFuelTrim,
                            "STARTER SIGNAL:"         to ECUFrame::starterSignal,
                    )
            )
            sidePane(
                    mapOf(
                            "IAT:"                    to ECUFrame::iat,
                            "TPS:"                    to ECUFrame::tps,
                            "EGR STATUS:"             to ECUFrame::egrStatus,
                            "BATTERY VOLTAGE:"        to ECUFrame::batteryVoltage,
                            "SPARK ADVANCE:"          to ECUFrame::sparkAdvance,
                            "EXHAUST STATUS:"         to ECUFrame::exhaustStatus,
                            "KNOCK:"                  to ECUFrame::knock,
                            "LOOP STATUS:"            to ECUFrame::loopStatus,
                            "FUEL SYNC:"              to ECUFrame::fuelSync,
                    )
            )
        }
    }

    private fun Parent.sidePane(map: Map<String, KProperty1<ECUFrame, Any>>) = gridpane {
        hgap = 40.0

        for ((key, value) in map) {
            row {
                lcarsDataKey(key)
                lcarsDataValue(value)
            }
        }
    }

    private fun Parent.lcarsDataKey(textValue: String) = text {
        font = this@DataView.font
        text = textValue
        fill = LCARS_BLUE
    }

    private fun Parent.lcarsDataValue(property: KProperty1<ECUFrame, Any>) = text {
        font = this@DataView.font
        textProperty().bind(serialDataSource.property().stringBinding { rawData ->
            // If data is null, transform to nullable string. if that is null, return "0", else value
            rawData?.let { data -> property.get(data).toStringPrecision(2) } ?: "0"
        })
        fill = LCARS_LIGHTORANGE
    }
}
