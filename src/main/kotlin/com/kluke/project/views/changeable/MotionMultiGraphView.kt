package com.kluke.project.views.changeable

import com.kluke.project.controllers.graphs.MotionMultiViewGraphController
import com.kluke.project.viewcomponents.Styles
import com.kluke.project.viewcomponents.lcarsLineGraph
import com.kluke.project.viewcomponents.lcarsSide
import javafx.beans.property.SimpleDoubleProperty
import tornadofx.*

class MotionMultiGraphView : View("MultiViewGraph") {
    private val controller by inject<MotionMultiViewGraphController>()

    private val graphHeightProperty = SimpleDoubleProperty(400.0)
    private val graphWidthProperty = SimpleDoubleProperty(630.0)

    override val root = hbox {
        val shift = -40.0
        lcarsSide()
        vbox {
            addClass(Styles.chart)

            translateX = shift
            translateY = -10.0

            spacing = -15.0

            lcarsLineGraph {
                this@lcarsLineGraph.graphTitle = "ROADSPEED"
                this@lcarsLineGraph.graphHeightProperty.bind(this@MotionMultiGraphView.graphHeightProperty / 3.0)
                this@lcarsLineGraph.graphWidthProperty.bind(this@MotionMultiGraphView.graphWidthProperty)

                data.add(controller.roadSpeedSeries)
            }
            lcarsLineGraph {
                this@lcarsLineGraph.graphTitle = "THROTTLE POSITION"
                this@lcarsLineGraph.graphHeightProperty.bind(this@MotionMultiGraphView.graphHeightProperty / 3.0)
                this@lcarsLineGraph.graphWidthProperty.bind(this@MotionMultiGraphView.graphWidthProperty)

                data.add(controller.tpsSeries)
            }
            lcarsLineGraph {
                this@lcarsLineGraph.graphTitle = "RPM"
                this@lcarsLineGraph.graphHeightProperty.bind(this@MotionMultiGraphView.graphHeightProperty / 3.0)
                this@lcarsLineGraph.graphWidthProperty.bind(this@MotionMultiGraphView.graphWidthProperty)

                data.add(controller.rpmSeries)
            }
        }
        lcarsSide {
            scaleX = -1.0
            translateX = shift * 2.0
        }
    }


}