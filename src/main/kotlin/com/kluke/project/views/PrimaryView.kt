package com.kluke.project.views

import com.kluke.project.colors.LCARS_BLUE
import com.kluke.project.colors.LCARS_CORAL
import com.kluke.project.colors.LCARS_LIGHTORANGE
import com.kluke.project.colors.LCARS_LIGHTPINK
import com.kluke.project.colors.LCARS_ORANGE
import com.kluke.project.colors.LCARS_PEACH
import com.kluke.project.colors.LCARS_PINK
import com.kluke.project.colors.LCARS_YELLOW
import com.kluke.project.controllers.EBrakeController
import com.kluke.project.events.CamButtonEvent
import com.kluke.project.events.MainButtonEvent
import com.kluke.project.events.MapButtonEvent
import com.kluke.project.lcarsFont
import com.kluke.project.lcarsFontSmaller
import javafx.event.EventHandler
import javafx.event.EventTarget
import javafx.geometry.Pos
import javafx.geometry.Pos.BOTTOM_LEFT
import javafx.geometry.Pos.TOP_LEFT
import javafx.scene.Parent
import javafx.scene.layout.Region
import javafx.scene.paint.Color
import javafx.scene.paint.Color.*
import javafx.scene.text.TextAlignment.CENTER
import tornadofx.*

class PrimaryView : View() {
    private val eBrakeController by inject<EBrakeController>()

    companion object {
        const val horizontalHeight = 15.0
        const val verticalWidth = 60.0
    }

    override val root = Region()

    init {
        with(root) {
            val stageWidth = primaryStage.width / 2.0
            val stageHeight = primaryStage.height

            vbox {
                hbox {
                    // Top Header
                    lcarsCorner(
                            width = stageWidth / 3,
                            height = stageHeight * .15
                    )
                    hbox {
                        lcarsBlockDecoration(
                                width = stageWidth / 2.6,
                                height = horizontalHeight,
                                elements = listOf(
                                        BlockAttribute(partitionPercentage = .25, color = LCARS_PEACH),
                                        BlockAttribute(partitionPercentage = .25, color = LCARS_LIGHTPINK),
                                        BlockAttribute(partitionPercentage = .50, color = LCARS_ORANGE)
                                )
                        )
                    }
                    label {
                        text = "MJ.1990 JEEP PRIMARY DISPLAY"
                        font = lcarsFont(20.0)
                        textAlignment = CENTER
                        textFill = LCARS_ORANGE
                        translateY = -5.0
                        minWidth = Region.USE_PREF_SIZE
                    }
                    path {
                        val width = 35
                        moveTo(0)
                        vlineTo(horizontalHeight)
                        hlineTo(width)
                        arcTo(radiusX = 5, radiusY = 5, x = width, y = 0)
                        hlineTo(0)
                        withColor(LCARS_YELLOW)
                    }
                }

                vbox {
                    val height = stageHeight * .6
                    val totalComponentHeight = height / 3
                    val buttonHeight = totalComponentHeight * .8
                    val boxHeight = totalComponentHeight * .2

                    lcarsButton(color = LCARS_BLUE, textValue = "MAIN", height = buttonHeight) {
                        onMouseClicked = EventHandler { fire(MainButtonEvent) }
                    }

                    lcarsBox(color = LCARS_BLUE, width = verticalWidth)
                    lcarsBox(color = LCARS_PEACH, height = boxHeight, width = verticalWidth)
                    lcarsButton(color = LCARS_PEACH, textValue = "MAP", height = buttonHeight) {
                        onMouseClicked = EventHandler { fire(MapButtonEvent) }
                    }
                    lcarsBox(color = LCARS_LIGHTPINK, height = boxHeight, width = verticalWidth)
                    lcarsButton(color = LCARS_LIGHTPINK, textValue = "CAM", alignmentPos = TOP_LEFT, height = buttonHeight) {
                        onMouseClicked = EventHandler {
                            fire(CamButtonEvent)
                        }
                    }
                }

                hbox {
                    lcarsCorner(
                            width = stageWidth / 4,
                            height = stageHeight * .15
                    ) {
                        scaleY = -1.0
                    }
                    lcarsBlockDecoration(
                            width = stageWidth / 2.1,
                            height = horizontalHeight,
                            elements = listOf(
                                    BlockAttribute(partitionPercentage = .25, color = LCARS_PEACH),
                                    BlockAttribute(partitionPercentage = .25, color = LCARS_LIGHTPINK),
                                    BlockAttribute(partitionPercentage = .50, color = LCARS_ORANGE)
                            )
                    ) {
                        translateY = 57.0
                    }
                    region {
                        translateY = -127.0
                        region {
                            translateY = 85.0
                            lcarsCorner(
                                    width = 200.0,
                                    height = 115.0,
                                    verticalWidth = 80.0,
                                    color = LCARS_CORAL
                            ) {
                                scaleY = -1.0
                                scaleX = -1.0
                            }
                            lcarsCorner(
                                    width = 100.0,
                                    height = 100.0,
                                    verticalWidth = 80.0,
                                    horizontalHeight = 30.0,
                                    color = LCARS_YELLOW
                            ) {
                                translateY = -102.0
                                translateX = 100.0
                                scaleX = -1.0
                            }
                            lcarsCap(8.0, 8.0, LCARS_PEACH) {
                                scaleX = -1.0
                                translateX = 87.0
                                translateY = -102.0
                            }
                            gridpane {
                                translateY = -102.0
                                translateX = -50.0
                                hgap = 2.0
                                vgap = 2.0

                                row {
                                    lcarsDot(color = LCARS_LIGHTORANGE)
                                    lcarsDot(color = LCARS_LIGHTPINK)
                                }
                                row {
                                    lcarsDot(color = LCARS_PINK)
                                }
                            }
                        }
                        lcarsGearGauge()
                    }
                }
            }
            lcarsSpeedometerCluster()
        }
    }

    private fun EventTarget.lcarsGearGauge() {
        this += find<GearView>().apply {
            root.translateY = -10.0
            root.translateX = 7.0
        }
    }

    private fun Parent.lcarsSpeedometerCluster() = vbox {
        translateX = 65.0
        translateY = 20.0

        gridpane {
            row {
                this += find<MainGaugeView>()
                vbox {
                    this += find<TachometerView>()
                    add(eBrakeController.region)
                }
            }
        }
        hbox {
            translateY = -90.0
            spacing = -30.0

            this += find<OdometerView>()
            this += find<TripView>()
            this += find<ShifterView>()
        }
    }

    private fun Parent.lcarsButton(textValue: String,
                                   height: Double = 75.0,
                                   width: Double = verticalWidth,
                                   color: Color,
                                   alignmentPos: Pos = BOTTOM_LEFT,
                                   op: Parent.() -> Unit) =
            stackpane {
                lcarsBox(height = height, width = width, color = color)
                text(" $textValue") {
                    alignment = alignmentPos
                    font = lcarsFontSmaller()
                    fill = BLACK
                }

                op()
            }
}
