package com.kluke.project.views

import com.kluke.project.colors.LCARS_ORANGE
import com.kluke.project.controllers.changeable.FuelController
import com.kluke.project.views.components.LowWarningRange
import com.kluke.project.views.components.verticalGaugeComponent
import tornadofx.*

class FuelView : View("Fuel View") {
    private val controller by inject<FuelController>()

    override val root = hbox {
        spacing = 1.0

        val gaugeMaxBarCount = 20
        val gaugeSpacing = 2
        val gaugeBarHeight = 7.0

        vbox {
            val h = (gaugeBarHeight + gaugeSpacing + 1) * gaugeMaxBarCount
            val c = LCARS_ORANGE
            val w = 30.0
            val vw = 15.0
            val hh = 5.0

            lcarsGaugeEdge(width = w, height = h / 2, color = c, verticalWidth = vw, horizontalHeight = hh + 16)
            lcarsGaugeEdge(width = w, height = h / 2, color = c, verticalWidth = vw, horizontalHeight = hh) { scaleY = -1.0 }
        }
        val verticalGauge = verticalGaugeComponent(barHeight = gaugeBarHeight, gaugeColorRange = LowWarningRange()) {
            name = "FUEL"
            maxBarCount = gaugeMaxBarCount
            spacing = gaugeSpacing
        }

        // Perform update on change
        controller.fillBar.onChange(verticalGauge::update)
    }
}
