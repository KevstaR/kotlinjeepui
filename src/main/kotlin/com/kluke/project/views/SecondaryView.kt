package com.kluke.project.views

import com.kluke.project.colors.LCARS_BLUE
import com.kluke.project.colors.LCARS_LIGHTPINK
import com.kluke.project.colors.LCARS_ORANGE
import com.kluke.project.colors.LCARS_PEACH
import com.kluke.project.events.CycleViewEvent
import com.kluke.project.lcarsFont
import com.kluke.project.views.changeable.ChangeableViewContainer
import com.kluke.project.views.changeable.DataView
import javafx.geometry.Pos.CENTER_RIGHT
import javafx.scene.Parent
import javafx.scene.layout.Region.USE_PREF_SIZE
import javafx.scene.text.TextAlignment.CENTER
import org.koin.core.component.KoinComponent
import tornadofx.*

class SecondaryView : View("Secondary View"), KoinComponent {

    private companion object {
        private const val VERTICAL_WIDTH = 60.0
    }

    init {
        subscribe<CycleViewEvent> {
            find<ChangeableViewContainer>().replaceChild(it.viewReference)
        }
    }

    override val root = region {
        val stageWidth = primaryStage.width / 2.0
        val stageHeight = primaryStage.height

        this += find<ChangeableViewContainer>().replaceChild<DataView>()

        // Bottom position
        region {
            translateY = 410.0
            this += find<ThrottleView>()

            // Bottom right position
            region {
                translateY = -22.0
                translateX = 408.0

                this += find<SignalView>()
            }
        }

        borderpane {
            left {
                hbox {
                    lcarsCap(color = LCARS_PEACH) { scaleX = -1.0 }
                    lcarsBlockDecoration(
                            width = stageWidth / 2.1,
                            elements = listOf(
                                    BlockAttribute(partitionPercentage = .25, color = LCARS_PEACH),
                                    BlockAttribute(partitionPercentage = .25, color = LCARS_LIGHTPINK),
                                    BlockAttribute(partitionPercentage = .50, color = LCARS_ORANGE)
                            )
                    )
                }
            }
            right {
                hbox {
                    minWidth = USE_PREF_SIZE

                    lcarsTitleText()
                    region {
                        vbox {
                            lcarsCorner(width = 193.0, height = 100.0) { scaleX = -1.0 }
                            vbox {
                                alignment = CENTER_RIGHT

                                val height = stageHeight * .6
                                val totalComponentHeight = height / 3
                                val largeHeight = totalComponentHeight * .8
                                val smallerHeight = totalComponentHeight * .2

                                lcarsBox(color = LCARS_BLUE, height = largeHeight, width = VERTICAL_WIDTH)
                                lcarsBox(color = LCARS_BLUE, width = VERTICAL_WIDTH)

                                lcarsBox(color = LCARS_PEACH, height = smallerHeight, width = VERTICAL_WIDTH)
                                lcarsBox(color = LCARS_PEACH, height = largeHeight, width = VERTICAL_WIDTH)

                                lcarsBox(color = LCARS_LIGHTPINK, height = smallerHeight, width = VERTICAL_WIDTH)
                                lcarsBox(color = LCARS_LIGHTPINK, height = largeHeight, width = VERTICAL_WIDTH)

                                lcarsBox(color = LCARS_LIGHTPINK, height = smallerHeight, width = VERTICAL_WIDTH)
                                lcarsBox(color = LCARS_LIGHTPINK, height = largeHeight, width = VERTICAL_WIDTH)
                            }
                        }
                        region {
                            translateY = 24.0
                            translateX = 66.0

                            vbox {
                                spacing = 8.0

                                this += find<FuelView>()
                                this += find<EngineTempView>()
                            }
                        }

                    }
                }
            }
        }
    }

    private fun Parent.lcarsTitleText() = text {
        text = "MJ.1990 JEEP SECONDARY DISPLAY"
        font = lcarsFont(20.0)
        textAlignment = CENTER
        fill = LCARS_ORANGE
        translateY = -3.0
    }
}
