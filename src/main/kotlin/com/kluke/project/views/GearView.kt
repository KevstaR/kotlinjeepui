package com.kluke.project.views

import com.kluke.project.CustomFont
import com.kluke.project.colors.*
import com.kluke.project.controllers.GearController
import javafx.scene.Parent
import javafx.scene.control.Label
import javafx.scene.layout.Region
import javafx.scene.paint.Color
import javafx.scene.paint.Color.*
import javafx.scene.shape.ArcType
import javafx.scene.text.TextAlignment.CENTER
import tornadofx.*

class GearView : View("Gear View") {
    private val controller by inject<GearController>()
    override val root = Region()

    private val labelFont = CustomFont.LCARS_UC.getFont(28.0)

    private val gearRatios = listOf(1.5, 1.0, 0.5, 0.25).toDegrees()
    private val colors = listOf(LCARS_ORANGE, LCARS_PINK, LCARS_PEACH, LCARS_YELLOW)

    private val gaugeLocation = 95.0

    private val gearPosition = root.pieArc(
            radius = 0.0,
            startAngle = gearRatios.take(0).sum(),
            length = gearRatios[0],
            color = LCARS_BLUE
    )

    private val gearRegion = region {
        translateX = gaugeLocation
        translateY = gaugeLocation

        add(gearPosition)
    }


    init {
        with(root) {
            stackpane {
                region {
                    translateX = gaugeLocation
                    translateY = gaugeLocation

                    // Outermost black circle
                    circle {
                        radius = 95.0
                        fill = BLACK
                    }

                    var startAngle = 0.0
                    gearRatios.forEachIndexed { index, degrees ->
                        pieArc(radius = 90.0, startAngle = startAngle, length = degrees, color = colors[index])
                        startAngle += degrees
                    }
                }

                stackpane {
                    // Ring circle
                    circle {
                        radius = 70.0
                    }

                    add(gearRegion)

                    // Blue selector circle
                    circle {
                        radius = 65.0
                        fill = LCARS_BLUE
                    }

                    // Inner black circle where text is written
                    circle {
                        radius = 55.0
                        fill = BLACK
                    }

                        lcarsLabel {
                            translateY = -20.0
                            textProperty().bind(controller.gearSensorTextBinding())
                        }
                        rectangle {
                            width = 80.0
                            height = 2.0
                            fill = LCARS_ORANGE
                        }
                        lcarsLabel {
                            translateY = 20.0
                            textProperty().bind(controller.torqueConverterSensorTextBinding())
                        }
                    }
                }
            }

        controller.gearSensorProperty().onChange {
            it ?: return@onChange

            val position = it.ordinal - 1
            val element = root.pieArc(
                    radius = 95.0,
                    startAngle = gearRatios.take(position).sum(),
                    length = gearRatios[position],
                    color = LCARS_BLUE
            )

            gearRegion.replaceChildren(element)
        }
        }

    private fun Parent.pieArc(radius: Double, startAngle: Double, length: Double, color: Color) = arc {
        radiusX = radius
        radiusY = radius

        type = ArcType.ROUND

        this.startAngle = startAngle
        this.length = length

        fill = color
        stroke = BLACK
    }

    private fun Parent.lcarsLabel(op: Label.() -> Unit) = label {
        font = labelFont
        textAlignment = CENTER
        textFill = LCARS_ORANGE

        op()
    }
}
