package com.kluke.project.views

import com.kluke.project.Instruments.ODOMETER_INSTRUMENT
import com.kluke.project.instruments.VelocityInstrument
import com.kluke.project.views.components.tripBox
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.qualifier.named
import tornadofx.*

class OdometerView : View("Odometer View"), KoinComponent {
    private val instrument = get<VelocityInstrument>(named(ODOMETER_INSTRUMENT))
    override val root = region {
        tripBox(gaugeWidth = 30.0, gaugeHeight = 50.0) {
            unitNameProperty().bind(instrument.measuredUnitProperty().select { it.nameProperty() })
            valueProperty().bind(instrument.property().stringBinding { it?.paddedDecimal(paddedStart = 8, decimalPrecision = 1) ?: "" })
            titleProperty().bind(instrument.nameProperty())
        }
    }
}