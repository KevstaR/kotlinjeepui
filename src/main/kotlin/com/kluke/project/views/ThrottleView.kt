package com.kluke.project.views

import com.kluke.project.colors.LCARS_BLUE
import com.kluke.project.colors.LCARS_ORANGE
import com.kluke.project.controllers.ThrottleViewController
import com.kluke.project.lcarsFontSmall
import com.kluke.project.map
import javafx.scene.paint.Color.TRANSPARENT
import tornadofx.*

class ThrottleView : View("Throttle View") {
    private val controller by inject<ThrottleViewController>()

    override val root = region {
        text {
//                translateY = -10.0
            text = "THROTTLE"
            font = lcarsFontSmall()
            fill = LCARS_BLUE
        }
        lcarsCorner(width = 400.0, height = 35.0, horizontalHeight = 8.0) { scaleY = -0.75 }

        region {
            translateX = 75.0
            translateY = -15.0

            val barWidth = 324.0
            val barHeight = 30.0
            val barGap = 3.0

            rectangle {
                width = barWidth
                height = barHeight
                stroke = LCARS_ORANGE
                fill = TRANSPARENT
            }
            rectangle {
                translateX = barGap
                translateY = barGap

                widthProperty().bind(controller.tpsProperty().doubleBinding {
                    it ?: return@doubleBinding 0.0

                    it.toDouble().map(input = 0..100, output = 0..(barWidth - barGap).toInt())
                })
                height = barHeight - (barGap * 2)
                stroke = LCARS_ORANGE
                fill = LCARS_ORANGE
            }
        }
    }
}
