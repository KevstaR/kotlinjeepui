package com.kluke.project.views

import com.kluke.project.CustomFont
import com.kluke.project.Instruments.ACCELEROMETER_INSTRUMENT
import com.kluke.project.Instruments.SPEEDOMETER_INSTRUMENT
import com.kluke.project.colors.LCARS_BLUE
import com.kluke.project.colors.LCARS_LIGHTORANGE
import com.kluke.project.colors.LCARS_LIGHTPINK
import com.kluke.project.colors.LCARS_ORANGE
import com.kluke.project.colors.LCARS_PINK
import com.kluke.project.instruments.VelocityInstrument
import com.kluke.project.lcarsFontSmall
import com.kluke.project.lcarsFontSmaller
import com.kluke.project.map
import javafx.geometry.Pos.CENTER
import javafx.scene.Parent
import javafx.scene.layout.HBox
import javafx.scene.layout.Region
import javafx.scene.paint.Color.WHITE
import javafx.scene.paint.Paint
import javafx.scene.shape.Polygon
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.qualifier.named
import tornadofx.*

class MainGaugeView : View("MainGaugeView"), KoinComponent {
    private val speedometerInstrument = get<VelocityInstrument>(named(SPEEDOMETER_INSTRUMENT))
    private val accelerometerInstrument = get<VelocityInstrument>(named(ACCELEROMETER_INSTRUMENT))

    override val root = Region()

    private companion object {
        private const val CORNER_EDGE_HEIGHT = 60.0
        private const val HEIGHT = 200.0
        private const val V_WIDTH = 20.0
    }

    init {
        with(root) {
            accelerationValueText()
            accelerationAbbreviationText()

            hbox {
                leftSideNumberLine()
                lcarsSide(needle = speedometerNeedle())

                vbox {
                    alignment = CENTER

                    speedometerInstrumentSpeedUnitName()
                    speedometerValueText()
                    speedometerInstrumentName()
                }

                lcarsSide(needle = accelerationNeedle()) { scaleX = -1.0 }
                rightSideNumberLine()
            }
        }
    }

    private fun accelerationValueText() {
        text {
            x = 390.0
            y = 40.0
            textProperty().bind(accelerometerInstrument.property().stringBinding {
                it!!.makeTextWithSign(padding = 2)
            })
            fill = LCARS_BLUE
            fontProperty().set(lcarsFontSmall())
        }
    }

    private fun accelerationAbbreviationText() {
        text {
            x = 390.0
            y = 277.0
            textProperty().bind(accelerometerInstrument.measuredUnitProperty().stringBinding { it!!.abbreviation })
            fill = LCARS_BLUE
            font = lcarsFontSmaller()
        }
    }

    private fun HBox.leftSideNumberLine() = sideNumberLine(range = 0..10) { increment: Int ->
        hbox {
            text {
                text = increment.makeTextWithSign(padding = 3)
                fill = increment.colorSpeedometerTextByRange()
                font = lcarsFontSmaller()
            }
            rectangle {
                translateY = 8.0
                height = 2.0
                width = 6.0
                withColor(LCARS_ORANGE)
            }
        }
    }

    private fun HBox.rightSideNumberLine() = sideNumberLine(range = -5..5) { increment: Int ->
        hbox {
            rectangle {
                translateY = 8.0
                height = 2.0
                width = 6.0
                withColor(LCARS_ORANGE)
            }
            text {
                text = increment.makeTextWithSign(padding = 2)
                fill = increment.colorAccelerationTextByRange()
                font = CustomFont.LCARS_UC.getFont(18.0)
            }
        }
    }

    private fun Parent.sideNumberLine(
            step: Int = 10,
            range: IntProgression,
            needleSliderComponents: Parent.(Int) -> Unit) =
            vbox {
                translateY = 35.0

                for (i in range.reversed()) {
                    val increment = i * step

                    needleSliderComponents(increment)
                }
            }

    private fun Parent.speedometerInstrumentName() =
            text {
                translateY = -90.0
                textProperty().bind(speedometerInstrument.nameProperty())
                fill = LCARS_BLUE
                font = lcarsFontSmall()
            }

    private fun Parent.speedometerInstrumentSpeedUnitName() =
            text {
                translateY = -2.0
                textProperty().bind(speedometerInstrument.measuredUnitProperty().stringBinding { it!!.name })
                fill = LCARS_BLUE
                font = lcarsFontSmall()
            }

    private fun Parent.speedometerNeedle() =
            polygon(24.0, 12.0, 0.0, 6.0, 24.0, 0.0) {
                fill = WHITE
                scaleY = -1.0

                translateYProperty().bind(speedometerInstrument.property().doubleBinding {
                    val min = 0.0
                    val max = 100.0

                    it!!.toDouble()
                            .coerceIn(minimumValue = min, maximumValue = max)
                            .map(inMin = min, inMax = max, outMin = 210.0, outMax = 0.0)
                }.minus(20))
            }

    private fun Parent.accelerationNeedle() =
            polygon(24.0, 12.0, 0.0, 6.0, 24.0, 0.0) {
                fill = WHITE

                translateYProperty().bind(accelerometerInstrument.property().doubleBinding {
                    val min = -50.0
                    val max = 50.0

                    it!!.toDouble()
                            .coerceIn(minimumValue = min, maximumValue = max)
                            .map(inMin = min, inMax = max, outMin = 210.0, outMax = 0.0)
                }.minus(20))
            }

    private fun Int.colorSpeedometerTextByRange(): Paint =
            when (this) {
                in 70..100 -> LCARS_ORANGE
                in 0..30 -> LCARS_PINK
                else -> LCARS_BLUE
            }

    private fun Int.colorAccelerationTextByRange(): Paint =
            when (this) {
                in 30..50 -> LCARS_ORANGE
                in -30 downTo -50 -> LCARS_PINK
                else -> LCARS_BLUE
            }

    private fun Parent.speedometerValueText() =
            text {
                translateY = -40.0
                textProperty().bind(speedometerInstrument.property().stringBinding {
                    it?.toStringWithPaddedStart(pad = 3)
                })
                fillProperty().set(LCARS_BLUE)
                font = CustomFont.LCARS_UC.getFont(300.0)
            }

    private fun Parent.lcarsSide(stackHeight: Double = HEIGHT,
                                 verticalWidth: Double = V_WIDTH,
                                 needle: Polygon,
                                 op: Parent.() -> Unit = {}) =
            stackpane {
                val thisSpacing = 2.0
                vbox {
                    val fitWidth = (verticalWidth / 2.0) - thisSpacing
                    lcarsGaugeEdge(
                            width = 60.0,
                            height = CORNER_EDGE_HEIGHT,
                            verticalWidth = V_WIDTH,
                            color = LCARS_ORANGE
                    )
                    hbox {
                        spacing = thisSpacing
                        vbox {
                            spacing = thisSpacing
                            val halfHeight = stackHeight / 2.0
                            numberLineBox(halfHeight)
                            numberLineBox(halfHeight)
                        }
                        lcarsDecoratorBoxes(fitWidth, stackHeight - 20)
                        add(needle)
                    }
                    lcarsGaugeEdge(
                            width = 60.0,
                            height = CORNER_EDGE_HEIGHT,
                            verticalWidth = V_WIDTH,
                            color = LCARS_ORANGE
                    ) {
                        scaleY = -1.0
                        translateY = -28.0
                    }
                }
                op()
            }

    private fun Parent.numberLineBox(rectHeight: Double) {
        rectangle {
            translateX = 1.0
            translateY = -14.0
            width = 8.0
            height = rectHeight
            withColor(LCARS_ORANGE)
        }
    }

    private fun Parent.lcarsDecoratorBoxes(fitWidth: Double, stackHeight: Double) =
            vbox {
                spacing = 1.0
                listOf(
                        2.0 to LCARS_LIGHTPINK,
                        4.0 to LCARS_PINK,
                        4.0 to LCARS_LIGHTORANGE
                ).forEach { pair ->
                    rectangle {
                        width = fitWidth
                        height = (stackHeight - 10.0) / pair.first
                        translateX = 1.0
                        withColor(pair.second)
                    }
                }
            }
}