package com.kluke.project.views

import com.kluke.project.Instruments.TRIP_DISTANCE_INSTRUMENT
import com.kluke.project.instruments.trip.TripInstrument
import com.kluke.project.views.components.tripBox
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.qualifier.named
import tornadofx.View
import tornadofx.region
import tornadofx.select
import tornadofx.stringBinding

class TripView : View("Trip View"), KoinComponent {
    private val distanceInstrument = get<TripInstrument<Double>>(named(TRIP_DISTANCE_INSTRUMENT))
    override val root = region {
        tripBox(gaugeHeight = 50.0, gaugeWidth = 30.0) {
            unitNameProperty().bind(distanceInstrument.measuredUnitProperty().select { it.nameProperty() })
            valueProperty().bind(distanceInstrument.property().stringBinding { it?.paddedDecimal() ?: "" })
            titleProperty().bind(distanceInstrument.nameProperty())
        }
    }
}
