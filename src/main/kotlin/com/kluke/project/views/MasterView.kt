package com.kluke.project.views

import javafx.scene.layout.Background.EMPTY
import tornadofx.*

class MasterView : View("Master View") {
    override val root = hbox {
        background = EMPTY
        this += find<PrimaryView>()
        this += find<SecondaryView>()
    }
}
