package com.kluke.project.views

import com.kluke.project.colors.LCARS_PEACH
import javafx.beans.property.SimpleObjectProperty
import javafx.scene.paint.Color
import javafx.scene.shape.Shape
import tornadofx.*
import java.lang.NumberFormatException
import kotlin.math.abs
import kotlin.reflect.KClass

fun Number.toStringWithPaddedStart(pad: Int, paddingChar: Char = '0') = toInt().toString().padStart(pad, paddingChar)
fun Any.toStringPrecision(precision: Int): String {
    if (precision < 0) throw NumberFormatException("Unable to apply negative precision.")

    val string = toString()

    val lastIndexOfDecimalChar = string.lastIndexOf('.')
    if (lastIndexOfDecimalChar == -1) return string

    val decimals = string.lastIndex - lastIndexOfDecimalChar
    if (decimals - precision >= 0) return string.substring(0, lastIndexOfDecimalChar + 1 + precision)

    return string
}

fun Any.paddedDecimal(paddedStart: Int = 7, decimalPrecision: Int = 2) =
    toStringPrecision(decimalPrecision).padStart(paddedStart, '0').padEnd(decimalPrecision, '0')

fun Number.makeTextWithSign(padding: Int, paddingChar: Char = '0'): String {
    val int = toInt()
    val stringBuilder = if (int >= 0) StringBuilder(" ") else StringBuilder("-")
    return stringBuilder.append(abs(int).toString().padStart(padding, paddingChar)).toString()
}

fun List<Double>.toDegrees(): List<Double> = map { (it / sum()) * 360.0 }

fun Shape.withColor(color: Color) {
    fill = color
    stroke = color.darker()
}

fun Shape.withColorProperty(colorProperty: SimpleObjectProperty<Color>) {
    fillProperty().bind(colorProperty)
    strokeProperty().bind(colorProperty.objectBinding { it?.darker() })
}

fun ArrayList<Color>.getRandomUniqueColor(): Color =
        if (isNotEmpty()) removeAt(indices.random())
        else LCARS_PEACH

fun View.replaceChild(viewRef: KClass<out View>): View {
    root.getChildList()?.clear()
    this += find(viewRef)
    return this
}

inline fun <reified T : View> View.replaceChild(): View = replaceChild(T::class)
