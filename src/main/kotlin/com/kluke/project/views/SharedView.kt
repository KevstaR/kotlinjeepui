package com.kluke.project.views

import com.kluke.project.colors.LCARS_BLUE
import javafx.scene.Parent
import javafx.scene.paint.Color
import javafx.scene.shape.Path
import javafx.scene.shape.Rectangle
import tornadofx.*

fun Parent.lcarsCap(width: Double = 30.0,
                    height: Double = 15.0,
                    color: Color,
                    op: Path.() -> Unit = {}) =
        path {
            moveTo()
            vlineTo(height)
            hlineTo(width)
            arcTo {
                val radius = 5.0
                radiusX = radius
                radiusY = radius
                x = width
                y = 0.0
            }
            hlineTo(0.0)
            withColor(color)

            op()
        }

fun Parent.lcarsCorner(width: Double,
                       height: Double,
                       verticalWidth: Double = 60.0,
                       horizontalHeight: Double = 15.0,
                       color: Color = LCARS_BLUE,
                       op: Path.() -> Unit = {}) =
        path {
            val outerArc = 32
            val innerArc = 20
            moveTo(0, outerArc)
            vlineTo(height)
            hlineTo(verticalWidth)
            vlineTo(horizontalHeight + innerArc)
            arcTo(
                    radiusX = innerArc,
                    radiusY = innerArc,
                    xAxisRotation = 0,
                    x = innerArc + verticalWidth,
                    y = horizontalHeight,
                    largeArcFlag = false,
                    sweepFlag = true
            )
            hlineTo(width)
            vlineTo(0)
            hlineTo(outerArc)
            arcTo(
                    radiusX = outerArc,
                    radiusY = outerArc,
                    xAxisRotation = 0,
                    x = 0,
                    y = outerArc,
                    largeArcFlag = false,
                    sweepFlag = false
            )
            vlineTo(outerArc)
            withColor(color)

            op()
        }

fun Parent.lcarsGaugeEdge(width: Double,
                          height: Double,
                          verticalWidth: Double = 10.0,
                          horizontalHeight: Double = 10.0,
                          color: Color = LCARS_BLUE,
                          op: Path.() -> Unit = {}) =
        path {
            val outerArc = 25
            val innerArc = 15
            val capHeight = 15

            moveTo(0, outerArc)
            vlineTo(height - capHeight)
            hlineTo(verticalWidth / 2)
            vlineTo(height)
            hlineTo(verticalWidth)
            vlineTo(horizontalHeight + innerArc)
            arcTo(
                    radiusX = innerArc,
                    radiusY = innerArc,
                    xAxisRotation = 0,
                    x = innerArc + verticalWidth,
                    y = horizontalHeight,
                    largeArcFlag = false,
                    sweepFlag = true
            )
            hlineTo(width)
            vlineTo(0)
            hlineTo(outerArc)
            arcTo(
                    radiusX = outerArc,
                    radiusY = outerArc,
                    xAxisRotation = 0,
                    x = 0,
                    y = outerArc,
                    largeArcFlag = false,
                    sweepFlag = false
            )
            vlineTo(outerArc)

            withColor(color)

            op()
        }

fun Parent.lcarsThrottleEdge(width: Double,
                          height: Double,
                          verticalWidth: Double = 10.0,
                          horizontalHeight: Double = 10.0,
                          color: Color = LCARS_BLUE,
                          op: Path.() -> Unit = {}) =
        path {
            val outerArc = 25
            val innerArc = 15
            
            moveTo(0, outerArc)
            vlineTo(height)
            hlineTo(verticalWidth)
            vlineTo(horizontalHeight + innerArc)
            arcTo(
                    radiusX = innerArc,
                    radiusY = innerArc,
                    xAxisRotation = 0,
                    x = innerArc + verticalWidth,
                    y = horizontalHeight,
                    largeArcFlag = false,
                    sweepFlag = true
            )
            hlineTo(width)
            vlineTo(0)
            hlineTo(outerArc)
            arcTo(
                    radiusX = outerArc,
                    radiusY = outerArc,
                    xAxisRotation = 0,
                    x = 0,
                    y = outerArc,
                    largeArcFlag = false,
                    sweepFlag = false
            )
            vlineTo(outerArc)

            withColor(color)

            op()
        }

fun Parent.lcarsDot(width: Double = 50.0,
                    height: Double = 15.0,
                    color: Color) = lcarsDot(width, height) { withColor(color) }

fun Parent.lcarsDot(width: Double = 50.0,
                    height: Double = 15.0,
                    op: Rectangle.() -> Unit = {}) =
        rectangle {
            arcHeight = 15.0
            arcWidth = 15.0
            this.width = width
            this.height = height

            op()
        }

fun Parent.lcarsBlockDecoration(width: Double,
                                height: Double = 15.0,
                                elements: Collection<BlockAttribute>,
                                op: Path.() -> Unit = {}) {
    val colors = elements.map { it.color }.toCollection(ArrayList())
    for (element in elements) {
        path {
            moveTo()
            hlineTo(width * element.partitionPercentage)
            vlineTo(height)
            hlineTo(0)
            vlineTo(0)
            withColor(colors.getRandomUniqueColor())

            op()
        }
    }
}

data class BlockAttribute(val partitionPercentage: Double, val color: Color)

fun Parent.lcarsBox(height: Double = 20.0, width: Double = 20.0,
                            color: Color, op: Path.() -> Unit = {}) =
        path {
            moveTo(0.0, 0.0)
            hlineTo(width)
            vlineTo(height)
            hlineTo(0.0)
            vlineTo(0.0)

            withColor(color)

            op()
        }