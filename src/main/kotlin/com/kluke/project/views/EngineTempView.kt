package com.kluke.project.views

import com.kluke.project.colors.LCARS_ORANGE
import com.kluke.project.controllers.changeable.EngineTempController
import com.kluke.project.views.components.HighWarningRange
import com.kluke.project.views.components.verticalGaugeComponent
import tornadofx.*

class EngineTempView : View("Engine Temp View") {
    private val controller by inject<EngineTempController>()

    override val root = hbox {
        spacing = 1.0

        val gaugeMaxBarCount = 20
        val gaugeSpacing = 2
        val gaugeBarHeight = 6.0

        vbox {
            val h = (gaugeBarHeight + gaugeSpacing + 1) * gaugeMaxBarCount
            val c = LCARS_ORANGE
            val w = 30.0
            val vw = 15.0
            val hh = 5.0

            lcarsGaugeEdge(width = w, height = h / 2, color = c, verticalWidth = vw, horizontalHeight = hh + 16)
            lcarsGaugeEdge(width = w, height = h / 2 + 20, color = c, verticalWidth = vw, horizontalHeight = hh + 16) { scaleY = -1.0 }
        }

        val verticalGauge = verticalGaugeComponent(barHeight = gaugeBarHeight, gaugeColorRange = HighWarningRange()) {
            name = "TEMP"
            maxBarCount = gaugeMaxBarCount
            spacing = gaugeSpacing

            rawValueProperty.bind(binding())
        }

        controller.fillBar.onChange(verticalGauge::update)
    }

    private fun binding() = controller.temperature().stringBinding {
        it ?: return@stringBinding "000"
        it.toInt().toStringWithPaddedStart(3)
    }
}
