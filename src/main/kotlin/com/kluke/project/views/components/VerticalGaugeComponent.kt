package com.kluke.project.views.components

import com.kluke.project.colors.LCARS_BLUE
import com.kluke.project.lcarsFontSmall
import com.kluke.project.views.withColor
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.scene.Parent
import javafx.scene.layout.Region
import javafx.scene.paint.Color
import javafx.scene.shape.Rectangle
import javafx.scene.shape.Shape
import javafx.scene.text.TextAlignment.CENTER
import tornadofx.*

/**
 * Creates a custom vertical bar display that empties from top to bottom.
 * The default vbox and hbox do not have this capability.
 */
class VerticalGaugeComponent(
        private val barWidth: Double,
        private val barHeight: Double,
        private val gaugeColorRange: GaugeColorRange) : Region() {

    val textFillProperty = SimpleObjectProperty(LCARS_BLUE)
    var textFill: Color by textFillProperty

    val nameProperty = SimpleStringProperty("GAUGE")
    var name: String by nameProperty

    val maxBarCountProperty = SimpleIntegerProperty(20)
    var maxBarCount by maxBarCountProperty

    val spacingProperty = SimpleIntegerProperty(2)
    var spacing by spacingProperty

    val rawValueProperty = SimpleStringProperty("")
    val rawValue: String by rawValueProperty

    private val bar = Region().apply { translateY = 16.0 /* Offset to align with trim */ }

    init {
        text {
            val textPosition = maxBarCount * barHeight
            translateY = -textPosition - 20 /* Offset to align with trim */

            font = lcarsFontSmall()
            textProperty().bind(nameProperty)
            fillProperty().bind(textFillProperty)
            textAlignment = CENTER
        }
        // Shift entire region by the max bar count.
        translateY = -calculatePosition(maxBarCount)

        children += bar

        text {
            val textPosition = maxBarCount * barHeight
            translateY = -textPosition + 162 /* Offset to align with trim */
            translateX = 2.0

            font = lcarsFontSmall()
            textProperty().bind(rawValueProperty)
            fillProperty().bind(textFillProperty)
            textAlignment = CENTER
        }
    }

    fun update(barCount: Int) {
        val children = bar.getChildList() ?: return
        children.clear()

        for (position in 0..barCount) children += lcarsRectangle(color = gaugeColorRange.apply(barCount), position)
    }

    private fun lcarsRectangle(color: Color, position: Int): Shape = Rectangle().apply {
        translateY = calculatePosition(position)
        height = barHeight
        width = barWidth
        withColor(color)
    }

    private fun calculatePosition(position: Int) = position * -(barHeight + spacing)
}

fun Parent.verticalGaugeComponent(barWidth: Double = 27.0,
                                  barHeight: Double = 6.0,
        gaugeColorRange: GaugeColorRange = NoColorRange(),
                                  op: VerticalGaugeComponent.() -> Unit = {}
) = VerticalGaugeComponent(barWidth, barHeight, gaugeColorRange).attachTo(this, op)