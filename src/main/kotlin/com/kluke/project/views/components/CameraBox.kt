package com.kluke.project.views.components

import com.kluke.project.colors.LCARS_ORANGE
import com.kluke.project.instruments.camera.Status
import com.kluke.project.instruments.camera.Status.COMPLETED
import com.kluke.project.instruments.camera.Status.IDLE
import com.kluke.project.lcarsFont
import com.kluke.project.views.lcarsGaugeEdge
import javafx.animation.Animation.Status.RUNNING
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.event.EventTarget
import javafx.scene.image.Image
import javafx.scene.layout.BorderPane
import javafx.scene.paint.Color.TRANSPARENT
import javafx.scene.shape.Rectangle
import javafx.scene.text.Font
import javafx.scene.text.Text
import tornadofx.*

class CameraBox(
        private val gaugeWidth: Double,
        private val gaugeHeight: Double,
        private val topHeight: Double,
        private val bottomHeight: Double,
        private val leftWidth: Double,
        private val rightWidth: Double,
        private val titleFont: Font,
) : BorderPane() {
    private val titleProperty: StringProperty = SimpleStringProperty()
    fun titleProperty(): StringProperty = titleProperty

    private val encodingStatusProperty: ObjectProperty<Status> = SimpleObjectProperty(IDLE)
    fun encodingStatusProperty(): ObjectProperty<Status> = encodingStatusProperty

    private val cameraProperty: ObjectProperty<Image> = SimpleObjectProperty()
    fun cameraProperty(): ObjectProperty<Image> = cameraProperty

    private val encodingDisplayText = Text().apply {
        fill = LCARS_ORANGE
        textProperty().bind(encodingStatusProperty().stringBinding { it?.status ?: "" })
        translateY = 340.0
        translateX = 420.0
        font = lcarsFont(32.0)
    }

    init {
        val outlineTightness = -18.0
        left {
            vbox {
                lcarsGaugeEdge(
                        width = gaugeWidth,
                        height = gaugeHeight,
                        color = LCARS_ORANGE,
                        horizontalHeight = topHeight,
                        verticalWidth = leftWidth
                )
                lcarsGaugeEdge(
                        width = gaugeWidth,
                        height = gaugeHeight,
                        color = LCARS_ORANGE,
                        horizontalHeight = bottomHeight,
                        verticalWidth = leftWidth
                ) {
                    scaleY = -1.0
                }
            }
        }
        right {
            vbox {
                translateX = (outlineTightness * 2) - gaugeWidth + leftWidth
                lcarsGaugeEdge(
                        width = gaugeWidth,
                        height = gaugeHeight,
                        color = LCARS_ORANGE,
                        horizontalHeight = topHeight,
                        verticalWidth = rightWidth
                ) {
                    scaleX = -1.0
                }
                lcarsGaugeEdge(
                        width = gaugeWidth,
                        height = gaugeHeight,
                        color = LCARS_ORANGE,
                        horizontalHeight = bottomHeight,
                        verticalWidth = rightWidth
                ) {
                    scaleX = -1.0
                    scaleY = -1.0
                }
            }
        }
        center {
            region {
                imageview {
                    imageProperty().bind(cameraProperty)

                    translateX = -gaugeWidth + leftWidth + 2
                    translateY = topHeight + 4

                    // 4:3
                    val cameraViewWidth = 586.0
                    val cameraViewHeight = 440.0
                    fitWidth = cameraViewWidth
                    fitHeight = cameraViewHeight
                    prefWidth(cameraViewWidth)
                    prefHeight(cameraViewHeight)
                    isPreserveRatio = true

                    clip = Rectangle(x, y, cameraViewWidth, cameraViewHeight - 94).apply {
                        arcWidth = 32.0
                        arcHeight = 32.0
                    }
                }
                text {
                    fill = LCARS_ORANGE
                    textProperty().bind(titleProperty)
                    font = titleFont
                    translateY = 50.0
                    translateX = -20.0
                }
                getChildren().add(encodingDisplayText)
            }
        }

        timeline(play = false) {
            keyframe(500.millis) {
                keyvalue(encodingDisplayText.fillProperty(), TRANSPARENT)
            }

            isAutoReverse = true
            cycleCount = 5

            encodingStatusProperty.onChange { if (it == COMPLETED && status != RUNNING) playFromStart() }
        }


    }
}

fun EventTarget.cameraBox(
        gaugeWidth: Double = 30.0,
        gaugeHeight: Double = 50.0,
        topHeight: Double = 10.0,
        bottomHeight: Double = 10.0,
        leftWidth: Double = 10.0,
        rightWidth: Double = 10.0,
        titleFont: Font = lcarsFont(32.0),
        op: CameraBox.() -> Unit = {}
) = CameraBox(
        gaugeWidth,
        gaugeHeight,
        topHeight,
        bottomHeight,
        leftWidth,
        rightWidth,
        titleFont,
).attachTo(this, op)