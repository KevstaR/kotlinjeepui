package com.kluke.project.views.components

import com.kluke.project.lcarsFontSmaller
import javafx.scene.layout.StackPane
import javafx.scene.paint.Color
import tornadofx.rectangle
import tornadofx.stackpane
import tornadofx.text

class EBrakeIndicator : StackPane() {
    init {
        stackpane {
            rectangle {
                arcHeight = 15.0
                arcWidth = 15.0
                width = 250.0
                height = 15.0
                fill = Color.TRANSPARENT
                stroke = Color.RED
                strokeWidth = 2.0
            }
            text("EMERGENCY BRAKE ENGAGED!") {
                translateY = 1.0
                font = lcarsFontSmaller()
                fill = Color.RED
            }
        }
    }
}