package com.kluke.project.views.components

import com.kluke.project.colors.LCARS_BLUE
import com.kluke.project.colors.LCARS_YELLOW
import javafx.scene.paint.Color
import javafx.scene.paint.Color.RED

interface GaugeColorRange {
    fun apply(barCount: Int): Color
}

class NoColorRange : GaugeColorRange {
    override fun apply(barCount: Int): Color = LCARS_BLUE
}

class LowWarningRange : GaugeColorRange {
    override fun apply(barCount: Int): Color = when (barCount) {
        in 6..10 -> LCARS_YELLOW
        in 0..5 -> RED
        else -> LCARS_BLUE
    }
}

class HighWarningRange : GaugeColorRange {
    override fun apply(barCount: Int): Color = when (barCount) {
        in 15..20 -> RED
        else -> LCARS_BLUE
    }
}