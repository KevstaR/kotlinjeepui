package com.kluke.project.views.components

import com.kluke.project.colors.LCARS_BLUE
import com.kluke.project.colors.LCARS_YELLOW
import com.kluke.project.lcarsFontSmall
import com.kluke.project.views.lcarsDot
import com.pi4j.io.gpio.PinState
import com.pi4j.io.gpio.PinState.HIGH
import javafx.application.Platform.runLater
import javafx.beans.property.ObjectProperty
import javafx.scene.Parent
import javafx.scene.layout.Region
import javafx.scene.layout.StackPane
import javafx.scene.paint.Color
import javafx.scene.shape.Rectangle
import tornadofx.*

class SignalBox(private val displayText: String,
                private val color: Color = LCARS_YELLOW,
                private val statusProperty: ObjectProperty<PinState>
) : Region() {
    private val signalContainer = StackPane()

    init {
        hbox {
            spacing = 4.0

            statusProperty.onChange {
                it ?: return@onChange

                runLater {
                    clearSignalContainer()
                    if (it == HIGH) addSignalMarker()
                }
            }

            text {
                text = displayText
                fill = LCARS_BLUE
                font = lcarsFontSmall()
            }
            stackpane {
                lcarsDot(width = 60.0, height = 18.0) {
                    stroke = color
                    strokeWidth = 2.0
                }
                this@stackpane += signalContainer
            }
        }
    }

    private fun clearSignalContainer() = signalContainer.getChildList()?.clear()
    private fun addSignalMarker() { signalContainer += dot() }
    private fun dot() = Rectangle().apply {
                arcHeight = 15.0
                arcWidth = 15.0
                width = 54.0
                height = 12.0
                fill = color
            }
}

fun Parent.signalBox(displayText: String,
                     color: Color = LCARS_YELLOW,
                     statusProperty: ObjectProperty<PinState>,
                     op: SignalBox.() -> Unit = {}) = SignalBox(displayText, color, statusProperty).attachTo(this, op)