package com.kluke.project.views.components

import com.kluke.project.colors.LCARS_BLUE
import com.kluke.project.colors.LCARS_ORANGE
import com.kluke.project.lcarsFont
import com.kluke.project.lcarsFontSmall
import com.kluke.project.views.lcarsGaugeEdge
import javafx.beans.property.*
import javafx.event.EventTarget
import javafx.geometry.Pos.CENTER
import javafx.scene.Parent
import javafx.scene.layout.BorderPane
import javafx.scene.paint.Color
import javafx.scene.text.Font
import tornadofx.*

class TripBox(
    private val gaugeWidth: Double,
    private val gaugeHeight: Double,
    private val topHeight: Double,
    private val bottomHeight: Double,
    private val leftWidth: Double,
    private val rightWidth: Double,
    private val unitFont: Font,
    private val valueFont: Font,
    private val titleFont: Font,
) : BorderPane() {
    private val unitNameProperty: StringProperty = SimpleStringProperty()
    fun unitNameProperty(): StringProperty = unitNameProperty

    private val valueProperty: StringProperty = SimpleStringProperty()
    fun valueProperty(): StringProperty = valueProperty

    private val titleProperty: StringProperty = SimpleStringProperty()
    fun titleProperty(): StringProperty = titleProperty

    private val textColorProperty: ObjectProperty<Color> = SimpleObjectProperty(LCARS_BLUE)
    fun textColorProperty(): ObjectProperty<Color> = textColorProperty

    private val centerSpacingProperty: DoubleProperty = SimpleDoubleProperty(-12.0)
    fun centerSpacingProperty(): DoubleProperty = centerSpacingProperty

    private val unitNameTextOffsetProperty: DoubleProperty = SimpleDoubleProperty()
    fun unitNameTextOffsetProperty(): DoubleProperty = unitNameTextOffsetProperty

    private val valueTextOffsetProperty: DoubleProperty = SimpleDoubleProperty()
    fun valueTextOffsetProperty(): DoubleProperty = valueTextOffsetProperty

    private val titleTextOffsetProperty: DoubleProperty = SimpleDoubleProperty()
    fun titleTextOffsetProperty(): DoubleProperty = titleTextOffsetProperty

    init {
        val outlineTightness = -18.0
        left {
            vbox {
                lcarsGaugeEdge(
                    width = gaugeWidth,
                    height = gaugeHeight,
                    color = LCARS_ORANGE,
                    horizontalHeight = topHeight,
                    verticalWidth = leftWidth
                )
                lcarsGaugeEdge(
                    width = gaugeWidth,
                    height = gaugeHeight,
                    color = LCARS_ORANGE,
                    horizontalHeight = bottomHeight,
                    verticalWidth = leftWidth
                ) {
                    scaleY = -1.0
                }
            }
        }
        right {
            vbox {
                translateX = outlineTightness * 2
                lcarsGaugeEdge(
                    width = gaugeWidth,
                    height = gaugeHeight,
                    color = LCARS_ORANGE,
                    horizontalHeight = topHeight,
                    verticalWidth = rightWidth
                ) {
                    scaleX = -1.0
                }
                lcarsGaugeEdge(
                    width = gaugeWidth,
                    height = gaugeHeight,
                    color = LCARS_ORANGE,
                    horizontalHeight = bottomHeight,
                    verticalWidth = rightWidth
                ) {
                    scaleX = -1.0
                    scaleY = -1.0
                }
            }
        }
        center {
            vbox {
                translateX = outlineTightness
                translateY = -4.0

                alignment = CENTER
                spacingProperty().bind(centerSpacingProperty)

                unitName()
                displayedValue()
                title()
            }
        }
    }

    private fun Parent.unitName() = text {
        translateYProperty().bind(unitNameTextOffsetProperty)
        textProperty().bind(unitNameProperty)
        fillProperty().bind(textColorProperty)
        font = unitFont
    }

    private fun Parent.displayedValue() = text {
        translateYProperty().bind(valueTextOffsetProperty)
        textProperty().bind(valueProperty)
        fillProperty().bind(textColorProperty)
        font = valueFont
    }

    private fun Parent.title() = text {
        translateYProperty().bind(titleTextOffsetProperty)
        textProperty().bind(titleProperty)
        fillProperty().bind(textColorProperty)
        font = titleFont
    }
}

fun EventTarget.tripBox(
    gaugeWidth: Double = 30.0,
    gaugeHeight: Double = 50.0,
    topHeight: Double = 10.0,
    bottomHeight: Double = 10.0,
    leftWidth: Double = 10.0,
    rightWidth: Double = 10.0,
    unitFont: Font = lcarsFontSmall(),
    valueFont: Font = lcarsFont(72.0),
    titleFont: Font = lcarsFontSmall(),
    op: TripBox.() -> Unit = {}
) = TripBox(gaugeWidth, gaugeHeight, topHeight, bottomHeight, leftWidth, rightWidth, unitFont, valueFont, titleFont).attachTo(this, op)