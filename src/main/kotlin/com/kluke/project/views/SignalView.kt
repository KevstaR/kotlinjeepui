package com.kluke.project.views

import com.kluke.project.colors.LCARS_BLUE
import com.kluke.project.colors.LCARS_LIGHTPINK
import com.kluke.project.controllers.SignalViewController
import com.kluke.project.views.components.signalBox
import tornadofx.*

class SignalView : View("Signal View") {
    private val controller by inject<SignalViewController>()

    override val root = gridpane {
        hgap = 10.0
        row {
            signalBox("L SIGNAL:", statusProperty = controller.leftSignalSensor.property())
            signalBox("R SIGNAL:", statusProperty = controller.rightSignalSensor.property())
        }
        row {
            signalBox("   LIGHTS:", LCARS_BLUE, statusProperty = controller.lightsSignalSensor.property())
            signalBox(" BRIGHTS:", LCARS_LIGHTPINK, statusProperty = controller.brightsSignalSensor.property())
        }
    }
}
