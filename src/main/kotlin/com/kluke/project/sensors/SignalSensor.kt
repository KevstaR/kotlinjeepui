package com.kluke.project.sensors

import com.pi4j.io.gpio.GpioPinDigitalInput
import com.pi4j.io.gpio.PinState
import com.pi4j.io.gpio.event.GpioPinListenerDigital
import javafx.application.Platform.runLater
import javafx.beans.property.SimpleObjectProperty


class SignalSensor(pin: GpioPinDigitalInput) : Sensor<PinState> {
    private val sensorValue = SimpleObjectProperty<PinState>()

    init {
        pin.addListener(
                GpioPinListenerDigital {
                    runLater {
                        synchronized(sensorValue) {
                            sensorValue.set(it.state)
                        }
                    }
                }
        )
    }

    override fun property() = sensorValue
}