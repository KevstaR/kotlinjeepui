package com.kluke.project.sensors

import com.kluke.project.EventContext
import com.kluke.project.database.EntityMapper
import com.kluke.project.events.TickEvent
import com.pi4j.io.gpio.GpioPinAnalogInput
import javafx.application.Platform
import javafx.beans.property.SimpleObjectProperty
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.joda.time.Instant.now
import tornadofx.*
import kotlin.math.roundToInt

class EngineTempSensor(pin: GpioPinAnalogInput, eventContext: EventContext) : Component(), Sensor<Int> {

    private val sensorValueProperty = SimpleObjectProperty(0)
    override fun property() = sensorValueProperty

    init {
        subscribe<TickEvent> {
            // Check every 5 seconds
            if (it.tickCount % 50 == 0L) {

                val pinValue = pin.value.roundToInt()

                Platform.runLater {
                    sensorValueProperty.set(pinValue)
                }

                // Add event to context
                eventContext[now()] = EngineTempEvent(pinValue)
            }
        }
    }
}

data class EngineTempEvent(val value: Int) : EntityMapper {
    override fun newEntity(timeMillis: Long) =
            EngineTempSensorEntry.new {
                time  = timeMillis
                value = this@EngineTempEvent.value
            }
}

object EngineTempSensorTable : IntIdTable() {
    val time  = long("time")
    val value = integer("value")
}

class EngineTempSensorEntry(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<EngineTempSensorEntry>(EngineTempSensorTable)

    var time  by EngineTempSensorTable.time
    var value by EngineTempSensorTable.value

    override fun toString() = """EngineTempSensorEntry(
        time  = $time,
        value = $value
    )""".trimIndent()
}