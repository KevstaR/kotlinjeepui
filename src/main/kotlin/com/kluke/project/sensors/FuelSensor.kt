package com.kluke.project.sensors

import com.kluke.project.EventContext
import com.kluke.project.database.EntityMapper
import com.pi4j.io.gpio.GpioPinAnalogInput
import javafx.application.Platform.runLater
import javafx.beans.property.SimpleObjectProperty
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.joda.time.Instant.now
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit.MILLISECONDS

class FuelSensor(scheduler: ScheduledExecutorService,
                 pin: GpioPinAnalogInput,
                 eventContext: EventContext) : Sensor<Int> {
    private val sensorValue = SimpleObjectProperty(0)

    init {
        val invocation = {
            val pinValue = pin.value.toInt()

            runLater {
                synchronized(sensorValue) {
                    sensorValue.set(pinValue)
                }
            }

            eventContext[now()] = FuelSensorEvent(pinValue)
        }

        scheduler.scheduleAtFixedRate(invocation, 0, 200, MILLISECONDS)
    }

    override fun property(): SimpleObjectProperty<Int> = sensorValue
}

data class FuelSensorEvent(val value: Int) : EntityMapper {
    override fun newEntity(timeMillis: Long) = FuelSensorEntry.new {
        val event = this@FuelSensorEvent.value

        time  = timeMillis
        value = event
    }
}

object FuelSensorTable : IntIdTable() {
    val time  = long("time")
    val value = integer("value")
}

class FuelSensorEntry(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<FuelSensorEntry>(FuelSensorTable)

    var time  by FuelSensorTable.time
    var value by FuelSensorTable.value

    override fun toString() = """FuelSensorEntry(
        time  = $time,
        value = $value
    )""".trimMargin()
}
