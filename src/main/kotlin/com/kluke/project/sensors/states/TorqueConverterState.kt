package com.kluke.project.sensors.states

import com.pi4j.io.gpio.PinState

data class TorqueConverterState(val solenoid: PinState)