package com.kluke.project.sensors.states

import com.pi4j.io.gpio.PinState

data class TransmissionState(val solenoid1: PinState,
                             val solenoid2: PinState)
