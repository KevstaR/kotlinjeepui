package com.kluke.project.sensors.states

import com.pi4j.io.gpio.PinState

data class ShifterState(val reverseGear: PinState,
                        val thirdGear: PinState,
                        val secondFirstGear: PinState,
                        val hallEffect: PinState)