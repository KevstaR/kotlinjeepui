package com.kluke.project.sensors.enums

/**
 * Ordinal called for graphing order.
 */
enum class ShifterPosition(val abbreviation: String) {
    UNDETERMINED(""), // Between positions
    PARK("P"),
    REVERSE("R"),
    NEUTRAL("N"),
    DRIVE("D"),
    THIRD("3"),
    SECOND_FIRST("2-1");
}