package com.kluke.project.sensors.enums

/**
 * Ordinal called for graphing order.
 */
enum class TransmissionGear {
    NONE, FIRST, SECOND, THIRD, OVERDRIVE;
}