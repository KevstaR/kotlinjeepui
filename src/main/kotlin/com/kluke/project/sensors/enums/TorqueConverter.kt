package com.kluke.project.sensors.enums

/**
 * Ordinal called for graphing order.
 */
enum class TorqueConverter {
    UNLOCKED, LOCKED, UNDETERMINED;
}