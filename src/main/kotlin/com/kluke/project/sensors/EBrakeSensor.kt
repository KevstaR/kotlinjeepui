package com.kluke.project.sensors

import com.pi4j.io.gpio.GpioPinDigitalInput
import com.pi4j.io.gpio.PinState
import com.pi4j.io.gpio.PinState.LOW
import com.pi4j.io.gpio.event.GpioPinListenerDigital
import javafx.beans.property.SimpleObjectProperty

class EBrakeSensor(pin: GpioPinDigitalInput) : Sensor<PinState> {

    private val pinStateProperty = SimpleObjectProperty(LOW)

    override fun property(): SimpleObjectProperty<PinState> = pinStateProperty

    init {
        pin.addListener(
                GpioPinListenerDigital {
                    synchronized(pinStateProperty) { pinStateProperty.set(it.state) }
                }
        )
    }
}