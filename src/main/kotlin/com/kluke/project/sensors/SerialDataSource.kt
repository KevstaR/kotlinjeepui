package com.kluke.project.sensors

import com.fazecast.jSerialComm.SerialPort
import com.kluke.project.EventContext
import com.kluke.project.sensors.algorithms.serial.ECUFrame
import com.kluke.project.sensors.algorithms.serial.createFrame
import com.kluke.project.sensors.algorithms.serial.extractFrame
import javafx.beans.property.SimpleObjectProperty
import kotlinx.coroutines.delay
import org.joda.time.Instant.now
import tornadofx.*

class SerialPortUnavailableException(message: String) : RuntimeException(message)

enum class Platform {
    UNIX { override fun port() = "/dev/ttyS0" },
    WINDOWS { override fun port() = "COM4"};
    abstract fun port(): String
}

interface SerialDataSource : Sensor<ECUFrame>{
    suspend fun run()
}

class SerialDataSourceImpl(private val serialPort: SerialPort,
                           private val eventContext: EventContext,
                           private val baud: Int = 57600,
                           private val frameSize: Int = 31) : SerialDataSource {

    private val frameProperty = SimpleObjectProperty<ECUFrame>()

    override suspend fun run() {
        // Mutable read buffer is modified by extractFrames function. It stores data temporarily from the buffer.
        val mutableReadBuffer = mutableListOf<UByte>()

        serialPort {
            while (true) {
                waitForBytes()

                val readBuffer = ByteArray(bytesAvailable())

                readBytes(readBuffer, readBuffer.size.toLong())

                mutableReadBuffer.addAll(readBuffer.toUByteArray())

                extractFrame(mutableBuffer = mutableReadBuffer).forEach {

                    if (it.size == frameSize) {
                        val now = now()
                        val frame = it.createFrame(frameSize)

                        // Set frame property
                        runLater {
                            synchronized(frameProperty) {
                                frameProperty.set(frame)
                            }
                        }

                        // Record to event stream
                        eventContext[now] = frame
                    }
                }
            }
        }
    }

    override fun property(): SimpleObjectProperty<ECUFrame> = frameProperty

    private suspend fun serialPort(operation: suspend SerialPort.() -> Unit) {
        try {
            serialPort.baudRate = baud // 62500
            if (!serialPort.openPort()) throw SerialPortUnavailableException("$serialPort - ${serialPort.systemPortName}")
            serialPort.operation()
        } finally {
            serialPort.closePort()
        }
    }

    private suspend fun SerialPort.waitForBytes(delayMillis: Long = 20) {
        while (bytesAvailable() == 0) delay(delayMillis)
    }
}