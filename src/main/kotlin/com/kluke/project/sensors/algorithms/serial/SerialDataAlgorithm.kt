package com.kluke.project.sensors.algorithms.serial

import kotlin.collections.ArrayList

fun extractFrame(mutableBuffer: MutableList<UByte>): List<UByteArray> {
    val stopByte: UByte = 255u
    val startByte: UByte = 0u
    val stopStartByteIndexOffset = 2

    val frames: MutableList<UByteArray> = mutableListOf()

    // Loop over all bytes and examine in pairs
    var i = 0
    while (i <= mutableBuffer.size - stopStartByteIndexOffset) {
        if (mutableBuffer[i] == stopByte && mutableBuffer[i + 1] == startByte) {

            // Process frame
            val rawFrame = mutableBuffer.take(i)
            val filteredFrame = rawFrame.removeDuplicateMaxBits()
            if (filteredFrame.isNotEmpty()) frames.add(filteredFrame)

            // Remove taken elements from the buffer
            repeat(rawFrame.size + stopStartByteIndexOffset) { mutableBuffer.removeAt(0) }

            // Zero the index
            i = i - i - 1
        }
        i++
    }
    return frames
}

fun List<UByte>.removeDuplicateMaxBits(): UByteArray {
    val filterBit: UByte = 255u
    val result = ArrayList<UByte>(size)

    var i = 0
    while (i <= size - 2) {
        if (this[i] == filterBit && this[i + 1] == filterBit)
            result.add(filterBit).also { i++ } else result.add(this[i])
        i++
    }
    if (i == lastIndex) result.add(this[i])

    return result.toUByteArray()
}