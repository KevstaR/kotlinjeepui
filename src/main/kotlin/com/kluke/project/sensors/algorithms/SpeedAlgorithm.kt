package com.kluke.project.sensors.algorithms

import com.kluke.project.sensors.POLLING_INTERVAL
import java.util.concurrent.TimeUnit
import kotlin.math.PI

interface SpeedAlgorithm : SensorAlgorithm<Number, Double>

class SpeedAlgorithmImpl : SpeedAlgorithm {
    private companion object {
        private const val REV_FACTOR = 25.39
    }
    private fun timeInterval() = POLLING_INTERVAL / TimeUnit.HOURS.toMillis(1).toDouble()
    private fun revolutionRatio(sensorValue: Double) = sensorValue / REV_FACTOR
    private fun defaultConversionFactor() = 81.28 * PI / 100_000.0 // 81.28 is wheel size in CM (32 inch)
    override fun read(state: Number): Double = revolutionRatio(state.toDouble()) * (defaultConversionFactor() / timeInterval())
}