package com.kluke.project.sensors.algorithms

import com.kluke.project.sensors.enums.ShifterPosition
import com.kluke.project.sensors.enums.ShifterPosition.*
import com.kluke.project.sensors.states.ShifterState
import com.pi4j.io.gpio.PinState.HIGH
import com.pi4j.io.gpio.PinState.LOW

class ShifterAlgorithm : SensorAlgorithm<ShifterState, ShifterPosition> {

    private var startFlag = true
    private var shiftFlag = false

    private val table = mapOf(
            ShifterState(reverseGear = LOW, thirdGear  = LOW, secondFirstGear  = LOW, hallEffect  = HIGH) to UNDETERMINED,
            ShifterState(reverseGear = HIGH, thirdGear = LOW, secondFirstGear  = LOW, hallEffect  = LOW)  to REVERSE,
            ShifterState(reverseGear = LOW, thirdGear  = HIGH, secondFirstGear = LOW, hallEffect  = LOW)  to THIRD,
            ShifterState(reverseGear = LOW, thirdGear  = LOW, secondFirstGear  = HIGH, hallEffect = LOW)  to SECOND_FIRST,
            ShifterState(reverseGear = LOW, thirdGear  = LOW, secondFirstGear  = LOW, hallEffect  = LOW)  to NEUTRAL
    )

    override fun read(state: ShifterState): ShifterPosition =
            when (table.getOrDefault(key = state, defaultValue = UNDETERMINED)) {
                UNDETERMINED ->
                    when (shiftFlag) {
                        true -> DRIVE
                        false -> PARK
                    }

                REVERSE -> {
                    startFlag = false
                    shiftFlag = false

                    REVERSE
                }

                NEUTRAL -> {
                    shiftFlag = true

                    NEUTRAL
                }

                THIRD -> THIRD

                SECOND_FIRST -> SECOND_FIRST

                else -> UNDETERMINED
            }
}