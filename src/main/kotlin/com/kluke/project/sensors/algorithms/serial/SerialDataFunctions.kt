package com.kluke.project.sensors.algorithms.serial

import com.kluke.project.map
import kotlin.math.PI

fun UByte.programVersion(): ProgramVersion {
    val map = hashMapOf(
            177.toUByte() to ProgramVersion.NEW,
            176.toUByte() to ProgramVersion.OLD
    )

    return map[this] ?: ProgramVersion.NONE
}

fun UByte.vehicleCalibration(): VehicleCalibration {
    val map = hashMapOf(
            16.toUByte() to VehicleCalibration.OLD_PROGRAM_WITH_AUTO_TRANS_PROGRAM,
            144.toUByte() to VehicleCalibration.OLD_PROGRAM_WITH_MANUAL_TRANS_PROGRAM,
            20.toUByte()  to VehicleCalibration.NEW_PROGRAM_WITH_AUTO_TRANS_PROGRAM,
            148.toUByte() to VehicleCalibration.NEW_PROGRAM_WITH_MANUAL_TRANS_PROGRAM
    )

    return map[this] ?: VehicleCalibration.NONE
}

fun UByte.toHg() = toDouble() / 9.13 + 3.1

fun UByte.toCelsius() = toDouble() * 0.625 - 40.0

fun UByte.tomV() = toDouble() * 4.34

fun UByte.toVolts() = toDouble() / 16.24

fun UByte.toLambda() = toDouble() / 51.2

fun toRPM(highByte: UByte, lowByte: UByte): Int {
    val rpmRaw: UInt = highByte * 255.toUByte() + lowByte

    // Filter RPM to a range
    return if (rpmRaw > 255.toUByte() && rpmRaw < 65280.toULong()) (20000000.toULong() / rpmRaw).toInt() else 0
}

fun UByte.extractErrorCodes(): List<InjectorErrorCodes> = Integer
        .toBinaryString(toInt())
        .subSequence(0, 6)
        .mapIndexed { index, char -> if (char == '0') InjectorErrorCodes.values()[index] else InjectorErrorCodes.NULL_INJECTOR }
        .filter { it != InjectorErrorCodes.NULL_INJECTOR }
        .toList()

fun UByte.toTPSData(): TPSData {
    val value = toDouble()

    // From microcontroller code
    val voltage = value / 51.2
    val percentage = value / 2.55

    return TPSData(voltage, percentage)
}

data class TPSData(val voltage: Double, /* 0-4.98V Range */ val percentage: Double /* 0-100% Range */) {
    override fun toString(): String = percentage.toString()
    fun calibratedMapping(input: IntRange = 5..79, output: IntRange = 0..100) = percentage.map(input, output)
}

fun UByte.toAngularUnit(): AngularUnit {
    val value = toDouble()
    return AngularUnit(degree = value, radian = value * PI / 180.0)
}

data class AngularUnit(val degree: Double, val radian: Double) {
    override fun toString(): String = degree.toString()
}

fun UByte.getBit(position: Int) = toInt() shr position and 1

fun UByte.getLoopStatus() =
        when (getBit(position = 1)) {
            1 -> LoopStatus.DECELERATION_LOOP
            else -> if (getBit(position = 6) == 1) LoopStatus.CLOSED_LOOP else LoopStatus.OPEN_LOOP
        }

fun UByte.toEGRStatus() = EGRStatus.values()[getBit(position = 3)]

fun UByte.toExhaustStatus() = if (this > 127u) ExhaustStatus.LEAN else ExhaustStatus.RICH

fun UByte.toInjectorPulseWidth() = toInt() * 0.128

fun UByte.toColdEnrichmentOffset() = toInt() * 0.128

fun UByte.toShortTermFuelTrim() = toDouble().map(inMin = 0.0, inMax = 255.0, outMin = -100.0, outMax = 100.0)

fun UByte.toLongTermFuelTrim() = toDouble().map(inMin = 0.0, inMax = 255.0, outMin = -100.0, outMax = 100.0)

fun UByte.toFuelSync() = DistributorFuelSync.values()[getBit(position = 2)]

fun UByte.toStarterSignal() = StarterSignal.values()[getBit(position = 4)]