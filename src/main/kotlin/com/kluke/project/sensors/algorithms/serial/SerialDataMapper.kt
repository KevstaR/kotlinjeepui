package com.kluke.project.sensors.algorithms.serial

import com.kluke.project.database.EntityMapper
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

data class ECUFrame(
        val programVersion: ProgramVersion,
        val vehicleCalibration: VehicleCalibration,
        val map: Double,
        val cts: Double,
        val iat: Double,
        val batteryVoltage: Double,
        val o2: Double,
        val rpm: Int,
        val injectorOpenFlags: List<InjectorErrorCodes>,
        val tps: TPSData,
        val sparkAdvance: AngularUnit,
        val barometricPressure: Double,
        val loopStatus: LoopStatus,
        val egrStatus: EGRStatus,
        val injectorPulseWidth: Double,
        val exhaustStatus: ExhaustStatus,
        val coldEnrichmentOffset: Double,
        val shortTermFuelTrim: Double,
        val longTermFuelTrim: Double,
        val knock: Int,
        val fuelSync: DistributorFuelSync,
        val starterSignal: StarterSignal
) : EntityMapper {
    override fun newEntity(timeMillis: Long) =
        SerialDataEntry.new {
            val frame = this@ECUFrame

            time                 = timeMillis
            map                  = frame.map
            cts                  = frame.cts
            iat                  = frame.iat
            batteryVoltage       = frame.batteryVoltage
            o2                   = frame.o2
            rpm                  = frame.rpm
            injectorOpenFlags    = frame.injectorOpenFlags.toString()
            tps                  = frame.tps.percentage
            sparkAdvance         = frame.sparkAdvance.degree
            barometricPressure   = frame.barometricPressure
            loopStatus           = frame.loopStatus.name
            egrStatus            = frame.egrStatus.name
            injectorPulseWidth   = frame.injectorPulseWidth
            exhaustStatus        = frame.exhaustStatus.name
            coldEnrichmentOffset = frame.coldEnrichmentOffset
            shortTermFuelTrim    = frame.shortTermFuelTrim
            longTermFuelTrim     = frame.longTermFuelTrim
            knock                = frame.knock
        }
}

enum class ProgramVersion {
    NEW, // 177u
    OLD, // 176u
    NONE
}

enum class VehicleCalibration {
    OLD_PROGRAM_WITH_AUTO_TRANS_PROGRAM,   // 16u
    OLD_PROGRAM_WITH_MANUAL_TRANS_PROGRAM, // 144u
    NEW_PROGRAM_WITH_AUTO_TRANS_PROGRAM,   // 20u
    NEW_PROGRAM_WITH_MANUAL_TRANS_PROGRAM, // 148u
    NONE
}

/**
 * Order dependent enums.
 */

enum class InjectorErrorCodes {
    INJECTOR_4_OPEN,
    INJECTOR_2_OPEN,
    INJECTOR_6_OPEN,
    INJECTOR_3_OPEN,
    INJECTOR_5_OPEN,
    INJECTOR_1_OPEN,
    NULL_INJECTOR
}

enum class LoopStatus { DECELERATION_LOOP, OPEN_LOOP, CLOSED_LOOP }
enum class EGRStatus { OFF, ON }
enum class ExhaustStatus { LEAN, RICH }
enum class DistributorFuelSync { POSITIVE, NEGATIVE }
enum class StarterSignal { OFF, ON }

/**
 * https://nickintimedesign.com/renix-4l-ecu-datastream/
 */
fun UByteArray.createFrame(dataFrameSize: Int) =
        if (size == dataFrameSize) ECUFrame(
            programVersion       = this[0].programVersion(),
            vehicleCalibration   = this[1].vehicleCalibration(),
            map                  = this[3].toHg(),
            cts                  = this[4].toCelsius(),
            iat                  = this[5].toCelsius(),
            batteryVoltage       = this[6].toVolts(),
            o2                   = this[7].toLambda(),
            rpm                  = toRPM(lowByte = this[8], highByte = this[9]),
            injectorOpenFlags    = this[11].extractErrorCodes(),
            tps                  = this[12].toTPSData(),
            sparkAdvance         = this[13].toAngularUnit(),
            barometricPressure   = this[16].toHg(),
            loopStatus           = this[18].getLoopStatus(),
            egrStatus            = this[18].toEGRStatus(),
            injectorPulseWidth   = this[19].toInjectorPulseWidth(),
            exhaustStatus        = this[7].toExhaustStatus(),
            coldEnrichmentOffset = this[23].toColdEnrichmentOffset(),
            shortTermFuelTrim    = this[24].toShortTermFuelTrim(),
            longTermFuelTrim     = this[26].toLongTermFuelTrim(),
            knock                = this[27].toInt(),
            fuelSync             = this[29].toFuelSync(),
            starterSignal        = this[29].toStarterSignal()
    ) else throw ECUFrameSizeContradictionException("Raw data frame size: $size != $dataFrameSize")

class ECUFrameSizeContradictionException(message: String) : RuntimeException(message)

object SerialDataTable : IntIdTable() {
    val time                 = long("time")
    val map                  = double("map")
    val cts                  = double("cts")
    val iat                  = double("iat")
    val batteryVoltage       = double("batteryVoltage")
    val o2                   = double("o2")
    val rpm                  = integer("rpm")
    val injectorOpenFlags    = text("injectorOpenFlags")
    val tps                  = double("tpsSensorPercentage")
    val sparkAdvance         = double("sparkAdvanceDegrees")
    val barometricPressure   = double("barometricPressure")
    val loopStatus           = text("loopStatus")
    val egrStatus            = text("egrStatus")
    val injectorPulseWidth   = double("injectorPulseWidth")
    val exhaustStatus        = text("exhaustStatus")
    val coldEnrichmentOffset = double("coldEnrichmentOffset")
    val shortTermFuelTrim    = double("shortTermFuelTrim")
    val longTermFuelTrim     = double("longTermFuelTrim")
    val knock                = integer("knock")
}

class SerialDataEntry(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<SerialDataEntry>(SerialDataTable)

    var time                 by SerialDataTable.time
    var map                  by SerialDataTable.map
    var cts                  by SerialDataTable.cts
    var iat                  by SerialDataTable.iat
    var batteryVoltage       by SerialDataTable.batteryVoltage
    var o2                   by SerialDataTable.o2
    var rpm                  by SerialDataTable.rpm
    var injectorOpenFlags    by SerialDataTable.injectorOpenFlags
    var tps                  by SerialDataTable.tps
    var sparkAdvance         by SerialDataTable.sparkAdvance
    var barometricPressure   by SerialDataTable.barometricPressure
    var loopStatus           by SerialDataTable.loopStatus
    var egrStatus            by SerialDataTable.egrStatus
    var injectorPulseWidth   by SerialDataTable.injectorPulseWidth
    var exhaustStatus        by SerialDataTable.exhaustStatus
    var coldEnrichmentOffset by SerialDataTable.coldEnrichmentOffset
    var shortTermFuelTrim    by SerialDataTable.shortTermFuelTrim
    var longTermFuelTrim     by SerialDataTable.longTermFuelTrim
    var knock                by SerialDataTable.knock

    override fun toString() = """SerialDataEntry(
            time                 = $time                ,
            map                  = $map                 ,
            cts                  = $cts                 ,
            iat                  = $iat                 ,
            batteryVoltage       = $batteryVoltage      ,
            o2                   = $o2                  ,
            rpm                  = $rpm                 ,
            injectorOpenFlags    = $injectorOpenFlags   ,
            tps                  = $tps                 ,
            sparkAdvance         = $sparkAdvance        ,
            barometricPressure   = $barometricPressure  ,
            loopStatus           = $loopStatus          ,
            egrStatus            = $egrStatus           ,
            injectorPulseWidth   = $injectorPulseWidth  ,
            exhaustStatus        = $exhaustStatus       ,
            coldEnrichmentOffset = $coldEnrichmentOffset,
            shortTermFuelTrim    = $shortTermFuelTrim   ,
            longTermFuelTrim     = $longTermFuelTrim    ,
            knock                = $knock
    )""".trimIndent()
}
