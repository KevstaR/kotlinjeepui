package com.kluke.project.sensors.algorithms

interface SensorAlgorithm<A, B> {
    fun read(state: A): B
}