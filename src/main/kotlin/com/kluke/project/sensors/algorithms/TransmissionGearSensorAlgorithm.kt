package com.kluke.project.sensors.algorithms

import com.kluke.project.sensors.enums.TransmissionGear
import com.kluke.project.sensors.enums.TransmissionGear.*
import com.kluke.project.sensors.states.TransmissionState
import com.pi4j.io.gpio.PinState.HIGH
import com.pi4j.io.gpio.PinState.LOW

class TransmissionGearSensorAlgorithm : SensorAlgorithm<TransmissionState, TransmissionGear> {

    /**
    Solenoid States:
    1 0 = 1st Gear
    1 1 = 2nd Gear
    0 1 = 3rd Gear
    0 0 = Overdrive

    Optoisolator Logic is Inverted, but this is done at the pin state level.
     */
    private val table = mapOf(
        TransmissionState(solenoid1 = HIGH, solenoid2 = LOW) to FIRST,
        TransmissionState(solenoid1 = HIGH, solenoid2 = HIGH) to SECOND,
        TransmissionState(solenoid1 = LOW, solenoid2 = HIGH) to THIRD,
        TransmissionState(solenoid1 = LOW, solenoid2 = LOW) to OVERDRIVE
    )

    override fun read(state: TransmissionState) = table.getOrDefault(key = state, defaultValue = NONE)
}