package com.kluke.project.sensors.algorithms

import com.kluke.project.sensors.enums.TorqueConverter
import com.kluke.project.sensors.enums.TorqueConverter.*
import com.kluke.project.sensors.states.TorqueConverterState
import com.pi4j.io.gpio.PinState.HIGH
import com.pi4j.io.gpio.PinState.LOW

class TransmissionTorqueConverterAlgorithm : SensorAlgorithm<TorqueConverterState, TorqueConverter> {

    /**
     * TC: 0 = Unlocked
     */
    private val table = mapOf(
            TorqueConverterState(solenoid = LOW) to UNLOCKED,
            TorqueConverterState(solenoid = HIGH) to LOCKED
    )

    override fun read(state: TorqueConverterState) = table.getOrDefault(key = state, defaultValue = UNDETERMINED)
}