package com.kluke.project.sensors.algorithms

import kotlin.math.ln
import kotlin.math.pow

/**
 * Converts a given sensor reading in the range of 0-1023 into a temperature in Kelvin.
 */
class TemperatureAlgorithm : SensorAlgorithm<Int, Double> {
    private companion object {
        //Engine Block Temperature Values
        const val VOLTAGE_IN = 3.3             // Input voltage
        const val REFERENCE_RESISTANCE = 326.0 // Reference resistor's value in ohms (you can give this value in kiloohms or megaohms - the resistance of the tested resistor will be given in the same units)
        const val MAX_ANALOG_READING = 1023.0

        const val ZERO_FAHRENHEIT = 12255.0

        const val A = 0.0023203898
        const val B = 0.000059065872
        const val C = 0.0000012454982
    }

    override fun read(state: Int) = steinhartHart(state)

    /**
    Steinhart-Hart Equation:
    The Steinhart–Hart equation is a model of the resistance of a semiconductor at different temperatures.

    https://en.wikipedia.org/wiki/Steinhart%E2%80%93Hart_equation

    Returned unit is in Kelvin.
     */
    private fun steinhartHart(value: Int): Double {

        val r = resistance(value)
        val denominator = A + B * ln(r) + C * (ln(r)).pow(3.0)

        return 1.0 / denominator // Kelvin
    }

    private fun resistance(sensorValue: Int): Double {
        // Calculate tested resistor's value
        val resistance = calculateResistanceFromVoltage(sensorValue)

        return if (resistance >= 15000.0) ZERO_FAHRENHEIT else resistance
    }

    private fun calculateResistanceFromVoltage(value: Int): Double {
        fun calculateVoltageOut(value: Int) = (VOLTAGE_IN * value) / MAX_ANALOG_READING
        return REFERENCE_RESISTANCE * (1.0 / (VOLTAGE_IN / calculateVoltageOut(value) - 1.0))
    }
}