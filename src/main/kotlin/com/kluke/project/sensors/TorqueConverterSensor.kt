package com.kluke.project.sensors

import com.kluke.project.EventContext
import com.kluke.project.database.EntityMapper
import com.kluke.project.executors.DebounceExecutor
import com.kluke.project.sensors.algorithms.SensorAlgorithm
import com.kluke.project.sensors.algorithms.TransmissionTorqueConverterAlgorithm
import com.kluke.project.sensors.enums.TorqueConverter
import com.kluke.project.sensors.enums.TorqueConverter.UNLOCKED
import com.kluke.project.sensors.states.TorqueConverterState
import com.pi4j.io.gpio.GpioPinDigitalInput
import com.pi4j.io.gpio.PinState.getInverseState
import com.pi4j.io.gpio.event.GpioPinListenerDigital
import javafx.application.Platform
import javafx.beans.property.SimpleObjectProperty
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.joda.time.Instant.now

const val TORQUE_CONVERTER_DEBOUNCE_MS = 75L

class TorqueConverterSensorImpl(solenoidTcPin: GpioPinDigitalInput,
                                debounceExecutor: DebounceExecutor,
                                eventContext: EventContext) : Sensor<TorqueConverter> {
    private val torqueConverterStatus = SimpleObjectProperty(UNLOCKED)

    private val algorithm: SensorAlgorithm<TorqueConverterState, TorqueConverter> = TransmissionTorqueConverterAlgorithm()

    init {
        solenoidTcPin.addListener(
                GpioPinListenerDigital {
                    debounceExecutor.execute("TORQUE_CONVERTER_SENSOR_KEY", TORQUE_CONVERTER_DEBOUNCE_MS) {
                        // Query state
                        val state = TorqueConverterState(solenoid = getInverseState(solenoidTcPin.state))

                        // Process output
                        val read = algorithm.read(state)

                        // Set property
                        Platform.runLater { torqueConverterStatus.set(read) }

                        // Record to event stream
                        eventContext[now()] = TorqueConverterSensorEvent(read)
                    }
                }
        )
    }

    override fun property(): SimpleObjectProperty<TorqueConverter> = torqueConverterStatus
}

data class TorqueConverterSensorEvent(val value: TorqueConverter) : EntityMapper {
    override fun newEntity(timeMillis: Long) =
            TorqueConverterSensorEntry.new {
                val event = this@TorqueConverterSensorEvent.value

                time     = timeMillis
                value    = event.name
                position = when(event.ordinal) { // Make LOCKED high and UNLOCKED low
                    0 -> 1
                    1 -> 0
                    else -> 2 // UNDETERMINED
                }
            }
}

object TorqueConverterSensorTable : IntIdTable() {
    val time     = long("time")
    val value    = text("value")
    val position = integer("position")
}

class TorqueConverterSensorEntry(id :EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<TorqueConverterSensorEntry>(TorqueConverterSensorTable)

    var time     by TorqueConverterSensorTable.time
    var value    by TorqueConverterSensorTable.value
    var position by TorqueConverterSensorTable.position

    override fun toString() = """TorqueConverterSensorEntry(
        time     = $time,
        value    = $value,
        position = $position
    )""".trimIndent()
}