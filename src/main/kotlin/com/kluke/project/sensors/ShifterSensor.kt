package com.kluke.project.sensors

import com.kluke.project.EventContext
import com.kluke.project.database.EntityMapper
import com.kluke.project.executors.DebounceExecutor
import com.kluke.project.sensors.algorithms.SensorAlgorithm
import com.kluke.project.sensors.algorithms.ShifterAlgorithm
import com.kluke.project.sensors.enums.ShifterPosition
import com.kluke.project.sensors.enums.ShifterPosition.PARK
import com.kluke.project.sensors.states.ShifterState
import com.pi4j.io.gpio.GpioPinDigitalInput
import com.pi4j.io.gpio.PinState.getInverseState
import com.pi4j.io.gpio.event.GpioPinListenerDigital
import javafx.beans.property.SimpleObjectProperty
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.joda.time.Instant.now
import tornadofx.*

const val SHIFTER_DEBOUNCE_MS = 75L

class ShifterSensor(nssReversePin: GpioPinDigitalInput,
                    nssThirdPin: GpioPinDigitalInput,
                    nssSecondFirstPin: GpioPinDigitalInput,
                    nssHallEffectPin: GpioPinDigitalInput,
                    debounceExecutor: DebounceExecutor,
                    eventContext: EventContext,
                    private val shifterPosition: SimpleObjectProperty<ShifterPosition> = SimpleObjectProperty(PARK),
                    private val algorithm: SensorAlgorithm<ShifterState, ShifterPosition> = ShifterAlgorithm()
) : Component(), Sensor<ShifterPosition> {
    init {
        listOf(nssReversePin, nssThirdPin, nssSecondFirstPin, nssHallEffectPin).map { pin ->
            pin.addListener(
                    GpioPinListenerDigital {
                        debounceExecutor.execute("SHIFTER_SENSOR_KEY", SHIFTER_DEBOUNCE_MS) {
                            // Query state
                            val state = ShifterState(
                                    reverseGear     = getInverseState(nssReversePin.state),
                                    thirdGear       = getInverseState(nssThirdPin.state),
                                    secondFirstGear = getInverseState(nssSecondFirstPin.state),
                                    hallEffect      = getInverseState(nssHallEffectPin.state)
                            )

                            // Process output
                            val read = algorithm.read(state)

                            // Set property
                            shifterPosition.set(read)

                            // Record to event stream
                            eventContext[now()] = ShifterSensorEvent(read)

                            // Fire event if match
                            if (read == PARK) fire(ParkEvent)
                        }
                    }
            )
        }
    }

    override fun property() = shifterPosition
}

object ParkEvent : FXEvent()

data class ShifterSensorEvent(val value: ShifterPosition) : EntityMapper {
    override fun newEntity(timeMillis: Long) =
            ShifterSensorEntry.new {
                val event = this@ShifterSensorEvent.value

                time         = timeMillis
                value        = event.name
                abbreviation = event.abbreviation
                position     = event.ordinal
            }
}

object ShifterSensorTable : IntIdTable() {
    val time         = long("time")
    val value        = text("value")
    val abbreviation = text("abbreviation")
    val position     = integer("position")
}

class ShifterSensorEntry(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<ShifterSensorEntry>(ShifterSensorTable)

    var time         by ShifterSensorTable.time
    var value        by ShifterSensorTable.value
    var abbreviation by ShifterSensorTable.abbreviation
    var position     by ShifterSensorTable.position

    override fun toString() = """ShifterSensorEntry(
        time         = $time,
        value        = $value,
        abbreviation = $abbreviation,
        position     = $position
    )""".trimMargin()
}