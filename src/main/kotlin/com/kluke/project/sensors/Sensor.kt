package com.kluke.project.sensors

import javafx.beans.property.ObjectProperty

interface Sensor<T> {
    fun property(): ObjectProperty<T>
}