package com.kluke.project.sensors

import com.kluke.project.EventContext
import com.kluke.project.database.EntityMapper
import com.kluke.project.datastructures.ObservableRingBuffer
import com.kluke.project.datastructures.query
import com.kluke.project.events.TickEvent
import com.pi4j.io.gpio.GpioPinDigitalInput
import com.pi4j.io.gpio.event.GpioPinListenerDigital
import javafx.beans.property.*
import javafx.collections.ObservableList
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.joda.time.Instant.now
import tornadofx.*
import java.util.*
import java.util.concurrent.ConcurrentLinkedDeque

const val POLLING_INTERVAL = 200L
const val POLLING_INTERVAL_MOD = 2

/**
 * Rationale:
 *  Since the RoadSpeed delta could be the same value more than once in a row, a data class abstraction can help prevent
 *  the issue where each delta, even if it's the same, needs to be recorded by upstream listeners.
 *
 *  Three fields on the data class prevents the new object being equal than the last as at least tickCount will never be
 *  the same and the objects won't have the same hash.
 */
data class RoadSpeedFrame(val delta: Int, val tickCount: Long, val tickTimeDelta: Long)

interface RoadSpeedSensor {
    fun totalCountProperty(): LongProperty
    fun frameProperty(): ObjectProperty<RoadSpeedFrame>
    fun deltaBufferProperty(): ObservableList<Int>
}

class RoadSpeedSensorImpl(pin: GpioPinDigitalInput, eventContext: EventContext) : Component(), RoadSpeedSensor {

    private val totalCountProperty: LongProperty = SimpleLongProperty(0)
    override fun totalCountProperty() = totalCountProperty
    private var totalCount by totalCountProperty

    private val frameProperty: ObjectProperty<RoadSpeedFrame> = SimpleObjectProperty(RoadSpeedFrame(0,0,0))
    override fun frameProperty() = frameProperty
    private var frame by frameProperty

    private val deltaBuffer: ObservableList<Int> = ObservableRingBuffer<Int>(maxCapacity = 10, fill = 0)
    override fun deltaBufferProperty() = deltaBuffer

    private val eventTimes: Deque<Long> = ConcurrentLinkedDeque()

    init {
        // Add change listener to pin
        pin.addListener(GpioPinListenerDigital { eventTimes.add(System.currentTimeMillis()) })

        // Subscribe to global polling interval event
        subscribe<TickEvent> {
            if (it.tickCount % POLLING_INTERVAL_MOD == 0L) {
                synchronized(frame) {
                    // Capture a delta
                    frame = RoadSpeedFrame(
                        delta = eventTimes.query(POLLING_INTERVAL),
                        tickCount = it.tickCount,
                        tickTimeDelta = it.delta
                    )

                    // Get current time for event processing
                    val now = now()

                    // Add event to context
                    eventContext[now] = RoadSpeedEvent(frame.delta)

                    deltaBuffer.add(frame.delta)

                    synchronized(totalCount) {
                        totalCount += frame.delta

                        // Add event to context with a 1 ms offset to prevent overwriting keys
                        eventContext[now + 1] = TotalRoadDistanceEvent(totalCount)
                    }
                }
            }
        }
    }
}

data class TotalRoadDistanceEvent(val value: Long) : EntityMapper {
    override fun newEntity(timeMillis: Long) =
            TotalRoadDistanceEntry.new {
                time = timeMillis
                distance = this@TotalRoadDistanceEvent.value
            }
}

object TotalRoadDistanceTable : IntIdTable() {
    val time     = long("time")
    val distance = long("distance")
}

class TotalRoadDistanceEntry(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<TotalRoadDistanceEntry>(TotalRoadDistanceTable)

    var time by TotalRoadDistanceTable.time
    var distance by TotalRoadDistanceTable.distance

    override fun toString() = """TotalRoadDistanceEntry(
        time     = $time,
        distance = $distance
    )""".trimIndent()
}

data class RoadSpeedEvent(val value: Int) : EntityMapper {
    override fun newEntity(timeMillis: Long) =
            RoadSpeedSensorEntry.new {
                time  = timeMillis
                value = this@RoadSpeedEvent.value
            }
}

object RoadSpeedSensorTable : IntIdTable() {
    val time  = long("time")
    val value = integer("value")
}

class RoadSpeedSensorEntry(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<RoadSpeedSensorEntry>(RoadSpeedSensorTable)

    var time  by RoadSpeedSensorTable.time
    var value by RoadSpeedSensorTable.value

    override fun toString() = """RoadSpeedSensorEntry(
        time  = $time,
        value = $value
    )""".trimIndent()
}