package com.kluke.project.sensors

import com.kluke.project.sensors.algorithms.serial.ECUFrame
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import tornadofx.doubleBinding

class GallonsPerHourSensor(private val serialDataSource: SerialDataSource) : Sensor<Double> {
    private companion object {
        private val INJECTOR_LBS = 18.927
    }
    private val property: ObjectProperty<Double> = SimpleObjectProperty(0.0)
    override fun property() = property

    private fun binding() = serialDataSource.property().doubleBinding { frame ->
        frame ?: return@doubleBinding 0.0
        INJECTOR_LBS * injectorDuty(frame) / 100.0
    }.asObject()
    private fun injectorDuty(frame: ECUFrame) = (frame.rpm * frame.injectorPulseWidth) / 1200.0

    init {
        property.bind(binding())
    }
}