package com.kluke.project.sensors

import com.kluke.project.EventContext
import com.kluke.project.database.EntityMapper
import com.kluke.project.executors.DebounceExecutor
import com.kluke.project.sensors.algorithms.SensorAlgorithm
import com.kluke.project.sensors.algorithms.TransmissionGearSensorAlgorithm
import com.kluke.project.sensors.enums.TransmissionGear
import com.kluke.project.sensors.enums.TransmissionGear.FIRST
import com.kluke.project.sensors.states.TransmissionState
import com.pi4j.io.gpio.GpioPinDigitalInput
import com.pi4j.io.gpio.PinState.getInverseState
import com.pi4j.io.gpio.event.GpioPinListenerDigital
import javafx.application.Platform
import javafx.beans.property.SimpleObjectProperty
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.joda.time.Instant.now

const val TRANSMISSION_GEAR_DEBOUNCE_MS = 75L

class TransmissionGearSensorImpl(solenoidPin1: GpioPinDigitalInput,
                                 solenoidPin2: GpioPinDigitalInput,
                                 debounceExecutor: DebounceExecutor,
                                 eventContext: EventContext) : Sensor<TransmissionGear> {
    private val gearStatus = SimpleObjectProperty(FIRST)

    private val algorithm: SensorAlgorithm<TransmissionState, TransmissionGear> = TransmissionGearSensorAlgorithm()

    init {
        listOf(solenoidPin1, solenoidPin2).map { pin ->
            pin.addListener(
                    GpioPinListenerDigital {
                        debounceExecutor.execute("TRANSMISSION_GEAR_SENSOR_KEY", TRANSMISSION_GEAR_DEBOUNCE_MS) {
                            // Query state
                            val state = TransmissionState(
                                    solenoid1 = getInverseState(solenoidPin1.state),
                                    solenoid2 = getInverseState(solenoidPin2.state)
                            )

                            // Process output
                            val read = algorithm.read(state)

                            // Set property
                            Platform.runLater { gearStatus.set(read) }

                            // Record to event stream
                            eventContext[now()] = TransmissionGearSensorEvent(read)
                        }
                    }
            )
        }
    }

    override fun property() = gearStatus
}

data class TransmissionGearSensorEvent(val value: TransmissionGear) : EntityMapper {
    override fun newEntity(timeMillis: Long) =
            TransmissionGearSensorEntry.new {
                val event = this@TransmissionGearSensorEvent.value

                time     = timeMillis
                value    = event.name
                position = event.ordinal
            }
}

object TransmissionGearSensorTable : IntIdTable() {
    val time     = long("time")
    val value    = text("value")
    val position = integer("position")
}

class TransmissionGearSensorEntry(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<TransmissionGearSensorEntry>(TransmissionGearSensorTable)

    var time     by TransmissionGearSensorTable.time
    var value    by TransmissionGearSensorTable.value
    var position by TransmissionGearSensorTable.position

    override fun toString() = """TransmissionGearSensorEntry(
        time     = $time,
        value    = $value,
        position = $position
    )""".trimIndent()
}