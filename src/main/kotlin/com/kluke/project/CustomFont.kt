package com.kluke.project

import javafx.scene.text.Font
import javafx.scene.text.Font.loadFont
import java.io.InputStream

enum class CustomFont(val fontName: String) {
    LCARS_UC("fonts/Swiss911UCmBT.TTF") {
        override fun getFont(size: Double): Font = loadFont(
                getResourceAsStream(fontName), size)
    },
    LCARS_XC("fonts/Swiss911XCmBT.TTF") {
        override fun getFont(size: Double): Font = loadFont(
                getResourceAsStream(fontName), size)
    };
    
    abstract fun getFont(size: Double): Font
    
    protected fun getResourceAsStream(fontName: String): InputStream? =
            this::class.java.classLoader!!.getResourceAsStream(fontName)
}

fun lcarsFont(size: Double) = CustomFont.LCARS_UC.getFont(size)
fun lcarsFontSmall() = lcarsFont(24.0)
fun lcarsFontSmaller() = lcarsFont(18.0)