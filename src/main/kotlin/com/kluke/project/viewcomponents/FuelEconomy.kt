package com.kluke.project.viewcomponents

import com.kluke.project.colors.LCARS_BLUE
import com.kluke.project.colors.LCARS_CORAL
import com.kluke.project.lcarsFont
import com.kluke.project.views.*
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleStringProperty
import javafx.scene.Parent
import javafx.scene.layout.Region
import javafx.scene.paint.Color.TRANSPARENT
import javafx.scene.paint.Color.WHITE
import tornadofx.*

class FuelEconomy : Region() {

    private val valueProperty = SimpleDoubleProperty(0.0)
    fun valueProperty() = valueProperty

    private val abbreviationProperty = SimpleStringProperty("")
    fun abbreviationProperty() = abbreviationProperty


    init {
        rectangle {
            translateX = 1.0
            translateY = 32.0
            width = 4.0
            height = 30.0
            withColor(LCARS_CORAL)
        }
        rectangle {
            translateX = 119.0
            translateY = 32.0
            width = 4.0
            height = 30.0
            withColor(LCARS_CORAL)
        }
        stackpane {
            maxWidth = 124.0
            maxHeight = 93.0
            minWidth = maxWidth
            minHeight = maxHeight

            label {
                textProperty().bind(valueProperty.stringBinding {
                    it ?: return@stringBinding "0.0"
                    it.toStringPrecision(1)
                })
                font = lcarsFont(64.0)
                textFill = LCARS_BLUE
            }
        }
        gridpane {
            val w = 30.0
            val h = 45.0
            val c = WHITE
            val hh = 17.0
            row {
                lcarsGaugeEdge(
                        width = w,
                        height = h,
                        color = c,
                        horizontalHeight = hh
                )
                stackpane {
                    translateY = -14.0
                    text {
                        text = "FUEL ECO"
                        fill = LCARS_BLUE
                        font = lcarsFont(24.0)
                    }
                }
                lcarsGaugeEdge(
                        width = w,
                        height = h,
                        color = c,
                        horizontalHeight = hh
                ) { scaleX = -1.0 }
            }
            row {
                lcarsGaugeEdge(
                        width = w,
                        height = h,
                        color = c,
                        horizontalHeight = hh
                ) {
                    scaleY = -1.0
                }
                stackpane {
                    translateY = 15.0

                    rectangle {
                        width = 60.0
                        height = 10.0
                        fill = TRANSPARENT
                    }
                    text {
                        textProperty().bind(abbreviationProperty)
                        fill = LCARS_BLUE
                        font = lcarsFont(24.0)
                    }

                }
                lcarsGaugeEdge(
                        width = w,
                        height = h,
                        color = c,
                        horizontalHeight = hh
                ) {
                    scaleX = -1.0
                    scaleY = -1.0
                }
            }
        }
    }
}

fun Parent.fuelEconomy(op: FuelEconomy.() -> Unit = {}) = FuelEconomy().attachTo(this, op)