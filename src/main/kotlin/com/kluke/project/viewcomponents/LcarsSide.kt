package com.kluke.project.viewcomponents

import com.kluke.project.colors.LCARS_LIGHTORANGE
import com.kluke.project.colors.LCARS_LIGHTPINK
import com.kluke.project.colors.LCARS_ORANGE
import com.kluke.project.colors.LCARS_PINK
import com.kluke.project.views.withColor
import javafx.scene.Parent
import javafx.scene.layout.Region
import javafx.scene.shape.Path
import tornadofx.*

class LcarsSide : Region() {
    private val sideHeight = 270.0
    private val edgeHeight = 60.0
    private val edgeWidth = 50.0
    private val sideWidth = 20.0
    private val color = LCARS_ORANGE
    private val decorator = observableMapOf(LCARS_LIGHTPINK to 2.0,LCARS_PINK to  4.0, LCARS_LIGHTORANGE to  4.0)

    private val space = 2.0

    init {
        stackpane {
            vbox {
                val fitWidth = (sideWidth / 2.0) - space
                lcarsGaugeEdge()
                hbox {
                    spacing = space
                    vbox {
                        spacing = space
                        val halfHeight = sideHeight / 2.0
                        numberLineBox(halfHeight)
                        numberLineBox(halfHeight)
                    }
                    lcarsDecoratorBoxes(fitWidth, sideHeight - 20)
                }
                lcarsGaugeEdge {
                    scaleY = -1.0
                    translateY = -28.0
                }
            }
        }
    }

    private fun Parent.lcarsGaugeEdge(horizontalHeight: Double = 10.0, op: Path.() -> Unit = {}) =
            path {
                val outerArc = 25
                val innerArc = 15
                val capHeight = 15

                moveTo(0, outerArc)
                vlineTo(edgeHeight - capHeight)
                hlineTo(sideWidth / 2)
                vlineTo(edgeHeight)
                hlineTo(sideWidth)
                vlineTo(horizontalHeight + innerArc)
                arcTo(
                        radiusX = innerArc,
                        radiusY = innerArc,
                        xAxisRotation = 0,
                        x = innerArc + sideWidth,
                        y = horizontalHeight,
                        largeArcFlag = false,
                        sweepFlag = true
                )
                hlineTo(edgeWidth)
                vlineTo(0)
                hlineTo(outerArc)
                arcTo(
                        radiusX = outerArc,
                        radiusY = outerArc,
                        xAxisRotation = 0,
                        x = 0,
                        y = outerArc,
                        largeArcFlag = false,
                        sweepFlag = false
                )
                vlineTo(outerArc)

                withColor(color)

                op()
            }

    private fun Parent.numberLineBox(rectHeight: Double) =
            rectangle {
                translateX = 1.0
                translateY = -14.0
                width = 8.0
                height = rectHeight

                withColor(color)
            }

    private fun Parent.lcarsDecoratorBoxes(fitWidth: Double, stackHeight: Double) =
            vbox {
                spacing = 1.0
                decorator.forEach {
                    rectangle {
                        width = fitWidth
                        height = (stackHeight - 10.0) / it.value
                        translateX = 1.0

                        withColor(it.key)
                    }
                }
            }
}

fun Parent.lcarsSide(op: LcarsSide.() -> Unit = {}) = LcarsSide().attachTo(this, op)