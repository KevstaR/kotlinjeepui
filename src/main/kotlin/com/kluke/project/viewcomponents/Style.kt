package com.kluke.project.viewcomponents

import com.kluke.project.colors.LCARS_BLUE
import com.kluke.project.colors.LCARS_ORANGE
import com.kluke.project.lcarsFontSmaller
import javafx.scene.paint.Color.*
import tornadofx.*

class Styles : Stylesheet() {
    companion object {
        // Define our styles
        val chart by cssclass()
    }

    init {
        chartSeriesLine {
            stroke = LCARS_ORANGE
        }

        defaultColor1 {
            chartSeriesLine {
                this@defaultColor1.stroke = WHITE
            }
        }

        label {
            textFill = LCARS_BLUE
        }

        chartTitle {
            font = lcarsFontSmaller()
        }

        chartVerticalGridLines {
            stroke = LCARS_BLUE
        }

        chartHorizontalGridLines {
            stroke = LCARS_BLUE
        }

        chartPlotBackground {
            opacity = 0.6

            backgroundColor += BLACK
        }

        chart {
            backgroundColor += TRANSPARENT
        }
    }
}