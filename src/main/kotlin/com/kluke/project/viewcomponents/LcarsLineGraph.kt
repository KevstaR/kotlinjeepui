package com.kluke.project.viewcomponents

import com.kluke.project.CustomFont.LCARS_UC
import com.kluke.project.colors.LCARS_BLUE
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections.observableArrayList
import javafx.collections.ObservableList
import javafx.scene.Parent
import javafx.scene.chart.Axis
import javafx.scene.chart.CategoryAxis
import javafx.scene.chart.LineChart.SortingPolicy.NONE
import javafx.scene.chart.NumberAxis
import javafx.scene.chart.XYChart.Series
import javafx.scene.layout.Region
import tornadofx.*

class LcarsLineGraph : Region() {

    val graphTitleProperty = SimpleStringProperty("")
    var graphTitle: String by graphTitleProperty

    val graphHeightProperty = SimpleDoubleProperty(200.0)
    var graphHeight by graphHeightProperty

    val graphWidthProperty = SimpleDoubleProperty(200.0)
    var graphWidth by graphWidthProperty

    val animationProperty = SimpleBooleanProperty(false)
    var animation by animationProperty

    val dataProperty = SimpleObjectProperty(observableArrayList<Series<String, Number>>())
    var data: ObservableList<Series<String, Number>> by dataProperty

    init {
        linechart(x = CategoryAxis().style(), y = NumberAxis().style()) {
            minHeightProperty().bind(graphHeightProperty)
            prefHeightProperty().bind(graphHeightProperty)

            minWidthProperty().bind(graphWidthProperty)
            prefWidthProperty().bind(graphWidthProperty)

            titleProperty().bind(graphTitleProperty)

            animatedProperty().bind(animationProperty)

            createSymbols = false
            isLegendVisible = false

            axisSortingPolicy = NONE

            dataProperty().bind(dataProperty)
        }
    }

    private fun <T> Axis<T>.style(): Axis<T> {
        tickLabelFont = LCARS_UC.getFont(18.0)
        tickLabelFill = LCARS_BLUE
        return this
    }
}

fun Parent.lcarsLineGraph(op: LcarsLineGraph.() -> Unit = {}) = LcarsLineGraph().attachTo(this, op)