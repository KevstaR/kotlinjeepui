package com.kluke.project

import com.ginsberg.cirkle.CircularList
import com.kluke.project.datastructures.CircularListIterator
import com.kluke.project.events.CycleViewEvent
import com.kluke.project.events.EncodingEvent
import com.kluke.project.events.TripResetEvent
import com.kluke.project.instruments.OdometerInstrumentTable
import com.kluke.project.instruments.trip.database.TripInstrumentTable
import com.kluke.project.sensors.*
import com.kluke.project.sensors.algorithms.serial.SerialDataTable
import com.kluke.project.viewcomponents.Styles
import com.kluke.project.views.MasterView
import com.kluke.project.views.changeable.*
import javafx.event.EventHandler
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.input.KeyCode.*
import javafx.scene.paint.Color.BLACK
import javafx.stage.Stage
import org.jetbrains.exposed.sql.SchemaUtils.create
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import org.koin.core.component.KoinComponent
import org.koin.core.context.startKoin
import org.koin.core.logger.PrintLogger
import org.koin.core.module.Module
import tornadofx.*
import java.sql.Connection
import kotlin.reflect.KClass

abstract class AbstractApp : App(MasterView::class, Styles::class), KoinComponent {
    private companion object {
        private const val MASTER_WIDTH = 1600.0
        private const val MASTER_HEIGHT = 480.0
    }

    init {
        startKoin {
            modules(koinModules())
            logger(PrintLogger())
        }

        configureDatabase()

        setDiContainer()
        reloadStylesheetsOnFocus()
        reloadViewsOnFocus()
    }

    abstract fun koinModules(): List<Module>

    private fun configureDatabase() {
        // this isolation level is required for sqlite, may not be applicable to other DBMS
        TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE

        transaction {
            addLogger(StdOutSqlLogger)

            create(
                TripInstrumentTable,
                OdometerInstrumentTable,
                TotalRoadDistanceTable,
                RoadSpeedSensorTable,
                TransmissionGearSensorTable,
                TorqueConverterSensorTable,
                ShifterSensorTable,
                SerialDataTable,
                FuelSensorTable,
                EngineTempSensorTable,
            )
        }
    }

    override fun start(stage: Stage) {
        super.start(stage.apply {
//            initStyle(StageStyle.UNDECORATED)
            isResizable = false
            width = MASTER_WIDTH
            height = MASTER_HEIGHT
        })
    }

    private fun setDiContainer() {
        FX.dicontainer = object : DIContainer, KoinComponent {
            override fun <T : Any> getInstance(type: KClass<T>): T =
                getKoin().get(clazz = type, qualifier = null, parameters = null)
        }
    }

    override fun shouldShowPrimaryStage() = true
    override fun createPrimaryScene(view: UIComponent) = scene(view.root)

    private fun scene(root: Parent) = Scene(root, MASTER_WIDTH, MASTER_HEIGHT, BLACK).apply {
        onMouseClicked = EventHandler {
            println("x: ${it.sceneX}, y: ${it.sceneY}")
        }
        onKeyPressed = EventHandler {
            when (it.code) {
                W -> fire(CycleViewEvent(secondaryViews.next()))
                S -> fire(CycleViewEvent(secondaryViews.previous()))
                R -> fire(TripResetEvent)
                E -> fire(EncodingEvent)
                else -> return@EventHandler
            }
        }
    }
}

