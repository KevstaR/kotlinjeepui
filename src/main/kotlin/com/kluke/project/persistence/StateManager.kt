package com.kluke.project.persistence

interface StateManager<T> {
    fun load(): T
    fun reset() {}
    fun save(current: Long, elapsedDurationNanos: Long) {}
}