package com.kluke.project.database

import org.jetbrains.exposed.dao.IntEntity

interface EntityMapper {

    fun newEntity(timeMillis: Long): IntEntity
}