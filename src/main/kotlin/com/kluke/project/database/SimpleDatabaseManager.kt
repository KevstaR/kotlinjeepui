package com.kluke.project.database

import com.kluke.project.instruments.trip.database.TripDatabase
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import tornadofx.cleanBind
import tornadofx.getValue
import tornadofx.objectBinding
import tornadofx.setValue

/**
 * Implements a TripDatabase and encapsulates the logic behind basic load, save, and reset operations.
 */
class SimpleDatabaseManager(private val tripDatabase: TripDatabase) : TripDatabaseManager<Long, Long> {
    private val inputProperty: ObjectProperty<Long> = SimpleObjectProperty(0L)
    private var input by inputProperty
    override fun inputProperty(): ObjectProperty<Long> = inputProperty

    private val outputProperty: ObjectProperty<Long> = SimpleObjectProperty(0L)
    override fun outputProperty(): ObjectProperty<Long> = outputProperty

    private fun binding() = inputProperty.objectBinding { calculate(it ?: 0L) }
    private fun calculate(number: Number) = load().toLong() + number.toLong() - last

    init {
        outputProperty.bind(binding())
    }

    /**
    Zero out the counted states that have already been serialized to the total.
     */
    private var last: Long = 0L

    private fun load() = tripDatabase.read()

    @Synchronized
    override fun save() = tripDatabase.write(load().toLong() + input - last).also { last = input }

    @Synchronized
    override fun reset() {
        tripDatabase.write(0L)
        last = input ?: 0L

        outputProperty.cleanBind(binding())
    }
}