package com.kluke.project.database

import javafx.beans.property.ObjectProperty

/**
 * Manages the logic behind each serialization operation.
 */
interface TripDatabaseManager<A, B> {
    fun save()
    fun reset()
    fun inputProperty(): ObjectProperty<A>
    fun outputProperty(): ObjectProperty<B>
}