package com.kluke.project.database

import com.kluke.project.EventContext
import com.kluke.project.events.TickEvent
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.Instant
import tornadofx.*

class FrameManager(private val eventContext: EventContext) : Component() {

    private val minute = 600L

    fun run() {
        subscribe<TickEvent> { tickEvent ->
            if (tickEvent.tickCount % minute == 0L) {
                val oldEvents = eventsOlderThanOneMinute()

                transaction {
                    addLogger(StdOutSqlLogger)

                    createEntities(oldEvents)
                }

                removeEventsFromContext(oldEvents)
            }
        }
    }

    private fun eventsOlderThanOneMinute() = eventContext.headMap(Instant.now().minus(60_000))

    private fun createEntities(lastMinute: EventContext) = lastMinute.forEach { it.value.newEntity(it.key.millis) }

    private fun removeEventsFromContext(lastMinute: EventContext) = lastMinute.forEach { eventContext.remove(it.key) }
}