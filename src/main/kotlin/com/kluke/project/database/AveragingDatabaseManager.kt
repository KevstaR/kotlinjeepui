package com.kluke.project.database

import com.kluke.project.instruments.trip.database.TripDatabase
import com.kluke.project.services.CumulativeAverager
import com.kluke.project.services.CumulativeMovingAverager
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import tornadofx.*

class AveragingDatabaseManager(
    private val averageDatabase: TripDatabase,
    private val countDatabase: TripDatabase,
) : TripDatabaseManager<Double, Double> {
    private val averager: CumulativeAverager = CumulativeMovingAverager(loadAverage(), loadCount())
    private val inputProperty: ObjectProperty<Double> = SimpleObjectProperty()
    override fun inputProperty() = inputProperty

    private val outputProperty: ObjectProperty<Double> = SimpleObjectProperty()
    override fun outputProperty() = outputProperty

    init {
        inputProperty.onChange { changed -> changed?.let { setOutputProperty(accumulate(it)) } }
    }

    private fun accumulate(value: Double) = averager.accumulateAndGet(loadAverage() + value - last)

    @Synchronized
    private fun setOutputProperty(value: Double) = outputProperty.set(value)

    /**
    Zero out the counted states that have already been serialized to the total.
     */
    private var last = 0.0
    private var lastCount = 0L

    private fun loadAverage() = averageDatabase.read().toDouble()
    private fun loadCount() = countDatabase.read().toLong()

    @Synchronized
    override fun save() {
        saveAverage()
        saveCount()
    }

    private fun saveAverage() = averageDatabase.write(averager.get() - last)
    private fun saveCount() = countDatabase.write(averager.count() - lastCount)

    @Synchronized
    override fun reset() {
        last = 0.0
        lastCount = 0

        averager.reset()

        averageDatabase.write(0.0)
        countDatabase.write(0L)

        outputProperty.set(0.0)
    }
}