package com.kluke.project.instruments

import com.kluke.project.datastructures.ObservableRingBuffer
import com.kluke.project.sensors.Sensor
import javafx.collections.ObservableList
import tornadofx.*

interface FuelInstrument {
    fun observableData(): ObservableList<Int>
}

class FuelInstrumentImpl(sensor: Sensor<Int>) : FuelInstrument {
    private val buffer: ObservableList<Int> = ObservableRingBuffer<Int>(maxCapacity = 50)

    init {
        sensor.property().onChange { buffer += it }
    }

    override fun observableData() = buffer
}