package com.kluke.project.instruments

import com.sothawo.mapjfx.Coordinate
import javafx.beans.property.ObjectProperty
import kotlin.math.*

interface StreetMapInstrument {
    fun coordinateProperty(): ObjectProperty<Coordinate>
}

data class MapTile(val x: Int, val y: Int)

fun latLonUrlBuilder(startCoordinate: Coordinate, endCoordinate: Coordinate, zoom: Int = 16): List<String> {
    // Conversion function from degree to map tile
    fun convert(latDeg: Double, lonDeg: Double, zoom: Int): MapTile {
        val latRad = Math.toRadians(latDeg)
        val n = 2.0.pow(zoom)

        val xTile = (lonDeg + 180.0) / 360.0 * n
        val yTile = (1.0 - asinh(tan(latRad)) / PI) / 2.0 * n

        return MapTile(x = xTile.roundToInt(), y = yTile.roundToInt())
    }

    // Map rectangular selection of tiles by getting the top left coordinate and the bottom right coordinate.
    val start = convert(latDeg = startCoordinate.latitude, lonDeg = startCoordinate.longitude, zoom = zoom)
    val end = convert(latDeg = endCoordinate.latitude, lonDeg = endCoordinate.longitude, zoom = zoom)

    // Generate URL for all coordinates
    val result = mutableListOf<String>()

    // Each tile is available on one of three subdomains. Rotate subdomains to work around port constraints.
    val subdomains = listOf("a", "b", "c")
    for (xTile in start.x..end.x) for (yTile in start.y..end.y) {
        // Incrementally grab a different subdomain for each tile.
        val subdomain = subdomains[(xTile + yTile) % 3]

        // Collect result.
        result += "https://$subdomain.tile.openstreetmap.org/$zoom/$xTile/$yTile.png"
    }

    return result
}