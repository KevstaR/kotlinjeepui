package com.kluke.project.instruments.camera

import com.kluke.project.units.Describable
import javafx.beans.property.ObjectProperty
import javafx.scene.image.Image
import java.io.Closeable

interface CameraInstrument : Describable, Closeable {
    fun start()
    fun property(): ObjectProperty<Image>
    fun encodingStatusProperty(): ObjectProperty<Status>
    fun save()
    fun stop()
}