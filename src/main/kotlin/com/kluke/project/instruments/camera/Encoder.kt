package com.kluke.project.instruments.camera

import java.awt.image.BufferedImage
import java.io.File
import java.util.*

interface Encoder {
    fun encode(frameBuffer: Queue<BufferedImage>, file: File)
}