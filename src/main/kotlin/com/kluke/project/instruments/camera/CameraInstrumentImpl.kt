package com.kluke.project.instruments.camera

import com.github.sarxos.webcam.Webcam
import com.github.sarxos.webcam.WebcamResolution
import com.kluke.project.datastructures.NonBlockingFixedSizeBuffer
import com.kluke.project.events.EncodingEvent
import com.kluke.project.units.Describable
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.embed.swing.SwingFXUtils.toFXImage
import javafx.scene.image.Image
import org.jcodec.api.awt.AWTSequenceEncoder.create30Fps
import org.joda.time.LocalDateTime
import org.joda.time.format.DateTimeFormat
import tornadofx.Component
import tornadofx.finally
import tornadofx.runAsync
import java.awt.image.BufferedImage
import java.io.File
import java.lang.Thread.sleep
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean

class CameraInstrumentImpl(
        private val camera: Webcam,
        private val encoder: Encoder = DefaultEncoder,
        describable: Describable)
    : CameraInstrument, Component() , Describable by describable {

    companion object {
        private const val FPS = 30
        private const val FRAME_BUFFER_SIZE = FPS * 60 * 5 // Five Minutes
    }

    private val encodingLock = AtomicBoolean(false)

    private val frameBuffer: Queue<BufferedImage> = NonBlockingFixedSizeBuffer(FRAME_BUFFER_SIZE)

    private val imageProperty = SimpleObjectProperty<Image>()
    override fun property(): SimpleObjectProperty<Image> = imageProperty

    private val encodingStatusProperty = SimpleObjectProperty(Status.IDLE)
    override fun encodingStatusProperty(): ObjectProperty<Status> = encodingStatusProperty

    override fun start() {
        configureWebcam()
        runAsync {
            while (true) processImage()
        } finally {
            close()
        }
        subscribe<EncodingEvent> { save() }
    }

    private fun configureWebcam() = synchronized(camera) {
        with(camera) {
            viewSize = WebcamResolution.VGA.size
            open()
        }
    }

    private fun processImage() = camera.image?.apply {
        frameBuffer += this

        tornadofx.runLater {
            synchronized(imageProperty) {
                imageProperty.set(toFXImage(this, null))
            }
        }
    }

    override fun save() {
        if (encodingLock.get()) return

        runAsync {
            encodingLock.set(true)
            setEncodingStatusProperty(Status.WORKING)
            encode()
            setEncodingStatusProperty(Status.COMPLETED)
        } finally {
            encodingLock.set(false)
        }
    }

    private fun setEncodingStatusProperty(status: Status) = synchronized(encodingStatusProperty) {
        encodingStatusProperty.set(status)
    }

    override fun stop() = close()

    override fun close() = synchronized(camera) { if (camera.isOpen) camera.close() }

    private fun encode() = encoder.encode(frameBuffer, FileManager().outputFile())

    private inner class FileManager {
        fun outputFile() = File(makeDir(), outputFileName())
        private fun makeDir() = File(id).also { if (!it.exists()) it.mkdir() }
        private fun outputFileName() = "$id-${dateTime()}.mp4"
        private fun dateTime() = DateTimeFormat.forPattern("MM-dd-yyyy-HH-mm-ss").print(LocalDateTime.now())
    }
}