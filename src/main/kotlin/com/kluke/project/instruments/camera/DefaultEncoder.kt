package com.kluke.project.instruments.camera

import org.jcodec.api.awt.AWTSequenceEncoder.create30Fps
import java.awt.image.BufferedImage
import java.io.File
import java.util.*

object DefaultEncoder : Encoder {
    override fun encode(frameBuffer: Queue<BufferedImage>, file: File) = with(create30Fps(file)) {
        try {
            frameBuffer.forEach(::encodeImage)
        } finally {
            finish()
        }
    }
}