package com.kluke.project.instruments.camera

enum class Status(val status: String) {
    IDLE(""), WORKING("ENCODING..."), COMPLETED("COMPLETED")
}
