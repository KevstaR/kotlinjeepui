package com.kluke.project.instruments

import com.kluke.project.persistence.StateManager
import com.kluke.project.sensors.ParkEvent
import com.kluke.project.sensors.RoadSpeedSensor
import com.kluke.project.units.Describable
import com.kluke.project.units.MeasuredUnit
import com.kluke.project.units.Miles
import com.kluke.project.units.SimpleDescribable
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleObjectProperty
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.transactions.transaction
import tornadofx.*
import java.util.concurrent.atomic.AtomicLong

const val STATE_CHANGES = 16.25

class OdometerStateManager : StateManager<Long> {
    // Track the total number of new events since start.
    private val totalSinceStart = AtomicLong(0L)

    override fun load() = transaction {
        loadFirstEntry().distance
    }

    /**
     * Thread safe save.
     */
    override fun save(current: Long, elapsedDurationNanos: Long) =
        with(totalSinceStart.getAndSet(current)) {
            transaction {
                loadFirstEntry().distance += current - this@with
            }
        }

    private fun loadFirstEntry() = OdometerInstrumentEntry.loadById(1)
}

class OdometerInstrument(
    private val roadSpeedSensor: RoadSpeedSensor,
    measuredUnit: MeasuredUnit = Miles,
    private val stateManager: StateManager<Long> = OdometerStateManager(),
) : Component(), VelocityInstrument, Describable by SimpleDescribable(
    name = "ODOMETER",
    abbreviation = "ODO",
    id = "odometer-main"
) {
    private val measuredUnitProperty: ObjectProperty<MeasuredUnit> = SimpleObjectProperty(measuredUnit)
    override fun measuredUnitProperty(): ObjectProperty<MeasuredUnit> = measuredUnitProperty

    private val initial = AtomicLong(stateManager.load())
    private val property = SimpleDoubleProperty(0.0)

    init {
        // Bind to target
        property.bind(binding())

        // Property must be rebound if the measured unit changes.
        measuredUnitProperty().onChange { property.cleanBind(binding()) }

        // Save reading to db on event.
        subscribe<ParkEvent> {
            stateManager.save(roadSpeedSensor.totalCountProperty().get(), 0)
        }
    }

    private fun binding() = roadSpeedSensor.totalCountProperty().doubleBinding {
        measuredUnitProperty().get().conversionFactor() * (it!!.toDouble() + initial.get())
    }

    override fun property() = property

    override fun close() = stateManager.save(roadSpeedSensor.totalCountProperty().get(), 0)
}

object OdometerInstrumentTable : IntIdTable() {
    val time     = long("time")
    val distance = long("distance")
}

class OdometerInstrumentEntry(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<OdometerInstrumentEntry>(OdometerInstrumentTable) {
        fun loadById(id: Int) = findById(id)
            ?: new(id) {
                time = 0
                distance = 0
            }
    }

    var time by OdometerInstrumentTable.time
    var distance by OdometerInstrumentTable.distance

    override fun toString() = """OdometerInstrumentEntry(
        time     = $time,
        distance = $distance
    )""".trimIndent()
}
