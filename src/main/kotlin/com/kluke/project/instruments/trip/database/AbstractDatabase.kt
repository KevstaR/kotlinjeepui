package com.kluke.project.instruments.trip.database

abstract class AbstractDatabase(private val id: Int = 1) : TripDatabase {
    /**
     * Flag for determining if a cached entry is valid in memory or needs to be re-loaded from the database.
     */
    private var valid = false

    /**
     * Table row ID to load entry from. If you wanted to support multiple timer
     * counters, you can use separate IDs for each timer.
     */
    protected fun firstEntry() = TripInstrumentEntry.loadById(id)

    abstract fun readFromDatabase(): Number
    abstract fun writeToDatabase(value: Number)

    /**
     * In memory cached reference.
     */
    private var cached: Number? = null

    private fun readDbAndSet() = readFromDatabase().apply { cached = this }
    override fun read(): Number = if (valid) cached ?: readDbAndSet() else readDbAndSet().also { valid = true }
    override fun write(value: Number) = writeToDatabase(value).also {
        cached = value
        valid = true
    }
}