package com.kluke.project.instruments.trip

import com.kluke.project.database.AveragingDatabaseManager
import com.kluke.project.sensors.RoadSpeedSensor
import com.kluke.project.sensors.algorithms.SpeedAlgorithm
import com.kluke.project.units.Describable
import com.kluke.project.units.MeasuredUnit
import com.kluke.project.units.SimpleDescribable
import tornadofx.cleanBind
import tornadofx.objectBinding

class TripAverageSpeedInstrument(
    private val roadSpeedSensor: RoadSpeedSensor,
    private val averageSpeedManager: AveragingDatabaseManager,
    private val speedAlgorithm: SpeedAlgorithm,

    measuredUnit: MeasuredUnit,
) : AbstractTripInstrument<Double>(measuredUnit), Describable by SimpleDescribable(
    name         = "AVERAGE SPEED",
    abbreviation = "AVG-SPD",
    id           = "trip-average-speed"
) {
    override fun bindInputs() = averageSpeedManager.inputProperty().cleanBind(inputBinding())
    private fun inputBinding() = roadSpeedSensor.frameProperty().objectBinding { it?.delta?.toDouble() }

    override fun bindOutputs() = property().cleanBind(averageSpeedBinding())
    private fun averageSpeedBinding() = averageSpeedManager.outputProperty().objectBinding {
        speedAlgorithm.read(it ?: 0.0) * measuredUnitProperty().get().conversionFactor()
    }

    override fun save() = averageSpeedManager.save()
    override fun reset() = averageSpeedManager.reset()
    override fun close() = save()
}