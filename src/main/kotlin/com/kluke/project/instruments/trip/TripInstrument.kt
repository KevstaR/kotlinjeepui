package com.kluke.project.instruments.trip

import com.kluke.project.units.Describable
import com.kluke.project.units.Measurable
import javafx.beans.property.ObjectProperty
import java.io.Closeable

interface TripInstrument<T> : Measurable, Describable, Closeable {
    fun start()
    fun property(): ObjectProperty<T>
    fun save()
    fun reset()
}