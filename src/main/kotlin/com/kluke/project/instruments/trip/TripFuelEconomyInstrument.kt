package com.kluke.project.instruments.trip

import com.kluke.project.database.AveragingDatabaseManager
import com.kluke.project.sensors.RoadSpeedSensor
import com.kluke.project.sensors.Sensor
import com.kluke.project.sensors.algorithms.SpeedAlgorithm
import com.kluke.project.units.Describable
import com.kluke.project.units.FuelEconomyUnit
import com.kluke.project.units.SimpleDescribable
import tornadofx.cleanBind
import tornadofx.objectBinding
import java.util.logging.Logger

class TripFuelEconomyInstrument(
    private val gallonsPerHourSensor: Sensor<Double>,
    private val roadSpeedSensor: RoadSpeedSensor,

    private val speedAlgorithm: SpeedAlgorithm,

    private val gallonsPerHourAverager: AveragingDatabaseManager,
    private val rawRoadSpeedAverager: AveragingDatabaseManager,

    measuredUnit: FuelEconomyUnit,
) : AbstractTripInstrument<Double>(measuredUnit), Describable by SimpleDescribable(
    name               = "FUEL ECONOMY",
    abbreviation       = "ECO",
    id                 = "trip-fuel-economy"
) {
    override fun bindInputs() {
        rawRoadSpeedAverager.inputProperty().cleanBind(rawDistanceBinding())
        gallonsPerHourAverager.inputProperty().cleanBind(gallonsPerHourSensor.property())
    }

    override fun bindOutputs() = property().cleanBind(gallonsPerHourBinding())

    private fun rawDistanceBinding() = roadSpeedSensor.frameProperty().objectBinding { it?.delta?.toDouble() }

    private fun gallonsPerHourBinding() = gallonsPerHourAverager.outputProperty().objectBinding { gph ->
        if (gph == null || gph == 0.0) return@objectBinding 0.0
        convertedDistance() / gph
    }
    private fun convertedDistance() = speedAlgorithm.read(rawRoadSpeedAverage()) * conversionFactor()
    private fun rawRoadSpeedAverage() = rawRoadSpeedAverager.outputProperty().get()
    private fun conversionFactor() = measuredUnitProperty().get().conversionFactor()


    override fun save() {
        gallonsPerHourAverager.save()
        rawRoadSpeedAverager.save()
    }

    override fun reset() {
        gallonsPerHourAverager.reset()
        rawRoadSpeedAverager.reset()
    }

    override fun close() = save()
}