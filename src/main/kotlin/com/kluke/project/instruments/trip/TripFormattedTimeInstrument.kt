package com.kluke.project.instruments.trip

import com.kluke.project.database.SimpleDatabaseManager
import com.kluke.project.services.TimeService
import com.kluke.project.services.timeFormat
import com.kluke.project.units.Describable
import com.kluke.project.units.MeasuredUnit
import com.kluke.project.units.SimpleDescribable
import org.joda.time.Period
import tornadofx.cleanBind
import tornadofx.stringBinding

class TripFormattedTimeInstrument(
    private val elapsedTimeManager: SimpleDatabaseManager,
    private val timeService: TimeService,

    measuredUnit: MeasuredUnit,
) : AbstractTripInstrument<String>(measuredUnit), Describable by SimpleDescribable(
    name         = "TIME",
    abbreviation = "TIME",
    id           = "trip-formatted-time"
) {
    override fun bindInputs() = elapsedTimeManager.inputProperty().cleanBind(timeService.elapsedDurationProperty().asObject())
    override fun bindOutputs() = property().cleanBind(formattedTimeBinding())

    private fun formattedTimeBinding() = elapsedTimeManager.outputProperty().stringBinding { formattedTime(it ?: 0) }
    private fun formattedTime(value: Long) = timeFormat().print(Period.seconds(value.toInt()).normalizedStandard())

    override fun save() = elapsedTimeManager.save()
    override fun reset() = elapsedTimeManager.reset()
    override fun close() = save()
}