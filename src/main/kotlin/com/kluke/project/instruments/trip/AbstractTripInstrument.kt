package com.kluke.project.instruments.trip

import com.kluke.project.units.Describable
import com.kluke.project.units.MeasuredUnit
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import tornadofx.onChange

abstract class AbstractTripInstrument<T>(
    measuredUnit: MeasuredUnit,
) : TripInstrument<T>, Describable {
    private val property: ObjectProperty<T> = SimpleObjectProperty()
    override fun property(): ObjectProperty<T> = property

    private val measuredUnitProperty: ObjectProperty<MeasuredUnit> = SimpleObjectProperty(measuredUnit)
    override fun measuredUnitProperty(): ObjectProperty<MeasuredUnit> = measuredUnitProperty

    override fun start() {
        bind()
        setRebindMeasuredUnitChange()
    }

    private fun bind() {
        bindInputs()
        bindOutputs()
    }

    abstract fun bindInputs()
    abstract fun bindOutputs()

    /**
     * Properties must be rebound if the measured unit changes.
     */
    private fun setRebindMeasuredUnitChange() = measuredUnitProperty().onChange { bind() }
}