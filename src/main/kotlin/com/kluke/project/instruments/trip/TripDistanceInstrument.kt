package com.kluke.project.instruments.trip

import com.kluke.project.database.SimpleDatabaseManager
import com.kluke.project.sensors.RoadSpeedSensor
import com.kluke.project.units.Describable
import com.kluke.project.units.MeasuredUnit
import com.kluke.project.units.SimpleDescribable
import tornadofx.cleanBind
import tornadofx.objectBinding

class TripDistanceInstrument(
    private val roadSpeedSensor: RoadSpeedSensor,
    private val distanceManager: SimpleDatabaseManager,

    measuredUnit: MeasuredUnit,
    ) : AbstractTripInstrument<Double>(measuredUnit), Describable by SimpleDescribable(
    name         = "DISTANCE",
    abbreviation = "TRP-DIST",
    id           = "trip-distance"
) {
    override fun bindInputs() = distanceManager.inputProperty().cleanBind(roadSpeedSensor.totalCountProperty().asObject())
    override fun bindOutputs() = property().cleanBind(convertedMeasurementBinding())

    private fun convertedMeasurementBinding() = distanceManager.outputProperty().objectBinding { (it?.toDouble() ?: 0.0) * conversionFactor() }
    private fun conversionFactor() = measuredUnitProperty().get().conversionFactor()

    override fun save() = distanceManager.save()
    override fun reset() = distanceManager.reset()
    override fun close() = save()
}