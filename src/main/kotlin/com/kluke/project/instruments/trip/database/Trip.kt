package com.kluke.project.instruments.trip.database

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

object TripInstrumentTable : IntIdTable() {
    val elapsedSeconds    = long("elapsedSeconds")
    val formattedTime     = long("formattedTime")
    val distance          = long("distance")

    val fuelEconomy       = double("fuelEconomy")
    val fuelEconomyCount  = long("fuelEconomyCount")

    val rawDistance       = double("rawDistance")
    val rawDistanceCount  = long("rawDistanceCount")

    val averageSpeed      = double("averageSpeed")
    val averageSpeedCount = long("averageSpeedCount")
}

class TripInstrumentEntry(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<TripInstrumentEntry>(TripInstrumentTable) {
        /**
         * Load the entry from the database if it exists.  Otherwise,
         * create a new entry equal to the main odometer table entry.
         */
        fun loadById(id: Int) = findById(id)
            ?: new(id) {
//                val time = now().millis
                elapsedSeconds = 0L//time
                formattedTime = 0L//time
                distance = 0L

                fuelEconomy = 0.0
                fuelEconomyCount = 0L

                rawDistance = 0.0
                rawDistanceCount = 0L

                averageSpeed = 0.0
                averageSpeedCount = 0L
            }
    }

    var elapsedSeconds by TripInstrumentTable.elapsedSeconds
    var formattedTime by TripInstrumentTable.formattedTime
    var distance by TripInstrumentTable.distance
    var fuelEconomy by TripInstrumentTable.fuelEconomy
    var fuelEconomyCount by TripInstrumentTable.fuelEconomyCount
    var rawDistance by TripInstrumentTable.rawDistance
    var rawDistanceCount by TripInstrumentTable.rawDistanceCount
    var averageSpeed by TripInstrumentTable.averageSpeed
    var averageSpeedCount by TripInstrumentTable.averageSpeedCount

    override fun toString() = """TripInstrumentEntry(
        elapsedSeconds    = $elapsedSeconds,
        formattedTime     = $formattedTime,
        distance          = $distance,
        fuelEconomy       = $fuelEconomy,
        fuelEconomyCount  = $fuelEconomyCount,
        rawDistance       = $rawDistance,
        rawDistanceCount  = $rawDistanceCount,
        averageSpeed      = $averageSpeed,
        averageSpeedCount = $averageSpeedCount,
    )""".trimIndent()
}