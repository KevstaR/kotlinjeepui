package com.kluke.project.instruments.trip.database

import org.jetbrains.exposed.sql.transactions.transaction

class TripDistanceDatabase : AbstractDatabase() {
    override fun readFromDatabase() = transaction { firstEntry() }.distance
    override fun writeToDatabase(value: Number) = transaction { firstEntry().distance = value.toLong() }
}

class TripAverageSpeedDatabase : AbstractDatabase() {
    override fun readFromDatabase() = transaction { firstEntry() }.averageSpeed
    override fun writeToDatabase(value: Number) = transaction { firstEntry().averageSpeed = value.toDouble() }
}

class TripAverageSpeedDatabaseCount : AbstractDatabase() {
    override fun readFromDatabase() = transaction { firstEntry() }.averageSpeedCount
    override fun writeToDatabase(value: Number) = transaction { firstEntry().averageSpeedCount = value.toLong() }
}

class TripFormattedTimeDatabase : AbstractDatabase() {
    override fun readFromDatabase() = transaction { firstEntry() }.formattedTime
    override fun writeToDatabase(value: Number) = transaction { firstEntry().formattedTime = value.toLong() }
}

class TripFuelEconomyDatabase : AbstractDatabase() {
    override fun readFromDatabase() = transaction { firstEntry() }.fuelEconomy
    override fun writeToDatabase(value: Number) = transaction { firstEntry().fuelEconomy = value.toDouble() }
}

class TripFuelEconomyCountDatabase : AbstractDatabase() {
    override fun readFromDatabase() = transaction { firstEntry().fuelEconomyCount }
    override fun writeToDatabase(value: Number) = transaction { firstEntry().fuelEconomyCount = value.toLong() }
}

class TripRawDistanceDatabase : AbstractDatabase() {
    override fun readFromDatabase(): Number = transaction { firstEntry().rawDistance }
    override fun writeToDatabase(value: Number) = transaction { firstEntry().rawDistance = value.toDouble() }
}

class TripRawDistanceCountDatabase : AbstractDatabase() {
    override fun readFromDatabase(): Number = transaction { firstEntry().rawDistanceCount }
    override fun writeToDatabase(value: Number) = transaction { firstEntry().rawDistanceCount = value.toLong() }
}