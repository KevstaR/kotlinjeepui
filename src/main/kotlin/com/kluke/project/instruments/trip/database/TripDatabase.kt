package com.kluke.project.instruments.trip.database

/**
 * Abstraction of underlying database source as a raw type wrapper result instead of a DBO.
 */
interface TripDatabase {
    fun read(): Number
    fun write(value: Number)
}