package com.kluke.project.instruments

import com.kluke.project.units.Describable
import com.kluke.project.units.Measurable
import javafx.beans.property.DoubleProperty
import java.io.Closeable

interface VelocityInstrument : Describable, Measurable, Closeable {
    fun property(): DoubleProperty

    /**
     * Some instruments can be saved.
     */
    fun save() {}

    /**
     * For serialization of values and clean up.
     */
    override fun close() {}
}