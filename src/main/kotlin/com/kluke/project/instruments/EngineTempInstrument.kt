package com.kluke.project.instruments

import com.kluke.project.instruments.EngineTempInstrument.Companion.MAX_TEMP_KELVIN
import com.kluke.project.sensors.Sensor
import com.kluke.project.sensors.algorithms.SensorAlgorithm
import com.kluke.project.sensors.algorithms.TemperatureAlgorithm
import com.kluke.project.units.Fahrenheit
import com.kluke.project.units.TemperatureUnit
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleObjectProperty
import tornadofx.*

interface EngineTempInstrument {

    companion object {
        const val MAX_TEMP_KELVIN = 411.28
    }

    fun unitProperty(): SimpleObjectProperty<TemperatureUnit>
    fun temperatureProperty(): SimpleDoubleProperty
}

class EngineTempInstrumentImpl(
        private val sensor: Sensor<Int>,
        unit: TemperatureUnit = Fahrenheit,
        private val algorithm: SensorAlgorithm<Int, Double> = TemperatureAlgorithm()) : EngineTempInstrument {

    private val temperatureProperty = SimpleDoubleProperty(0.0)
    private val unitProperty = SimpleObjectProperty(unit)

    override fun temperatureProperty() = temperatureProperty
    override fun unitProperty() = unitProperty

    init {
        // Property must be rebound if the measured unit changes.
        temperatureProperty.bind(binding())
        unitProperty.onChange { temperatureProperty.cleanBind(binding()) }
    }

    private fun binding() = sensor.property().doubleBinding {
        if (it == null || it <= 0) return@doubleBinding 0.0
        if (it < 19) return@doubleBinding unitProperty().get().convert(MAX_TEMP_KELVIN)

        val kelvin = algorithm.read(it)

        unitProperty().get().convert(kelvin)
    }
}