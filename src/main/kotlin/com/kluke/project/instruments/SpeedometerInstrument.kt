package com.kluke.project.instruments

import com.kluke.project.sensors.RoadSpeedSensor
import com.kluke.project.sensors.algorithms.SpeedAlgorithm
import com.kluke.project.units.*
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleObjectProperty
import tornadofx.cleanBind
import tornadofx.doubleBinding
import tornadofx.onChange

/**
 * Base Speed and Acceleration units are in Metric with default settings to pass through Metric.
 */
class SpeedometerInstrument(
    private val roadSpeedSensor: RoadSpeedSensor,
    private val speedAlgorithm: SpeedAlgorithm,
    measuredUnit: MeasuredUnit = MilesPerHour) : VelocityInstrument, Measurable, Describable by SimpleDescribable(
        name = "SPEEDOMETER",
        abbreviation = "SPD",
        id = "speedometer-main"
    ) {
    private val measuredUnitProperty: ObjectProperty<MeasuredUnit> = SimpleObjectProperty(measuredUnit)
    override fun measuredUnitProperty(): ObjectProperty<MeasuredUnit> = measuredUnitProperty

    private val property = SimpleDoubleProperty(0.0)

    init {
        // Property must be rebound if the measured unit changes.
        property.bind(binding())
        measuredUnitProperty().onChange { property.cleanBind(binding()) }
    }

    private fun binding() = roadSpeedSensor.frameProperty().doubleBinding {
        speedAlgorithm.read(it?.delta ?: 0.0) * measuredUnitProperty().get().conversionFactor()
    }

    override fun property() = property
}
