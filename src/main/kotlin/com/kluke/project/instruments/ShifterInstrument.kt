package com.kluke.project.instruments

import com.kluke.project.sensors.Sensor
import com.kluke.project.sensors.enums.ShifterPosition
import javafx.beans.binding.DoubleBinding
import javafx.beans.property.DoubleProperty
import javafx.beans.property.SimpleDoubleProperty
import tornadofx.*

interface ShifterInstrument {
    fun shifterTextPositionProperty(): DoubleProperty
}

class ShifterInstrumentImpl(private val shifterSensor: Sensor<ShifterPosition>) : ShifterInstrument {
    private val shiftPositions = ShifterPosition
            .values()
            .mapIndexed { index, shifterPosition ->
                val startingPosition = 95.0
                val multiplier       = -index - 1
                val spacing          = 35.0
                val shiftedPosition  = startingPosition + multiplier * spacing

                // Correct last position for extra width
                val finalPosition = if (index == ShifterPosition.values().lastIndex) shiftedPosition - 22.0
                else shiftedPosition

                shifterPosition to finalPosition
            }.toMap()

    private val shifterTextPositionProperty: DoubleProperty = SimpleDoubleProperty()

    init {
        shifterTextPositionProperty.bind(shifterSensor.property().doubleBinding { shiftPositions[it]!! })
    }

    override fun shifterTextPositionProperty(): DoubleProperty = shifterTextPositionProperty
}