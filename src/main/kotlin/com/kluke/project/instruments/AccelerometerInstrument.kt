package com.kluke.project.instruments

import com.kluke.project.sensors.POLLING_INTERVAL
import com.kluke.project.sensors.RoadSpeedSensor
import com.kluke.project.sensors.algorithms.SpeedAlgorithm
import com.kluke.project.units.Describable
import com.kluke.project.units.MeasuredUnit
import com.kluke.project.units.MilesPerHourPerSecond
import com.kluke.project.units.SimpleDescribable
import javafx.beans.binding.Bindings.createDoubleBinding
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.ObservableList
import tornadofx.cleanBind
import tornadofx.onChange

class AccelerometerInstrument(
    private val roadSpeedSensor: RoadSpeedSensor,
    private val speedAlgorithm: SpeedAlgorithm,
    measuredUnit: MeasuredUnit = MilesPerHourPerSecond
) : VelocityInstrument, Describable by SimpleDescribable(
    name = "ACCELEROMETER",
    abbreviation = "ACC",
    id = "accelerometer-main"
) {
    private val measuredUnitProperty: ObjectProperty<MeasuredUnit> = SimpleObjectProperty(measuredUnit)
    override fun measuredUnitProperty(): ObjectProperty<MeasuredUnit> = measuredUnitProperty

    private val property = SimpleDoubleProperty(0.0)

    init {
        // Property must be rebound if the measured unit changes.
        property.bind(binding())
        measuredUnitProperty().onChange { property.cleanBind(binding()) }
    }

    private fun binding() = createDoubleBinding(
        { acceleration(roadSpeedSensor.deltaBufferProperty()) },
        roadSpeedSensor.deltaBufferProperty()
    )

    override fun property() = property

    private fun acceleration(deltaBuffer: ObservableList<Int>): Double {
        val speedsBuffer = deltaBuffer.map { speedAlgorithm.read(it.toDouble()) }

        val splitData = speedsBuffer.chunked(deltaBuffer.size / 2)
        val speedAverages = splitData.map { it.average() }

        val initialSpeed = speedAverages.first()
        val finalSpeed = speedAverages.last()

        val time = (deltaBuffer.size * POLLING_INTERVAL / 1000.0) / 2.0

        val speedDifference = finalSpeed - initialSpeed

        return (speedDifference / time) * measuredUnitProperty().get().conversionFactor()
    }
}