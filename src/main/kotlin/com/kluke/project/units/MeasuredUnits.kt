package com.kluke.project.units

import com.kluke.project.instruments.STATE_CHANGES
import javafx.beans.property.ObjectProperty
import kotlin.math.PI

interface Measurable {
    fun measuredUnitProperty(): ObjectProperty<MeasuredUnit>
}

interface MeasuredUnit : Describable {
    fun conversionFactor(): Double
}

object Kilometers : MeasuredUnit, Describable by SimpleDescribable(
        name = "KILOMETERS",
        abbreviation = "KM",
        id = "km-descriptor"
) {
    private const val WHEEL_SIZE_CM = 81.28
    private const val CM_IN_KM = 100_000.0

    private const val WHEEL_CIRCUMFERENCE_CM = WHEEL_SIZE_CM * PI
    private const val CM_PER_STATE_CHANGE = WHEEL_CIRCUMFERENCE_CM / STATE_CHANGES
    private const val DISTANCE_OF_MILE_TRAVELED = CM_PER_STATE_CHANGE / CM_IN_KM

    override fun conversionFactor() = DISTANCE_OF_MILE_TRAVELED
}

/**
Old value with 11 state changes: 0.00014424208694168014
New value with experimental live data state changes (16.25): 0.00009764079731436809
 */
object Miles : MeasuredUnit, Describable by SimpleDescribable(
        name = "MILES",
        abbreviation = "MI",
        id = "mi-descriptor"

) {
    private const val WHEEL_SIZE_INCHES = 32.0
    private const val INCHES_IN_MILE = 63_360.0

    private const val WHEEL_CIRCUMFERENCE_INCH = WHEEL_SIZE_INCHES * PI
    private const val INCHES_PER_STATE_CHANGE = WHEEL_CIRCUMFERENCE_INCH / STATE_CHANGES
    private const val DISTANCE_OF_MILE_TRAVELED = INCHES_PER_STATE_CHANGE / INCHES_IN_MILE

    override fun conversionFactor() = DISTANCE_OF_MILE_TRAVELED
}

object KilometersPerHour : MeasuredUnit, Describable by SimpleDescribable(
        name = "KILOMETERS PER HOUR",
        abbreviation = "KPH",
        id = "kph-descriptor"
) {
    override fun conversionFactor() = 1.0
}

object MilesPerHour : MeasuredUnit, Describable by SimpleDescribable(
        name = "MILES PER HOUR",
        abbreviation = "MPH",
        id = "mph-descriptor"
) {
    override fun conversionFactor() = 0.6214
}

object KilometersPerHourPerSecond : MeasuredUnit, Describable by SimpleDescribable(
        name = "KILOMETERS PER HOUR PER SECOND",
        abbreviation = "KM/H/S",
        id = "kmh-descriptor"
) {
    override fun conversionFactor() = 1.0 // km/h/s (kilometers per hour per second)
}

object MilesPerHourPerSecond : MeasuredUnit, Describable by SimpleDescribable(
        name = "MILES PER HOUR PER SECOND",
        abbreviation = "MI/H/S",
        id = "mihs-descriptor"
) {
    override fun conversionFactor() = 0.6214 // mi/h/s (miles per hour per second)
}

object GravitationalAcceleration : MeasuredUnit, Describable by SimpleDescribable(
        name = "GRAVITATIONAL ACCELERATION",
        abbreviation = "G",
        id = "g-descriptor"
) {
    override fun conversionFactor() = 0.02833 // g (standard accelerations due to gravity on the surface of the earth)
}

interface FuelEconomyUnit : MeasuredUnit

object MilesPerGallon : FuelEconomyUnit, Describable by SimpleDescribable(
    name = "MILES PER GALLON",
    abbreviation = "MPG",
    id = "mpg-descriptor"
) {
    override fun conversionFactor(): Double = MilesPerHour.conversionFactor()
}

object KilometersPerGallon : FuelEconomyUnit, Describable by SimpleDescribable(
    name = "KILOMETERS PER GALLON",
    abbreviation = "KPG",
    id = "kpg-descriptor"
) {
    override fun conversionFactor(): Double = KilometersPerHour.conversionFactor()
}

interface TemperatureUnit : Describable {
    fun convert(value: Double): Double
}

object Fahrenheit : TemperatureUnit, Describable by SimpleDescribable(
        name = "FAHRENHEIT",
        abbreviation = "F",
        id = "f-descriptor"
) {
    /**
     * Convert from Kelvin to Fahrenheit.
     */
    override fun convert(value: Double) = 1.8 * (value - 273.15) + 32.0
}

object Celsius : TemperatureUnit, Describable by SimpleDescribable(
        name = "CELSIUS",
        abbreviation = "C",
        id = "c-descriptor"
) {
    override fun convert(value: Double) = value - 273.15
}