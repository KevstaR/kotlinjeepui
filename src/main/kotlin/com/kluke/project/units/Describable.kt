package com.kluke.project.units

import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import tornadofx.*

interface Describable {
    fun nameProperty(): StringProperty
    fun abbreviationProperty(): StringProperty

    val name: String
    val abbreviation: String
    val id: String
}

class SimpleDescribable(
        name: String,
        abbreviation: String,
        override val id: String
) : Describable {
    override fun nameProperty() = nameProperty
    override fun abbreviationProperty() = abbreviationProperty

    private val nameProperty = SimpleStringProperty(name)
    private val abbreviationProperty = SimpleStringProperty(abbreviation)

    override val name: String by nameProperty
    override val abbreviation: String by abbreviationProperty
}