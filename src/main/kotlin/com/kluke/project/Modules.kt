package com.kluke.project

import com.github.sarxos.webcam.Webcam
import com.kluke.project.AnalogPins.ENGINE_TEMP_SENSOR_PIN
import com.kluke.project.AnalogPins.FUEL_SENSOR_PIN
import com.kluke.project.DigitalPins.*
import com.kluke.project.Instruments.*
import com.kluke.project.Sensors.*
import com.kluke.project.ServiceConfiguration.EVENT_CONTEXT
import com.kluke.project.ServiceConfiguration.SERIAL_DATA_SOURCE
import com.kluke.project.ServiceConfiguration.TICKER_100MS
import com.kluke.project.database.AveragingDatabaseManager
import com.kluke.project.database.SimpleDatabaseManager
import com.kluke.project.executors.DebounceExecutor
import com.kluke.project.executors.DebounceExecutorImpl
import com.kluke.project.instruments.*
import com.kluke.project.instruments.camera.CameraInstrument
import com.kluke.project.instruments.camera.CameraInstrumentImpl
import com.kluke.project.instruments.trip.*
import com.kluke.project.instruments.trip.database.*
import com.kluke.project.sensors.*
import com.kluke.project.sensors.algorithms.SpeedAlgorithm
import com.kluke.project.sensors.algorithms.SpeedAlgorithmImpl
import com.kluke.project.sensors.enums.ShifterPosition
import com.kluke.project.sensors.enums.TorqueConverter
import com.kluke.project.sensors.enums.TransmissionGear
import com.kluke.project.services.Service
import com.kluke.project.services.TickService
import com.kluke.project.services.TimeService
import com.kluke.project.services.TimeServiceImpl
import com.kluke.project.units.Miles
import com.kluke.project.units.MilesPerGallon
import com.kluke.project.units.MilesPerHour
import com.kluke.project.units.SimpleDescribable
import com.pi4j.io.gpio.*
import org.koin.core.qualifier.named
import org.koin.dsl.module
import java.util.concurrent.ConcurrentSkipListMap
import java.util.concurrent.Executors

val main = module {
    // Executors
    single<DebounceExecutor> { DebounceExecutorImpl() }
    single { Executors.newScheduledThreadPool(4) }

    // Services
    single<Service>(qualifier = named(TICKER_100MS), createdAtStart = true) {
        TickService(get())
    }

    single<TimeService> { TimeServiceImpl(get()) }

    // Event context
    single<EventContext>(qualifier = named(EVENT_CONTEXT), createdAtStart = true) {
        ConcurrentSkipListMap(Comparator.comparingLong { it.millis })
    }
}

val inputPins = module {
    fun createGpioPinDigitalInput(enum: DigitalPins) = single<GpioPinDigitalInput>(named(enum), createdAtStart = true) {
        get<GpioController>().provisionDigitalInputPin(get(named(enum)), PinPullResistance.PULL_UP)
    }
    for (pinName in DigitalPins.values()) createGpioPinDigitalInput(pinName)

    fun createGpioPinAnalogInput(enum: AnalogPins) = single<GpioPinAnalogInput>(named(enum), createdAtStart = true) {
        get<GpioController>().provisionAnalogInputPin(get(named(enum)))
    }
    for (pinName in AnalogPins.values()) createGpioPinAnalogInput(pinName)
}

val sensors = module {
    single<RoadSpeedSensor>(qualifier = named(ROAD_SPEED), createdAtStart = true) {
        RoadSpeedSensorImpl(pin = get(named(SPEEDOMETER_PIN)), eventContext = get(named(EVENT_CONTEXT)))
    }
    single<Sensor<TransmissionGear>>(qualifier = named(TRANSMISSION_GEAR)) {
        TransmissionGearSensorImpl(
            solenoidPin1 = get(named(TRANSMISSION_SOLENOID1_PIN)),
            solenoidPin2 = get(named(TRANSMISSION_SOLENOID2_PIN)),
            debounceExecutor = get(),
            eventContext = get(named(EVENT_CONTEXT))
        )
    }
    single<Sensor<TorqueConverter>>(qualifier = named(TORQUE_CONVERTER)) {
        TorqueConverterSensorImpl(
            solenoidTcPin = get(named(TRANSMISSION_SOLENOID3_PIN)),
            debounceExecutor = get(),
            eventContext = get(named(EVENT_CONTEXT))
        )
    }
    single<Sensor<ShifterPosition>>(qualifier = named(SHIFTER_POSITION)) {
        ShifterSensor(
            nssReversePin = get(named(NEUTRAL_SAFETY_SWITCH_REVERSE_PIN)),
            nssThirdPin = get(named(NEUTRAL_SAFETY_SWITCH_THIRD_PIN)),
            nssSecondFirstPin = get(named(NEUTRAL_SAFETY_SWITCH_SECOND_FIRST_PIN)),
            nssHallEffectPin = get(named(NEUTRAL_SAFETY_SWITCH_HALL_EFFECT_PIN)),
            debounceExecutor = get(),
            eventContext = get(named(EVENT_CONTEXT))
        )
    }

    single<Sensor<Int>>(qualifier = named(FUEL_SENSOR), createdAtStart = true) {
        FuelSensor(scheduler = get(), pin = get(named(FUEL_SENSOR_PIN)), eventContext = get(named(EVENT_CONTEXT)))
    }

    single<Sensor<Int>>(qualifier = named(ENGINE_TEMP_SENSOR), createdAtStart = true) {
        EngineTempSensor(pin = get(named(ENGINE_TEMP_SENSOR_PIN)), eventContext = get(named(EVENT_CONTEXT)))
    }

    single<Sensor<PinState>>(qualifier = named(E_BRAKE_SENSOR)) {
        EBrakeSensor(pin = get(named(E_BRAKE_SENSOR_PIN)))
    }

    single<Sensor<PinState>>(qualifier = named(LEFT_SIGNAL_SENSOR)) {
        SignalSensor(pin = get(named(LEFT_SIGNAL_SENSOR_PIN)))
    }

    single<Sensor<PinState>>(qualifier = named(RIGHT_SIGNAL_SENSOR)) {
        SignalSensor(pin = get(named(RIGHT_SIGNAL_SENSOR_PIN)))
    }

    single<Sensor<PinState>>(qualifier = named(LIGHTS_SIGNAL_SENSOR)) {
        SignalSensor(pin = get(named(LIGHTS_SIGNAL_SENSOR_PIN)))
    }

    single<Sensor<PinState>>(qualifier = named(BRIGHTS_SIGNAL_SENSOR)) {
        SignalSensor(pin = get(named(BRIGHTS_SIGNAL_SENSOR_PIN)))
    }

    single<Sensor<Double>>(qualifier = named(GALLONS_PER_HOUR_SENSOR)) {
        GallonsPerHourSensor(get(named(SERIAL_DATA_SOURCE)))
    }
}

val instruments = module {
    single<SpeedAlgorithm> {
        SpeedAlgorithmImpl()
    }

    single<VelocityInstrument>(qualifier = named(SPEEDOMETER_INSTRUMENT)) {
        SpeedometerInstrument(roadSpeedSensor = get(named(ROAD_SPEED)), speedAlgorithm = get())
    }

    single<VelocityInstrument>(qualifier = named(ACCELEROMETER_INSTRUMENT)) {
        AccelerometerInstrument(roadSpeedSensor = get(named(ROAD_SPEED)), speedAlgorithm = get())
    }

    single<VelocityInstrument>(qualifier = named(ODOMETER_INSTRUMENT)) {
        OdometerInstrument(roadSpeedSensor = get(named(ROAD_SPEED)))
    }

    single<TripInstrument<Double>>(named(TRIP_DISTANCE_INSTRUMENT)) {
        TripDistanceInstrument(
            roadSpeedSensor      = get(named(ROAD_SPEED)),
            distanceManager      = SimpleDatabaseManager(TripDistanceDatabase()),
            measuredUnit         = Miles,
        )
    }

    single<TripInstrument<Double>>(named(TRIP_AVERAGE_SPEED_INSTRUMENT)) {
        TripAverageSpeedInstrument(
            roadSpeedSensor     = get(named(ROAD_SPEED)),
            averageSpeedManager = AveragingDatabaseManager(TripAverageSpeedDatabase(), TripAverageSpeedDatabaseCount()),
            speedAlgorithm      = get(),
            measuredUnit        = MilesPerHour,
        )
    }

    single<TripInstrument<String>>(named(TRIP_FORMATTED_TIME_INSTRUMENT)) {
        TripFormattedTimeInstrument(
            elapsedTimeManager = SimpleDatabaseManager(TripFormattedTimeDatabase()),
            timeService        = get(),
            measuredUnit       = MilesPerHour,
        )
    }

    single<TripInstrument<Double>>(named(TRIP_FUEL_ECONOMY_INSTRUMENT)) {
        TripFuelEconomyInstrument(
            gallonsPerHourSensor   = get(named(GALLONS_PER_HOUR_SENSOR)),
            roadSpeedSensor        = get(named(ROAD_SPEED)),
            speedAlgorithm         = get(),
            gallonsPerHourAverager = AveragingDatabaseManager(TripFuelEconomyDatabase(), TripFuelEconomyCountDatabase()),
            rawRoadSpeedAverager   = AveragingDatabaseManager(TripRawDistanceDatabase(), TripRawDistanceCountDatabase()),
            measuredUnit           = MilesPerGallon,
        )
    }

    single<ShifterInstrument> { ShifterInstrumentImpl(shifterSensor = get(named(SHIFTER_POSITION))) }

    single<FuelInstrument>(createdAtStart = true) {
        FuelInstrumentImpl(sensor = get(named(FUEL_SENSOR)))
    }

    single<EngineTempInstrument>(createdAtStart = true) {
        EngineTempInstrumentImpl(sensor = get(named(ENGINE_TEMP_SENSOR)))
    }

    single<CameraInstrument>(named(FRONT_CAMERA)) {
        CameraInstrumentImpl(
                camera = Webcam.getDefault(),
                describable = SimpleDescribable(
                name               = "FRONT CAMERA",
                abbreviation       = "FC",
                id                 = "front-camera"
        ))
    }

    factory<List<TripInstrument<out Any>>>(qualifier = named(TRIP_INSTRUMENTS)) {
        listOf(
            get<TripInstrument<Double>>(named(TRIP_DISTANCE_INSTRUMENT)),
            get<TripInstrument<Double>>(named(TRIP_AVERAGE_SPEED_INSTRUMENT)),
            get<TripInstrument<String>>(named(TRIP_FORMATTED_TIME_INSTRUMENT)),
            get<TripInstrument<Double>>(named(TRIP_FUEL_ECONOMY_INSTRUMENT)),
        )
    }
}
