package com.kluke.project

class MainApp : AbstractApp() {
    override fun koinModules() = listOf(main, inputPins, sensors, instruments)
}