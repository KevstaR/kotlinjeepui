package com.kluke.project.executors

import kotlinx.coroutines.*
import java.util.concurrent.ConcurrentHashMap


/**
 * Debounce a given command by placing an arbitrary key in a hash set. The key is removed after a given delay.
 *
 * Example:
 * Addition of a key into the hash set represents a fixed duration of "debounce". If the same key is inserted again,
 * the hash set does not allow duplicates, so it will be ignored.
 *
 * Additionally, if the key already exists in the hash set, no action is taken.
 */
class DebounceExecutorImpl : DebounceExecutor {
    private companion object {
        private const val VALUE = ""
    }
    private val debounceHash = ConcurrentHashMap<Any, Any?>()

    override fun execute(key: Any, delay: Long, op: () -> Unit) {
            if (debounceHash.containsKey(key)) return

            debounceHash[key] = VALUE

        runBlocking {
            launch {
                delay(delay)
                op()
                debounceHash.remove(key)
            }
        }
    }
}