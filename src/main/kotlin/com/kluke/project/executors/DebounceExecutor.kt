package com.kluke.project.executors

interface DebounceExecutor {
    fun execute(key: Any, delay: Long, op: () -> Unit)
}