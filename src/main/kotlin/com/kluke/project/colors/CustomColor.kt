package com.kluke.project.colors

import javafx.scene.paint.Color
import javafx.scene.paint.Color.rgb

val LCARS_LIGHTBLUE   : Color = rgb(154, 153, 255)
val LCARS_BLUE        : Color = rgb(152, 152, 203)
val LCARS_LIGHTPINK   : Color = rgb(202, 152, 202)
val LCARS_PINK        : Color = rgb(203, 101, 152)
val LCARS_CORAL       : Color = rgb(204, 102, 102)
val LCARS_LIGHTORANGE : Color = rgb(254, 151, 100)
val LCARS_ORANGE      : Color = rgb(254, 152, 0)
val LCARS_YELLOW      : Color = rgb(253, 203, 101)
val LCARS_PEACH       : Color = rgb(255, 204, 154)
val LCARS_CONTROL     : Color = rgb(62 , 122, 179)

fun lcarsColors() = listOf(
        LCARS_LIGHTBLUE,
        LCARS_BLUE,
        LCARS_LIGHTPINK,
        LCARS_PINK,
        LCARS_CORAL,
        LCARS_LIGHTORANGE,
        LCARS_ORANGE,
        LCARS_YELLOW,
        LCARS_PEACH,
        LCARS_CONTROL
)

