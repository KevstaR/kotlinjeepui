package com.kluke.project.smoothing

fun List<Pair<Number, Number>>.kernelRegression(kernel: Kernel = GaussianKernel()): List<Pair<Number, Number>> {
    // Determine density of result
    val kernelDensityEstimationRange = 0..size

    // Prepare data point collections
    val xyUnzip = unzip()
    val x = xyUnzip.first
    val y = xyUnzip.second

    val result = mutableListOf<Pair<Number, Number>>()
    for (xEstimate in kernelDensityEstimationRange) {
        // Apply Kernel to all points on the x range of real data, offset by the estimate range.
        val kernelResult: List<Double> = x.map { kernel.apply(xEstimate - it.toDouble()) }

        // Generate weights
        val kernelSum = kernelResult.sum()
        val weight = kernelResult.map { it / kernelSum }

        // Apply weights to each y value
        val yK = (weight zip y).map { it.first * it.second.toDouble() }.sum()
        val xKyK = xEstimate to yK

        // Collect result
        result += xKyK
    }
    // Create immutable collection
    return result
}

fun Map<Int, Number>.kernelRegression(kernel: Kernel = GaussianKernel()): Map<Number, Double> {
    // Determine density of result
    val kernelDensityEstimationRange = 0..size

    // Prepare data point collections
    val x = keys
    val y = values

    val result = linkedMapOf<Number, Double>()
    for (xEstimate in kernelDensityEstimationRange) {
        // Apply Kernel to all points on the x range of real data, offset by the estimate range.
        val kernelResult: List<Double> = x.map { kernel.apply(xEstimate - it.toDouble()) }

        // Generate weights
        val kernelSum = kernelResult.sum()
        val weight = kernelResult.map { it / kernelSum }

        // Apply weights to each y value
        val yK = (weight zip y).map { it.first * it.second.toDouble() }.sum()
        val xKyK: Pair<Int, Double> = xEstimate to yK

        // Collect result
        result += xKyK
    }
    // Create immutable collection
    return result
}