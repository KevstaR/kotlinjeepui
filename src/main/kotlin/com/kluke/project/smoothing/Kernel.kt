package com.kluke.project.smoothing

import kotlin.math.PI
import kotlin.math.exp
import kotlin.math.pow
import kotlin.math.sqrt

interface Kernel {
    fun apply(x: Double): Double
}

class GaussianKernel(private val bandwidth: Double = 20.0) : Kernel {
    override fun apply(x: Double) = (1 / (bandwidth * sqrt(2 * PI))) * exp(-0.5 * (x / bandwidth).pow(2))
}