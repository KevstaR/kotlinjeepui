package com.kluke.project.events

import tornadofx.*
import kotlin.reflect.KClass

class CycleViewEvent(val viewReference: KClass<out View>) : FXEvent()