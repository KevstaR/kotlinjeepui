package com.kluke.project.events

import tornadofx.*
import tornadofx.EventBus.RunOn.BackgroundThread

class TickEvent(val tickCount: Long, val delta: Long) : FXEvent(BackgroundThread)