package com.kluke.project

import com.kluke.project.database.EntityMapper
import org.joda.time.Instant
import java.util.concurrent.ConcurrentNavigableMap

typealias EventContext = ConcurrentNavigableMap<Instant, EntityMapper>

inline fun <reified T : EntityMapper> EventContext.getType(key: Instant): Map<Instant, T> =
        LinkedHashMap<Instant, T>().apply {
            for (entry in tailMap(key)) if (entry.value is T) this[entry.key] = entry.value as T
        }