package com.kluke.project

fun Double.map(inMin: Double = 0.0, inMax: Double, outMin: Double = 0.0, outMax: Double = 100.0): Double =
        (this - inMin) * (outMax - outMin) / (inMax - inMin) + outMin

fun Int.map(inMin: Int = 0, inMax: Int, outMin: Int = 0, outMax: Int = 100): Double =
        (toDouble() - inMin) * (outMax - outMin) / (inMax - inMin) + outMin

fun Number.map(input: IntRange, output: IntRange): Double =
    (toDouble() - input.first) * (output.last - output.first) / (input.last - input.first) + output.first
