package com.kluke.project.controllers

import com.kluke.project.Sensors.*
import com.kluke.project.sensors.Sensor
import com.pi4j.io.gpio.PinState
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.qualifier.named
import tornadofx.*

class SignalViewController : Controller(), KoinComponent {
    val leftSignalSensor: Sensor<PinState> = get(named(LEFT_SIGNAL_SENSOR))
    val rightSignalSensor: Sensor<PinState> = get(named(RIGHT_SIGNAL_SENSOR))
    val lightsSignalSensor: Sensor<PinState> = get(named(LIGHTS_SIGNAL_SENSOR))
    val brightsSignalSensor: Sensor<PinState> = get(named(BRIGHTS_SIGNAL_SENSOR))
}