package com.kluke.project.controllers.graphs

import com.kluke.project.EventContext
import com.kluke.project.ServiceConfiguration.EVENT_CONTEXT
import com.kluke.project.events.TickEvent
import com.kluke.project.getType
import com.kluke.project.sensors.POLLING_INTERVAL_MOD
import com.kluke.project.sensors.RoadSpeedEvent
import javafx.scene.chart.XYChart
import javafx.scene.chart.XYChart.Data
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.qualifier.named
import tornadofx.*

class RoadSpeedGraphController : Controller(), KoinComponent {
    private val eventContext = get<EventContext>(named(EVENT_CONTEXT))

    val series = XYChart.Series<String, Number>()

    init {
        subscribe<TickEvent> { tickEvent ->
            runOnTickEvent(tickEvent = tickEvent, pollingIntervalMod = POLLING_INTERVAL_MOD) { queryPeriod ->
                val roadSpeedQuery = eventContext.getType<RoadSpeedEvent>(queryPeriod)

                updateSeries(series, roadSpeedQuery) { map, series ->
                    val key = map.key.keyPattern()
                    val value = map.value.value

                    series.data.add(Data(key, value))
                }
            }
        }
    }
}