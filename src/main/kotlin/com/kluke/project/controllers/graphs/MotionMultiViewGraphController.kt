package com.kluke.project.controllers.graphs

import com.kluke.project.EventContext
import com.kluke.project.ServiceConfiguration.EVENT_CONTEXT
import com.kluke.project.events.TickEvent
import com.kluke.project.getType
import com.kluke.project.sensors.POLLING_INTERVAL_MOD
import com.kluke.project.sensors.RoadSpeedEvent
import com.kluke.project.sensors.algorithms.serial.ECUFrame
import javafx.scene.chart.XYChart
import javafx.scene.chart.XYChart.Data
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.qualifier.named
import tornadofx.*

class MotionMultiViewGraphController : Controller(), KoinComponent {

    private val eventContext = get<EventContext>(named(EVENT_CONTEXT))

    val roadSpeedSeries = XYChart.Series<String, Number>()
    val tpsSeries = XYChart.Series<String, Number>()
    val rpmSeries = XYChart.Series<String, Number>()

    init {
        subscribe<TickEvent> { tickEvent ->
            runOnTickEvent(tickEvent, pollingIntervalMod = POLLING_INTERVAL_MOD) {  queryPeriod ->
                // Queries
                val roadSpeedQuery = eventContext.getType<RoadSpeedEvent>(queryPeriod)
                val ecuFrameQuery = eventContext.getType<ECUFrame>(queryPeriod)

                // Update
                updateSeries(roadSpeedSeries, roadSpeedQuery) { entry, series ->
                    // Map key and value
                    val key = entry.key.keyPattern()
                    val value = entry.value.value

                    // Add to graph series
                    series.data.add(Data(key, value))
                }
                updateSeries(tpsSeries, ecuFrameQuery) { entry, series ->
                    // Map key and value
                    val key = entry.key.keyPattern()
                    val value = entry.value.tps.percentage

                    series.data.add(Data(key, value))
                }

                updateSeries(rpmSeries, ecuFrameQuery) { entry, series ->
                    // Map key and value
                    val key = entry.key.keyPattern()
                    val value = entry.value.rpm

                    series.data.add(Data(key, value))
                }
            }
        }
    }
}
