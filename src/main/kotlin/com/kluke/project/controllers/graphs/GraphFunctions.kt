package com.kluke.project.controllers.graphs

import com.kluke.project.events.TickEvent
import javafx.scene.chart.XYChart.Series
import org.joda.time.Instant
import org.joda.time.format.DateTimeFormat
import tornadofx.*

fun Instant.keyPattern(): String = toString(DateTimeFormat.forPattern("ss.SS"))

fun lastFiveSeconds(): Instant = lastMilliseconds(5_000)

fun lastMilliseconds(millis: Long): Instant = Instant.now().minus(millis)

fun <A, B> updateSeries(series: Series<String, Number>, query: Map<A, B>,
                        op: (Map.Entry<A, B>, Series<String, Number>) -> Unit) =
        syncReplaceSeries(series) {
            query.forEach {
                op(it, series)
            }
        }

fun <A, B> syncReplaceSeries(series: Series<A, B>, context: () -> Unit) = with(series.data) {
    synchronized(this) {
        clear()
        context()
    }
}

fun runOnTickEvent(tickEvent: TickEvent,
                   queryPeriod: Instant = lastFiveSeconds(),
                   pollingIntervalMod: Int,
                   op: (Instant) -> Unit) {
    if (tickEvent.tickCount % pollingIntervalMod == 0L) {
        runAsync {
            runLater {
                op(queryPeriod)
            }
        }
    }
}