package com.kluke.project.controllers

import com.kluke.project.Sensors.*
import com.kluke.project.sensors.Sensor
import com.kluke.project.views.components.EBrakeIndicator
import com.pi4j.io.gpio.PinState
import javafx.scene.layout.Region
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.qualifier.named
import tornadofx.*
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit.SECONDS
import java.util.concurrent.atomic.AtomicBoolean

class EBrakeController : Controller(), KoinComponent {
    private val executorService: ScheduledExecutorService = get()
    private val sensor: Sensor<PinState> = get(named(E_BRAKE_SENSOR))
    private val eBrakeIndicator = EBrakeIndicator()

    // Initialize region with indicator active
    val region = Region().apply { getChildList()?.add(eBrakeIndicator) }

    // When start flag is disabled, the region's children will be cleared once.
    private val startFlag = AtomicBoolean(true)

    init {
        // Set start flag to false and remove component from UI.
        val action = {
            startFlag.set(false)

            // Turn off the indicator if it's not active via the sensor reading
            val sensorState = sensor.property().get()
            if (sensorState.isLow) runLater {
                region.getChildList()?.clear()
            }
        }

        // E-Brake light must be on for at least 5 seconds
        executorService.schedule(action, 8, SECONDS)

        sensor.property().onChange { pinState ->
            // Only function normally when the flag is false. This prevents the indicator from toggling off before 5 seconds.
            if (!startFlag.get() && pinState != null) {
                runLater {
                    val children = region.getChildList() ?: return@runLater

                    when {
                        pinState.isHigh -> children.add(eBrakeIndicator)
                        pinState.isLow -> children.clear()
                    }
                }
            }
        }
    }
}