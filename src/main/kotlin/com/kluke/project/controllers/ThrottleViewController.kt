package com.kluke.project.controllers

import com.kluke.project.ServiceConfiguration.SERIAL_DATA_SOURCE
import com.kluke.project.map
import com.kluke.project.sensors.SerialDataSource
import com.kluke.project.sensors.algorithms.serial.TPSData
import javafx.beans.property.SimpleDoubleProperty
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.qualifier.named
import tornadofx.*

class ThrottleViewController : Controller(), KoinComponent {
    private val serialDataSource = get<SerialDataSource>(named(SERIAL_DATA_SOURCE))
    private val tpsProperty = SimpleDoubleProperty(0.0)

    init {
        tpsProperty.bind(binding())
    }

    fun tpsProperty() = tpsProperty

    private fun binding() = serialDataSource.property().doubleBinding {
        it ?: return@doubleBinding 0.0

        // Apply custom mapping to calibrate range to fit my specific vehicle's TPS range.
        it.tps.calibratedMapping()
    }

}