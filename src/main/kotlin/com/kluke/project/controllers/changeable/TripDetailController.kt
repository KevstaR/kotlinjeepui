package com.kluke.project.controllers.changeable

import com.kluke.project.Instruments.*
import com.kluke.project.events.TripResetEvent
import com.kluke.project.instruments.trip.TripInstrument
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.qualifier.named
import tornadofx.Controller

class TripDetailController : Controller(), KoinComponent {
    val distanceInstrument = get<TripInstrument<Double>>(named(TRIP_DISTANCE_INSTRUMENT))
    val averageSpeedInstrument = get<TripInstrument<Double>>(named(TRIP_AVERAGE_SPEED_INSTRUMENT))
    val formattedTimeInstrument = get<TripInstrument<String>>(named(TRIP_FORMATTED_TIME_INSTRUMENT))
    val fuelEconomyInstrument = get<TripInstrument<Double>>(named(TRIP_FUEL_ECONOMY_INSTRUMENT))

    init {
        subscribe<TripResetEvent> {
            get<List<TripInstrument<out Any>>>(named(TRIP_INSTRUMENTS)).forEach(TripInstrument<out Any>::reset)
        }
    }
}