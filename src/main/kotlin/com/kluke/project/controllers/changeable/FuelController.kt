package com.kluke.project.controllers.changeable

import com.kluke.project.controllers.graphs.updateSeries
import com.kluke.project.events.TickEvent
import com.kluke.project.instruments.FuelInstrument
import com.kluke.project.map
import com.kluke.project.smoothing.kernelRegression
import javafx.beans.property.SimpleIntegerProperty
import javafx.scene.chart.XYChart
import javafx.scene.chart.XYChart.Data
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import tornadofx.*
import kotlin.math.roundToInt

class FuelController : Controller(), KoinComponent {
    val rawFuelInstrumentSeries = XYChart.Series<String, Number>()
    val fuelInstrumentSeries = XYChart.Series<String, Number>()

    val fillBar = SimpleIntegerProperty(0)

    private val fuelInstrument = get<FuelInstrument>()

    init {
        subscribe<TickEvent> { tickEvent ->
            if (tickEvent.tickCount % 10 == 0L) runAsync {
                with(indexedData()) {
                    // Don't run calculations on the Application thread.
                    val rsd: Map<String, Number> = rawSeriesData()
                    val ssd: Map<String, Double> = smoothedSeriesData()
                    val fbv: Int = ssd.fillBarValue()

                    runLater {
                        updateSeries(fuelInstrumentSeries, rsd) { entry, series ->
                            series.name = "RAW"
                            series.data.add(Data(entry.key, entry.value))
                        }
                        updateSeries(rawFuelInstrumentSeries, ssd) { entry, series ->
                            series.name = "REGRESSION"
                            series.data.add(Data(entry.key, entry.value))
                        }

                        synchronized(fillBar) {
                            fillBar.set(fbv)
                        }
                    }
                }
            }
        }
    }

    private fun indexedData() = fuelInstrument.observableData().mapIndexed { index, item -> index to item.map(inMax = 1023) }.toMap()
    private fun Map<Int, Number>.rawSeriesData() = mapKeys { it.key.toString() }
    private fun Map<Int, Number>.smoothedSeriesData(): Map<String, Double> = kernelRegression().mapKeys { it.key.toString() }
    private fun Map<String, Double>.fillBarValue() = values.average().map(inMax = 100.0, outMax = 19.0).roundToInt()
}