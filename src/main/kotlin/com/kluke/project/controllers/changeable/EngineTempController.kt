package com.kluke.project.controllers.changeable

import com.kluke.project.instruments.EngineTempInstrument
import com.kluke.project.instruments.EngineTempInstrument.Companion.MAX_TEMP_KELVIN
import com.kluke.project.map
import javafx.beans.property.SimpleIntegerProperty
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import tornadofx.*

class EngineTempController : Controller(), KoinComponent {
    private val engineTempInstrument = get<EngineTempInstrument>()
    val fillBar = SimpleIntegerProperty(0)

    fun temperature() = engineTempInstrument.temperatureProperty()

    init {
        fillBar.bind(fillBarBinding())
        engineTempInstrument.unitProperty().onChange { fillBar.bind(fillBarBinding()) }
    }

    private fun fillBarBinding() = temperature().doubleBinding {
        it ?: return@doubleBinding 0.0

        it.toDouble().map(
                // In max is 411.28 Kelvin, which is approx 280 F.
                inMax = engineTempInstrument.unitProperty().get().convert(MAX_TEMP_KELVIN),
                outMax = 19.0
        )
    }
}