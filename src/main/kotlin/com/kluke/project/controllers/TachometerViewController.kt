package com.kluke.project.controllers

import com.kluke.project.ServiceConfiguration.SERIAL_DATA_SOURCE
import com.kluke.project.map
import com.kluke.project.sensors.SerialDataSource
import com.kluke.project.sensors.algorithms.serial.ECUFrame
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.qualifier.named
import tornadofx.*
import kotlin.math.roundToInt

class TachometerViewController : Controller(), KoinComponent {

    private val serialDataSource = get<SerialDataSource>(named(SERIAL_DATA_SOURCE))
    private val list = observableListOf<Number>()

    fun property() = serialDataSource.property()

    init {
        serialDataSource.property().onChange { ecuFrame ->
            ecuFrame ?: return@onChange

            with(list) {
                clear()
                addAll(0..max(ecuFrame))
            }
        }
    }

    private fun max(ecuFrame: ECUFrame) = ecuFrame.rpm.toDouble().map(inMax = 5000.0, outMax = 9.0).roundToInt()

    fun fillBar() = list
}