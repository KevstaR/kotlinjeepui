package com.kluke.project.controllers

import com.kluke.project.Sensors
import com.kluke.project.sensors.Sensor
import com.kluke.project.sensors.enums.TorqueConverter
import com.kluke.project.sensors.enums.TransmissionGear
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.qualifier.named
import tornadofx.Controller
import tornadofx.stringBinding

class GearController : Controller(), KoinComponent {
    private val gearSensor = get<Sensor<TransmissionGear>>(named(Sensors.TRANSMISSION_GEAR))
    private val torqueConverterSensor = get<Sensor<TorqueConverter>>(named(Sensors.TORQUE_CONVERTER))

    fun gearSensorProperty() = gearSensor.property()

    fun gearSensorTextBinding() = gearSensor.property().stringBinding {
        it ?: return@stringBinding ""
        it.name
    }

    fun torqueConverterSensorTextBinding() = torqueConverterSensor.property().stringBinding {
        it ?: return@stringBinding ""
        it.name
    }
}