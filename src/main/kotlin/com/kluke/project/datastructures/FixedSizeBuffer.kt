package com.kluke.project.datastructures

import com.sun.javafx.collections.ObservableListWrapper
import javafx.collections.ObservableList
import kotlin.collections.ArrayList

/**
 * Array list acting as a ring buffer.  Requires a max capacity and can be filled on initialization.
 *
 * Once max capacity is reached, elements at the zero index are shifted out of existence.
 */
class FixedSizeBuffer<T>(private val maxCapacity: Int, fill: T? = null) : ArrayList<T>(maxCapacity) {

    init { if (fill != null) repeat (maxCapacity) { add(fill) } }

    override fun add(element: T): Boolean {
        if (size >= maxCapacity) removeAt(0)
        return super.add(element)
    }

    fun removeFirst(): T = removeAt(0)
}

class ObservableRingBuffer<T>(private val maxCapacity: Int, fill: T? = null,
        private val list: ArrayList<T> = ArrayList(maxCapacity)) : ObservableListWrapper<T>(list) {

    init { if (fill != null) repeat (maxCapacity) { list.add(fill) } }

    override fun add(element: T): Boolean {
        if (size >= maxCapacity) list.removeAt(0)
        return super.add(element)
    }
}
