package com.kluke.project.datastructures

import com.ginsberg.cirkle.CircularList

class CircularListIterator<T>(list: List<T>) : ListIterator<T> by list.listIterator() {
    private val circularList = CircularList(list)
    private var index = 0
    private var previousIndex = 0

    override fun next(): T = circularList[index].also {
        previousIndex = index
        index += 1
    }

    override fun previous(): T = circularList[previousIndex - 1].also {
        previousIndex -= 1
        index -= 1
    }

    override fun nextIndex() = index
    override fun previousIndex() = previousIndex

    override fun hasNext() = circularList.isNotEmpty()
    override fun hasPrevious() = circularList.isNotEmpty()
}

fun <T> CircularList<T>.circularIterator() = CircularListIterator(this)