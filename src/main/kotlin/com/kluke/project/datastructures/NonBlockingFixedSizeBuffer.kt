package com.kluke.project.datastructures

import java.util.concurrent.ArrayBlockingQueue

class NonBlockingFixedSizeBuffer<T>(private val maxCapacity: Int) : ArrayBlockingQueue<T>(maxCapacity) {
    override fun add(element: T): Boolean {
        if (super.size >= maxCapacity) remove()
        return super.add(element)
    }
}