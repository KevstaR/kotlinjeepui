package com.kluke.project.datastructures

import java.util.*

/**
 * This is used to count how many changes occurred within a given time delta.
 *
 * Starting with the top of the queue, pop and peek items in the queue until their total is within the specified range.
 *
 * Example:
 * Queue has: 0, 10, 20, 30
 *
 * Queue is queried for a range of 35. Result will be 2 and remainder in the deque will be 30.
 */
fun Deque<Long>.query(range: Long): Int {
    // Track the total duration
    var total: Long = 0

    // Count the deltas
    var deltaCount = 0

    while (size > 1) {
        val first = pop()
        val second = peek()
        val delta = second - first

        // Read external total and check it against the new delta
        val newTotal = total + delta

        // If total is greater than or equal to poll range, break, else replace total.
        total = if (newTotal >= range) break else {
            deltaCount++
            newTotal
        }
    }
    return deltaCount
}