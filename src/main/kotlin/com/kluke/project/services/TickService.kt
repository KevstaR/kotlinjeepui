package com.kluke.project.services

import com.kluke.project.events.TickEvent
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit.MILLISECONDS
import java.util.concurrent.atomic.AtomicLong

const val GLOBAL_TICK_RATE = 100L

/**
 * Tick service manages scheduling of TickEvents on a repeated interval.
 * TickEvents are fired on the event bus and read by various subscribers.
 */
class TickService(scheduler: ScheduledExecutorService) : AbstractService(scheduler), Service {

    val totalTicks = AtomicLong(0)
    private val lastTime = AtomicLong(0)

    override fun start() {
        val ticker = {
            // Compute time delta
            val time = System.currentTimeMillis()
            val previousTime = lastTime.get()

            val delta = time - previousTime

            // Fire main event with payload and increment total
            fire(TickEvent(totalTicks.incrementAndGet(), delta))
            lastTime.set(time)
        }

        handle = scheduler.scheduleWithFixedDelay(ticker, 0, GLOBAL_TICK_RATE, MILLISECONDS)
    }
}