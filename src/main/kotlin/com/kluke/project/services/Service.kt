package com.kluke.project.services

interface Service {
    fun start()
    fun waitFor(wait: Long)
    fun stop()
}