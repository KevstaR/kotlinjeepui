package com.kluke.project.services

interface CumulativeAverager {
    fun accumulateAndGet(value: Number): Double
    fun get(): Double

    /**
     * Get the count used to compute the averages.
     */
    fun count(): Long
    fun reset()
}

/**
 * https://en.wikipedia.org/wiki/Moving_average#Simple_moving_average_(boxcar_filter)
 *
 * the current cumulative average for a new datum point is equal to the previous cumulative average, times n,
 * plus the latest datum point, all divided by the number of points received so far, n+1.
 * When all of the datum points arrive (n = N), then the cumulative average will equal the final average.
 * It is also possible to store a running total of the datum point as well as the number of points and dividing the
 * total by the number of datum points to get the CMA each time a new datum point arrives.
 */
class CumulativeMovingAverager(initialAverage: Double = 0.0, initialCount: Long = 0) : CumulativeAverager {
    private var avg = initialAverage
    private var count = initialCount

    @Synchronized
    override fun accumulateAndGet(value: Number) = ((value.toDouble() + count * avg) / (count + 1.0)).also {
        avg = it
        count++
    }

    @Synchronized
    override fun get() = avg

    @Synchronized
    override fun count() = count

    @Synchronized
    override fun reset() {
        avg = 0.0
        count = 0
    }
}