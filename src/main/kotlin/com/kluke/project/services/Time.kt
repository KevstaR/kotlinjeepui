package com.kluke.project.services

import javafx.beans.property.LongProperty
import javafx.beans.property.SimpleLongProperty
import org.joda.time.format.PeriodFormatter
import org.joda.time.format.PeriodFormatterBuilder
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit.SECONDS
import tornadofx.*

interface TimeService {
    fun elapsedDurationProperty(): LongProperty
}

class TimeServiceImpl(scheduledExecutorService: ScheduledExecutorService) : TimeService {
    private val elapsedSecondsProperty: LongProperty = SimpleLongProperty(0)
    private var elapsedSeconds by elapsedSecondsProperty

    init {
        scheduledExecutorService.scheduleAtFixedRate({ incrementProperty() }, 0, 1, SECONDS)
    }

    @Synchronized
    private fun incrementProperty() {
        elapsedSeconds++
    }

    override fun elapsedDurationProperty() = elapsedSecondsProperty
}

data class Duration(private val seconds: Long) {
    fun toSeconds() = seconds
    fun toMinutes() = toSeconds() / 60.0
    fun toHours() = toMinutes() / 60.0

    operator fun plus(increment: Duration) = Duration(seconds + increment.seconds)
}

fun timeFormat(): PeriodFormatter = PeriodFormatterBuilder()
    .printZeroAlways().minimumPrintedDigits(2).appendHours()
    .appendSeparator(":")
    .printZeroAlways().minimumPrintedDigits(2).appendMinutes()
    .appendSeparator(":")
    .printZeroAlways().minimumPrintedDigits(2).appendSeconds()
    .toFormatter()

