package com.kluke.project.services

import tornadofx.*
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

abstract class AbstractService(protected val scheduler: ScheduledExecutorService) : Component(), Service {
    protected var handle: ScheduledFuture<*>? = null

    override fun waitFor(wait: Long) {
        if (handle == null) throw RuntimeException("Must call start before calling wait for.")
        scheduler.schedule({ handle!!.cancel(false) }, wait, TimeUnit.MILLISECONDS)
        scheduler.awaitTermination(wait, TimeUnit.MILLISECONDS)
    }

    override fun stop() = scheduler.shutdown()
}