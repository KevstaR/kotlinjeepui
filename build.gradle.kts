import io.gitlab.arturbosch.detekt.Detekt
import org.hidetake.groovy.ssh.core.Remote
import org.hidetake.groovy.ssh.core.RunHandler
import org.hidetake.groovy.ssh.core.Service
import org.hidetake.groovy.ssh.session.SessionHandler
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.31"
    id("io.gitlab.arturbosch.detekt") version "1.17.0"
    id("org.hidetake.ssh") version "2.10.1"
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions {
            jvmTarget = "1.8"
            freeCompilerArgs = listOf("-Xopt-in=kotlin.ExperimentalUnsignedTypes")
        }
    }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.4.31")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.3")

    implementation("no.tornado:tornadofx:1.7.20")
    implementation("com.github.sarxos:webcam-capture:0.3.12")
    implementation("com.pi4j:pi4j-core:1.3")
    implementation("com.pi4j:pi4j-gpio-extension:1.3")
    implementation("io.insert-koin:koin-core:3.0.1")
    implementation("com.fazecast:jSerialComm:2.7.0")
    implementation("com.sothawo:mapjfx:1.32.1")
    implementation("org.slf4j:slf4j-simple:1.7.30")
    implementation("org.xerial:sqlite-jdbc:3.31.1")
    implementation("org.jetbrains.exposed:exposed:0.17.13")
    implementation("com.ginsberg:cirkle:1.0.1")
    implementation("org.jcodec:jcodec:0.2.5")
    implementation("org.jcodec:jcodec-javase:0.2.5")

    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5:1.4.31")
    testImplementation("io.insert-koin:koin-test-core:2.2.2")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.1")
    testImplementation("org.assertj:assertj-core:3.19.0")
    testImplementation("io.mockk:mockk:1.11.0")
    testImplementation("org.testfx:testfx-junit5:4.0.16-alpha")
}

repositories {
    mavenCentral()
}

detekt {
    buildUponDefaultConfig = true // preconfigure defaults
    allRules = false // activate all available (even unstable) rules.
//    config = files("$projectDir/config/detekt.yml") // point to your custom config defining rules to run, overwriting default behavior
//    baseline = file("$projectDir/config/baseline.xml") // a way of suppressing issues before introducing detekt

    reports {
        html.enabled = true // observe findings in your browser with structure and code snippets
        xml.enabled = true // checkstyle like format mainly for integrations like Jenkins
        txt.enabled = true // similar to the console output, contains issue signature to manually edit baseline files
        sarif.enabled = true // standardized SARIF format (https://sarifweb.azurewebsites.net/) to support integrations with Github Code Scanning
    }

}

tasks.withType<Detekt>().configureEach {
    // Target version of the generated JVM bytecode. It is used for type resolution.
    jvmTarget = "1.8"
    onlyIf { project.hasProperty("runDetect") }
}

//remotes {
//    withGroovyBuilder {
//        "create"("webServer") {
//            setProperty("host", "raspberrypi.local")
//            setProperty("host", "127.0.0.1")
//            setProperty("host", "127.0.0.1")
//        }
//    }
//}

tasks.create(name = "deploy-war") {

    group = "deploy"

    val myServer = remotes.create("my-server") {
        host = "raspberrypi.local"
        user = "pi"
        password = "raspberrypi"
    }

    doLast {
        ssh.run(delegateClosureOf<RunHandler> {
            session(myServer, delegateClosureOf<SessionHandler> {
                put(hashMapOf("from" to "build/libs/kotlinjeepui.jar", "into" to "/home/pi/Documents"))
                execute("echo 'some bullshit' >> ~/Desktop/shit.txt")
            })
        })
    }
}